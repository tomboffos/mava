<?php

namespace App\Http\Controllers\Api\Admin\Attribute;

use App\Http\Controllers\Controller;
use App\Models\AttributeType;
use App\Models\CategoryAttribute;
use App\Models\ShopCategory;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    //

    public function get($id)
    {
        $attributes = CategoryAttribute::where('shop_category_id',$id)->get();
        $category = ShopCategory::where('id',$id)->first();
        if ($category['parent_id'] != null){
            $parent = 0;

            while ($parent < 1){
                $newAttributes = CategoryAttribute::where('shop_category_id',$category['parent_id'])->get();
                foreach ($newAttributes as $newAttribute){
                    $attributes[] = $newAttribute;
                }
                $categoryParent = ShopCategory::find($category['parent_id']);
                if ($categoryParent['parent_id'] != null){
                    $orderParentAttributes = CategoryAttribute::where('shop_category_id',$categoryParent['parent_id'])->get();

                    foreach ($orderParentAttributes as $orderAttribute){
                        $attributes[] = $orderAttribute;

                    }
                    $parent = 1;
                }else{
                    $parent = 1;
                }

            }
        }

        if ($attributes){
            foreach ($attributes as $attribute){
                $attribute['values'] = explode(',',$attribute['values']);
                $attribute['type'] = AttributeType::find($attribute['attribute_type_id']);
            }

            return response(['attributes' => $attributes],200);
        }else{
            return response(['attributes' => null],200);

        }

    }

    public function getCategories()
    {
        $categories = ShopCategory::with(['children','attributes'])->get();

        return response(['categories' => $categories],200);
    }
}
