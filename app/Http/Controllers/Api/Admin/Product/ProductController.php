<?php

namespace App\Http\Controllers\Api\Admin\Product;

use App\Http\Controllers\Controller;
use App\Models\CategoryAttribute;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //

    public function getProduct($id)
    {
        $product = Product::with('category')->find($id);

        return response(['product' => $product,],200);
    }
}
