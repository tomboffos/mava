<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'email|required|unique:users,email',
            'password' => 'required|min:8'
        ]);

        $user = User::where('email',$request->email)->first();
        if (!$user){
            return  redirect()->route('welcome');
        }
        if (Hash::check($request->password,$user->password)){
            return redirect()->route('user');
        }else{
            return redirect()->route('welcome');

        }
    }
}
