<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Models\AttributeType;
use App\Models\CategoryFilter;
use App\Models\Product;
use App\Models\PurchaseReturn;
use App\Models\ShopCategory;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    //
    public function categories($id = null)
    {
        if ($id == null){

        }else{

        }
    }
    public function filters(Request $request)
    {
        $inputs = [];

        if ($request->category){
            $categoryFilters = CategoryFilter::join('category_attributes','category_attribute_id','=','category_attributes.id')
                ->where('category_filters.shop_category_id',$request->category)->get();
            foreach ($categoryFilters as $filter){
                $inputs[] = $filter;
            }
            $categoryChilds  = ShopCategory::where('parent_id',$request->category)->get();
            foreach ($categoryChilds as $child){
                $categoryFiltersChild = CategoryFilter::join('category_attributes','category_attribute_id','=','category_attributes.id')
                    ->where('category_filters.shop_category_id',$child->id)->get();

                foreach ($categoryFiltersChild as $filter){
                    $inputs[] = $filter;
                }
            }


            foreach ($inputs as $categoryFilter){
                $categoryFilter['attribute_type'] =AttributeType::find( $categoryFilter['attribute_type_id']);
                $categoryFilter['values'] = explode(',',$categoryFilter['values']);
            }



        }






        return response(['data' =>$inputs],200);
    }

    public function products(Request $request)
    {
        if ($request->has('categories')){

            if ($request->categories[0] != null){
                $category[] = ShopCategory::where('id',$request->categories)->first()->id;
                if ($category != []){
                    $categories = ShopCategory::where('parent_id',$request->categories)->get();
                    foreach ($categories as $catParent){
                        $category[] = $catParent->id;

                        $catChild = ShopCategory::where('parent_id',$catParent->id)->get();
                        foreach ($catChild as $child){
                            $category[] = $child->id;
                        }
                    }

                }
            }else{
                $category = [];
            }
            $products = Product::with('variations')->where(function($query) use ($request,$category){
                if ($request->categories[0] != null){
                    $query->whereIn('shop_category_id',$category);
                }

                if ($request->has('price')){
                    $query->whereBetween('price',$request->price);
                }



                if ($request->has('additional_attributes')) {



//                    dd($attributes);
                    $query->whereJsonContains('additional_attributes',$request->additional_attributes);



                }

                if ($request->has('search')){
                    $query->where('name','LIKE','%'.$request->search.'%');
                }

                if ($request->has('brand')){
                    $query->where('brand_id',$request->brand);
                }

            })->orderBy('id','desc')->paginate(12);


            foreach ($products as $product){
                $product['image'] = asset('storage/'.$product['image']);
            }


            return response(['products' => $products],200);
        }


    }

    public function category(Request $request)
    {
        if ($request->has('category')){
            $category = ShopCategory::where('parent_id',$request->category)->get();



            return response(['categories'=> $category],200);
        }
    }

    public function returnCreate(Request $request)
    {
        if (!$request->has('reason')){
            return response(['message' => 'Укажите причину возврата']);
        }

        if(!$request->has('name')){
            return response(['message' => 'Укажите имя']);

        }
        if(!$request->has('phone')){
            return response(['message' => 'Укажите телефон']);

        }

        if(!$request->has('product_id')){
            return response(['message' => 'Выберите продукт']);

        }

        $return  = PurchaseReturn::create([
           'name' => $request->name,
           'phone' => $request->phone,
           'order_id' => $request->order_id,
           'user_id' => $request->user_id,
           'product_id' => $request->product_id,
           'return_reason' => $request->reason,

        ]);

        return  response(['message' =>  'Заявка была отправлена'],200);


    }
}
