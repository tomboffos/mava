<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\Review;
use App\Models\ShopCategory;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    //
    public function index($id = null,Request $request)
    {
        if ($id != null){
            $category = ShopCategory::find($id);

        }else{
            $category = null;
        }
        $search = null;
        if ($request->has('search')){
            $search = $request->search;
        }
        $brand = null;
        if ($request->has('brand')){
            $brand = $request->brand;
        }
        return view('product.index',compact('category','search','brand'));
    }

    public function show($id)
    {
        $product = Product::with(['category', 'color_size.size', 'reviews.user', 'product_flags.flag','variations'])->find($id);
        $product['slider_images'] = json_decode($product['slider_images']);

        $attributes = [];
        if ($product['additional_attributes'] != null){
            foreach ($product['additional_attributes'] as $key => $attribute) {
                $attribute = explode(':', $attribute);
                $attributes[] = $attribute;
            }
        }

        $reviews = Review::where('product_id',$product->id)->get();

        return view('product.show',['product' => $product,'attributes' => $attributes,'reviews' => $reviews]);
    }

    public function addReview(Request $request)
    {

    }

    public function addBasket($productId,Request $request)
    {
        if ($productId){
            $basket = session()->get('basket');
            if (!$basket){
                $basket = [];
                $newProduct['product_id'] = $productId;
                $newProduct['variation_id'] =$request->variation_id;
                $newProduct['quantity'] = 1;
                $basket[] = $newProduct;
                session()->put('basket',$basket);
            }else{

                foreach ($basket as $key=> $product){
                    if ($request->has('variation_id')) {
                        if ($product['product_id'] == $productId && $product['variation_id'] == $request->variation_id) {
                            $basket[$key]['quantity'] += $request->has('quantity') ? $request->quantity : 1;
                            break;
                        }elseif($product['product_id'] != $productId && $product['variation_id'] == $request->variation_id ||
                            $product['product_id'] == $productId && $product['variation_id'] != $request->variation_id){
                            $newProduct['product_id'] = $productId;
                            $newProduct['variation_id'] =$request->variation_id;
                            $newProduct['quantity'] =$request->has('quantity') ? $request->quantity : 1;
                            $basket[] = $newProduct;
                             break;
                        }else{

                            $newProduct['product_id'] = $productId;
                            $newProduct['variation_id'] =$request->variation_id;
                            $newProduct['quantity'] = $request->has('quantity') ? $request->quantity : 1;
                            $basket[] = $newProduct;
                            break;
                        }
                    }else{
                        if ($product['product_id'] == $productId && $product['variation_id'] == null) {
                            $basket[$key]['quantity'] += $request->has('quantity') ? $request->quantity : 1;
                            break;
                        }elseif($product['product_id'] != $productId && $product['variation_id'] == null||
                            $product['product_id'] == $productId && $product['variation_id'] != null){
                            $newProduct['product_id'] = $productId;
                            $newProduct['variation_id'] =null;
                            $newProduct['quantity'] =$request->has('quantity') ? $request->quantity : 1;
                            $basket[] = $newProduct;

                            break;
                        }else{
                            $newProduct['product_id'] = $productId;
                            $newProduct['variation_id'] =null;
                            $newProduct['quantity'] = $request->has('quantity') ? $request->quantity : 1;
                            $basket[] = $newProduct;
                            break;
                        }
                    }
                }

            }
            session()->put('basket',$basket);
            session()->save();

            return response(['message' => 'Продукт добавлен в корзину'],200);
        }
    }

    public function deleteBasketProduct($productId,Request $request)
    {
        $basket = session()->get('basket');

        if ($basket){

            foreach ($basket as $key=>$product){

                if ($request->has('variation_id')){
                    if ($product['variation_id'] == $request->variation_id && $product['product_id'] == $productId){
                        unset($basket[$key]);
                        break;
                    }
                }else{
                    if ($product['variation_id'] == "null" && $product['product_id'] == $productId || $product['variation_id'] == null && $product['product_id'] == $productId){
                        unset($basket[$key]);
                        break;
                    }
                }

            }

            session()->put('basket',$basket);

            session()->save();

            return back();
        }


    }

    public function addPlus($productId,Request $request)
    {
        $basket = session()->get('basket');

        if ($basket){

            foreach ($basket as $key=>$product){

                if ($request->has('variation_id')){
                    if ($product['variation_id'] == $request->variation_id && $product['product_id'] == $productId){
                        $basket[$key]['quantity'] += 1;
                        break;
                    }
                }else{
                    if ($product['variation_id'] == "null" && $product['product_id'] == $productId || $product['variation_id'] == null && $product['product_id'] == $productId){
                        $basket[$key]['quantity'] += 1;
                        break;
                    }
                }

            }

            session()->put('basket',$basket);

            session()->save();

            return back();
        }


    }
    public function addMinus($productId,Request $request)
    {
        $basket = session()->get('basket');

        if ($basket){

            foreach ($basket as $key=>$product){

                if ($request->has('variation_id')){
                    if ($product['variation_id'] == $request->variation_id && $product['product_id'] == $productId){
                        $basket[$key]['quantity'] -= 1;
                        if ($basket[$key]['quantity'] == 0){
                            unset($basket[$key]);
                        }
                        break;
                    }
                }else{
                    if ($product['variation_id'] == "null" && $product['product_id'] == $productId || $product['variation_id'] == null && $product['product_id'] == $productId){
                        $basket[$key]['quantity'] -= 1;
                        if ($basket[$key]['quantity'] == 0){
                            unset($basket[$key]);
                        }
                        break;
                    }
                }

            }

            session()->put('basket',$basket);

            session()->save();

            return back();
        }


    }

    public function addFavourite($productId)
    {
        if ($productId){
            $basket = session()->get('favourite');
            if (!$basket){
                $basket = [];
                $basket[ Product::find($productId)->id] = 1;
                session()->put('favourite',$basket);
            }else{
                if (array_key_exists($productId,$basket)){
                    unset($basket[$productId]);
                    session()->put('favourite',$basket);
                    session()->save();

                    return response(['message' => 'Продукт был удален из избранных'],422);

                }else{
                    $basket[$productId] = 1;
                    session()->put('favourite',$basket);
                }
            }
            session()->save();
            return response(['message' => 'Продукт добавлен в избранное'],200);
        }
    }

    public function deleteFavourite(Request $request)
    {
        $basket = session()->get('favourite');

        if ($basket){
            if (array_key_exists($request->product,$basket)){
                unset($basket[$request->product]);
                session()->put('favourite',$basket);
                session()->save();
            }

            return response(['message' => 'Продукт удален с корзины'],200);
        }


    }

    public function order()
    {
        $basket = session()->get('basket');
        $basketMain = [];
        $sum = 0;
        $quantitySum = 0;
        foreach ($basket as $id => $quantity){
            $product = Product::find($quantity['product_id']);
            $product['quantity'] = $quantity['quantity'];

            if ($quantity['variation_id'] != null && $quantity['variation_id'] != "null" ){
                $product['variation'] = ProductVariation::find($quantity['variation_id']);
            }else{
                $product['variation'] = null;
            }

            $basketMain[] = $product;


            if ($product['variation'] != null){
                $sum += $product['variation']['price'] * $quantity['quantity'];

            }else{
                $sum += $product->price * $quantity['quantity'];

            }
            $quantitySum += $product['quantity'];
        }
        return view('order.index',compact('sum','quantitySum'));
    }


    public function favourite()
    {
        $basket = session()->get('favourite');
        $basketMain = [];
        $sum = 0;
        $quantitySum = 0;


        if ($basket != []){
           foreach ($basket as $key=>$quantity){
               $product = Product::with('product_flags.flag')->find($key);
               $product['quantity'] = $quantity;
               $quantitySum += $quantity;
               $sum += $quantity*$product['price'];
               $basketMain[] = $product;
           }
        }

        return view('account.favourite',compact('basketMain','sum','quantitySum'));
    }
    public function basket()
    {
        $basket = session()->get('basket');
        $basketMain = [];
        $sum = 0;
        $quantitySum = 0;
        if($basket != []) {
            foreach ($basket as $id => $quantity) {
                $product = Product::find($quantity['product_id']);

                if ($quantity['variation_id'] != null){
                    $product['variation'] = ProductVariation::find(intval($quantity['variation_id']));

                }else{
                    $product['variation'] = null;
                }
                $product['quantity'] = $quantity['quantity'];
                $basketMain[] = $product;
                if ($product['variation'] != null){
                    $sum += $product['variation']['price'] * $quantity['quantity'];

                }else{
                    $sum += $product->price * $quantity['quantity'];

                }
                $quantitySum = count($basketMain);
            }
        }


        return view('basket.index',compact('basketMain','sum','quantitySum'));
    }
}
