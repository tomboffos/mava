<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\MainSection;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Stevebauman\Location\Facades\Location;
use TCG\Voyager\Models\User;
use App\Http\Requests\StoreRegister;

class IndexController extends Controller
{
    //
    public function conditions()
    {
        return view('conditions');
    }
    public function pay($id)
    {
        $order = Order::find($id);
        return view('pay',compact('order'));
    }

    public function about()
    {
        return view('about');

    }

    public function delivery()
    {
        return view('delivery');
    }

    public function contacts()
    {
        return view('contacts');
    }
    public function index(Request $request)
    {
        $data = Location::get($request->ip());
        dd($data);
        Log::error('info user'. print_r($data));
        $data['banners'] = Banner::orderBy('id','asc')->get();
        $data['main_sections'] = MainSection::with(['products_section.products.product_flags.flag'])->get();
        return view('welcome',$data);

    }

    public function login(Request $request)
    {

        if (!$request->has('password')){
            return back()->withErrors('Введите пароль');
        }
        if (!$request->has('email')){
            return back()->withErrors('Введите почту');
        }

        $user = User::where('email',$request->email)->first();
        if (!$user){
            return  redirect()->route('index');
        }
        if (Hash::check($request->password,$user->password)){
            session()->put('user',$user);
            session()->save();
            return redirect()->route('user');
        }else{
            return back();
        }


    }

    public function user()
    {
        $user = session()->get('user');
        return view('account.index',compact('user'));
    }
    public function order(Request $request)
    {
        $valData = $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country_city' => 'required',

        ]);
        $basket = session()->get('basket');

        $totalQuantity = 0;
        $totalSum = 0;

        foreach ($basket as $id => $quantity){
            $basketProduct = Product::find($quantity['product_id']);
            if($quantity['variation_id'] != "null"){
                $variation = ProductVariation::find($quantity['variation_id']);

            }else{
                $variation = null;

            }
            $totalQuantity += $quantity['quantity'];
            if ($variation != null){
                $totalSum +=$variation['price'] * $quantity['quantity'];

            }else {
                $totalSum += $basketProduct['price'] * $quantity['quantity'];
            }
        }

        $order = Order::create([
           'name' => $request->name,
           'phone' => $request->phone,
           'user_id' => $request->has('user_id') ? $request->user_id : null,
           'email' =>  $request->has('email') ? $request->email : null,
           'payment_type_id' => 1,
           'payment_status_id' => 1,
           'delivery_type_id' => 1,
           'order_status_id' => 1,
           'address' => $request->address,
           'country_city' => $request->country_city,
           'flat' => $request->has('flat') ? $request->flat : null,
           'mail_index' => $request->has('mail_index') ? $request->mail_index : null,
           'unit_price' => $totalSum >= 5000 ? $totalSum : $totalSum+500,
           'unit_quantity' => $totalQuantity,
           'delivery_price' => $totalSum >= 5000 ? 0 : 500,
        ]);
        $order->update([
            'order_number' => '#'.$order->created_at->day.$order->created_at->month.'-'.$order->created_at->year.'-'.$order->id
        ]);

        foreach ($basket as $key => $quantity){
            $productMain = Product::find($quantity['product_id']);
            if ($quantity['variation_id'] != null && $quantity['variation_id'] != "null"){
                $variation = ProductVariation::find($quantity['variation_id']);

            }
            OrderDetail::create([
               'unit_price' => $quantity['variation_id'] != null && $quantity['variation_id'] != "null" ? $quantity['quantity']*$variation['price'] : $quantity['quantity'] * $productMain['price'],
               'unit_quantity' => $quantity['quantity'],
               'order_id' => $order->id,
               'product_variation_id' => $quantity['variation_id'] != null && $quantity['variation_id'] != "null" ? $quantity['variation_id'] : null ,
               'product_id' => $quantity['product_id']
            ]);

        }
        $orderDetails = OrderDetail::with(['product','variation'])->where('order_id',$order->id)->get();
        session()->put('basket',[]);
        session()->save();
        Mail::send('email.mail',['order' => $order,'order_details'=> $orderDetails], function($message){
                    $message->to('mava.kz96@gmail.com', 'Mava')
                        ->subject('Заказ');
                    $message->from('infomava.kz@gmail.com','Заказ');
        });

        if($order->email != null){
            Mail::send('email.mail',['order' => $order,'order_details'=> $orderDetails], function($message) use ($order){
                $message->to($order->email, 'Mava')
                    ->subject('Заказ');
                $message->from('infomava.kz@gmail.com','mava.kz');
            });
        }
            return response([
                'message' => 'Заказ номер '.$order->order_number.' успешно создан',
                'redirect_url' => route('pay',$order->id)
            ]);
    }

    public function orderPage($id)
    {
        $order = Order::find($id);

        return view('order.page',compact($order));
    }

    public function logout()
    {
        $user = session()->get('user');
        if ($user){
            session()->remove('user');
            session()->save();

            return redirect()->route('index');
        }
        return redirect()->route('index');

    }

    public function storeReview(Request $request)
    {

        if(!$request->user_id){
            return back();
        }


        $orders = Order::where('user_id',$request->user_id)->get();

        $exception = 0;
        foreach ($orders as $order){

            $detail = OrderDetail::where('order_id',$order->id)->where('product_id',$request->product_id)->first();
            if ($detail != null){
                $exception = 1;
                break;
            }
        }

        if ($exception == 1) {
            $product = Product::find($request->product_id);
            $product['review_count']  += 1;
            $product['review_point'] += $request->star;
            $product->save();
            $review = new Review();
            $review['user_id'] = $request->user_id;
            $review['advantages'] = $request->advantages;
            $review['disadvantages'] = $request->disadvantages;
            $review['comments'] = $request->comment;
            $review['product_id'] = $request->product_id;
            $review['review'] = $request->star;
            $review->save();
        }

        return back();


    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|min:4',
            'email' => 'required',
            'password' => 'required|min:6',
            'phone' => 'required',
        ]);
        $user = User::create([
            'name' => $request->email,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        session()->put('user',$user);
        session()->save();

        return redirect()->route('user');
    }


    public function updateUser(Request $request)
    {
        $user = User::find($request->user_id);
        if ($request->has('name') && $request->name != '') {
            $user['name'] = $request->name;
        }
        if ($request->has('phone') && $request->phone != '') {
            $user['phone'] = $request->phone;
        }
        if ($request->has('is_male') && $request->is_male != '') {
            $user['is_male'] = 1;
            $user->avatar = setting('site.man');

        }
        if ($request->has('is_female')){
            $user['is_male'] = 0;
            $user->avatar = setting('site.female');
        }

        if ($request->has('email') && $request->email != ''){
            $user['email'] = $request->email;
        }
        if ($request->has('surname') && $request->email != ''){
            $user['surname'] = $request->surname;
        }
        session()->put('user',$user);
        session()->save();
        $user->save();

        return back();
    }


    public function orders()
    {
        $user = session()->get('user');
        $orders = Order::where('user_id',$user->id)
            ->with(['details.product','order_status'])
            ->orderBy('id','desc')
            ->get();


        return view('account.orders',['orders' => $orders]);
    }

    public function search($search)
    {
        if(strlen($search) > 3){
            $search = '%'.$search.'%';
        }else{
            $search = $search.'%';
        }
        $productNames = Product::where('name','LIKE',$search)
            ->orWhereJsonContains('additional_attributes',["$search"])
            ->distinct('name')->pluck('name');

        return response(['names' => $productNames],200);
    }

    public function orderMain($id)
    {

        $order = Order::with(['details.product','order_status','payment_type','returns.status'])->find($id);

        return view('account.order',['order' => $order]);
    }
    }
