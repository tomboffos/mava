<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryFilter extends Model
{
    public function attributes()
    {
        $this->belongsTo('App\Models\CategoryAttribute');
    }
    use HasFactory;
    use SoftDeletes;

}
