<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends Model
{

    public function size()
    {
        return $this->belongsTo('App\Models\Size');
    }
    use HasFactory;
    use SoftDeletes;

}
