<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainSection extends Model
{

    public function products_section()
    {
        return $this->hasMany('App\Models\MainSectionProduct');
    }
    use HasFactory;
    use SoftDeletes;

}
