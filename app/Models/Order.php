<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    protected $fillable = [
        'name',
        'phone',
        'address',
        'payment_type_id',
        'delivery_type_id',
        'order_status_id',
        'payment_status_id',
        'user_id',
        'name',
        'phone',
        'email',
        'address',
        'country_city',
        'flat',
        'mail_index',
        'unit_price',
        'unit_quantity',
        'delivery_price',
        'delivery_time',
        'order_number'
    ];


    public function details()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }

    public function order_status()
    {
        return $this->belongsTo('App\Models\OrderStatus');
    }

    public function payment_type()
    {
        return $this->belongsTo('App\Models\PaymentType');
    }
    public function payment_status()
    {
        return $this->belongsTo('App\Models\PaymentStatus');
    }

    public function returns()
    {
        return $this->hasMany('App\Models\PurchaseReturn');
    }
    use HasFactory;
    use SoftDeletes;

}
