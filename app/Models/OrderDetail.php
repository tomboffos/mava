<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{

    protected $fillable = [
        'order_id',
        'product_id',
        'color_id',
        'unit_quantity',
        'unit_price',
        'product_variation_id'
    ];


    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function variation()
    {
        return $this->belongsTo(ProductVariation::class,'product_variation_id','id');
    }
    use HasFactory;
    use SoftDeletes;

}
