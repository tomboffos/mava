<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public function variations()
    {
        return $this->hasMany('App\Models\ProductVariation');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ShopCategory','shop_category_id','id');
    }


    public function color_size()
    {
        return $this->hasMany('App\Models\Color');
    }

    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function product_flags()
    {
        return $this->hasMany('App\Models\ProductFlag');
    }

    protected $casts = [
        'additional_attributes' => 'array'
    ];
//    protected $casts = ['additional_attributes'];
    use HasFactory;
}
