<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductFlag extends Model
{

    use SoftDeletes;

    public function flag()
    {
        return $this->belongsTo('App\Models\Flag','flag_id','id');
    }
    use HasFactory;
}
