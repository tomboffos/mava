<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseReturn extends Model
{

    use SoftDeletes;

    use HasFactory;

    protected $fillable = [
      'return_reason',
      'name',
      'phone',
      'order_id',
      'color_id',
      'product_id',
      'user_id',
      'return_status_id'
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\ReturnStatus','return_status_id','id');
    }
}
