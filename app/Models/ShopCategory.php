<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopCategory extends Model
{
    use SoftDeletes;

    use HasFactory;

    public function attributes()
    {
        return $this->hasMany('App\Models\CategoryAttribute','shop_category_id','id');
    }


    public function parents()
    {
        return $this->belongsTo('App\Models\ShopCategory','id','parent_id');
    }


    public function children()
    {
        return $this->hasMany('App\Models\ShopCategory','parent_id','id');
    }
}
