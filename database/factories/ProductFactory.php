<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'shop_category_id' => 1,
            'display_product_type_id' => 1,
            'name' => Str::random(10),
            'stock' => 1,
            'image' => 'users/default.png',
            'slider_images' => Str::random(10),
            'description' => Str::random(10),
            'additional_attributes' => Str::random(10),
            'price' => 200,
        ];
    }
}
