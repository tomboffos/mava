<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_category_id')->constrained();
            $table->foreignId('display_product_type_id')->constrained();
            $table->string('name');
            $table->boolean('stock')->default(1);
            $table->integer('review_count')->default(0);
            $table->integer('review_point')->default(0);
            $table->text('image');
            $table->text('slider_images')->nullable();
            $table->longText('description')->nullable();
            $table->jsonb('additional_attributes')->nullable();
            $table->integer('price');
            $table->integer('discount_price')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
