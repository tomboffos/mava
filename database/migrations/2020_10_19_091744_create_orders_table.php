<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_type_id')->constrained();
            $table->foreignId('delivery_type_id')->constrained();
            $table->foreignId('order_status_id')->constrained();
            $table->foreignId('payment_status_id')->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->string('name');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('country_city')->nullable();
            $table->string('flat')->nullable();
            $table->string('mail_index')->nullable();
            $table->integer('unit_price');
            $table->integer('unit_quantity');
            $table->integer('delivery_price');
            $table->dateTime('delivery_time')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
