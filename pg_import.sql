--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14
-- Dumped by pg_dump version 10.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: attribute_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.attribute_types (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.attribute_types OWNER TO postgres;

--
-- Name: attribute_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.attribute_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attribute_types_id_seq OWNER TO postgres;

--
-- Name: attribute_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.attribute_types_id_seq OWNED BY public.attribute_types.id;


--
-- Name: banners; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banners (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    link character varying(255),
    image character varying(255) NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    percent character varying(255) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.banners OWNER TO postgres;

--
-- Name: banners_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banners_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banners_id_seq OWNER TO postgres;

--
-- Name: banners_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banners_id_seq OWNED BY public.banners.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    parent_id integer,
    "order" integer DEFAULT 1 NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: category_attributes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_attributes (
    id bigint NOT NULL,
    shop_category_id bigint NOT NULL,
    attribute_type_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    "values" text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    slug_name character varying(255) DEFAULT 'new_case'::character varying NOT NULL
);


ALTER TABLE public.category_attributes OWNER TO postgres;

--
-- Name: category_attributes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_attributes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_attributes_id_seq OWNER TO postgres;

--
-- Name: category_attributes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_attributes_id_seq OWNED BY public.category_attributes.id;


--
-- Name: category_filters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_filters (
    id bigint NOT NULL,
    shop_category_id bigint NOT NULL,
    category_attribute_id bigint NOT NULL,
    is_show boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.category_filters OWNER TO postgres;

--
-- Name: category_filters_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_filters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_filters_id_seq OWNER TO postgres;

--
-- Name: category_filters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_filters_id_seq OWNED BY public.category_filters.id;


--
-- Name: colors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.colors (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    size_id bigint NOT NULL,
    texture_image text NOT NULL,
    product_images text NOT NULL,
    in_stock boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.colors OWNER TO postgres;

--
-- Name: colors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.colors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.colors_id_seq OWNER TO postgres;

--
-- Name: colors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.colors_id_seq OWNED BY public.colors.id;


--
-- Name: data_rows; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_rows (
    id integer NOT NULL,
    data_type_id integer NOT NULL,
    field character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    required boolean DEFAULT false NOT NULL,
    browse boolean DEFAULT true NOT NULL,
    read boolean DEFAULT true NOT NULL,
    edit boolean DEFAULT true NOT NULL,
    add boolean DEFAULT true NOT NULL,
    delete boolean DEFAULT true NOT NULL,
    details text,
    "order" integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.data_rows OWNER TO postgres;

--
-- Name: data_rows_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.data_rows_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_rows_id_seq OWNER TO postgres;

--
-- Name: data_rows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.data_rows_id_seq OWNED BY public.data_rows.id;


--
-- Name: data_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_types (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    display_name_singular character varying(255) NOT NULL,
    display_name_plural character varying(255) NOT NULL,
    icon character varying(255),
    model_name character varying(255),
    description character varying(255),
    generate_permissions boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    server_side smallint DEFAULT '0'::smallint NOT NULL,
    controller character varying(255),
    policy_name character varying(255),
    details text
);


ALTER TABLE public.data_types OWNER TO postgres;

--
-- Name: data_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.data_types_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_types_id_seq OWNER TO postgres;

--
-- Name: data_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.data_types_id_seq OWNED BY public.data_types.id;


--
-- Name: delivery_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.delivery_types (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.delivery_types OWNER TO postgres;

--
-- Name: delivery_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.delivery_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delivery_types_id_seq OWNER TO postgres;

--
-- Name: delivery_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.delivery_types_id_seq OWNED BY public.delivery_types.id;


--
-- Name: display_product_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.display_product_types (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.display_product_types OWNER TO postgres;

--
-- Name: display_product_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.display_product_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.display_product_types_id_seq OWNER TO postgres;

--
-- Name: display_product_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.display_product_types_id_seq OWNED BY public.display_product_types.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: flags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flags (
    id bigint NOT NULL,
    color character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.flags OWNER TO postgres;

--
-- Name: flags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.flags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.flags_id_seq OWNER TO postgres;

--
-- Name: flags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.flags_id_seq OWNED BY public.flags.id;


--
-- Name: main_section_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_section_products (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    main_section_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.main_section_products OWNER TO postgres;

--
-- Name: main_section_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_section_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_section_products_id_seq OWNER TO postgres;

--
-- Name: main_section_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_section_products_id_seq OWNED BY public.main_section_products.id;


--
-- Name: main_sections; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_sections (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.main_sections OWNER TO postgres;

--
-- Name: main_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_sections_id_seq OWNER TO postgres;

--
-- Name: main_sections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_sections_id_seq OWNED BY public.main_sections.id;


--
-- Name: menu_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menu_items (
    id integer NOT NULL,
    menu_id integer,
    title character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    target character varying(255) DEFAULT '_self'::character varying NOT NULL,
    icon_class character varying(255),
    color character varying(255),
    parent_id integer,
    "order" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    route character varying(255),
    parameters text
);


ALTER TABLE public.menu_items OWNER TO postgres;

--
-- Name: menu_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menu_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_items_id_seq OWNER TO postgres;

--
-- Name: menu_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menu_items_id_seq OWNED BY public.menu_items.id;


--
-- Name: menus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menus (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.menus OWNER TO postgres;

--
-- Name: menus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menus_id_seq OWNER TO postgres;

--
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menus_id_seq OWNED BY public.menus.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_access_tokens (
    id character varying(100) NOT NULL,
    user_id bigint,
    client_id bigint NOT NULL,
    name character varying(255),
    scopes text,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_access_tokens OWNER TO postgres;

--
-- Name: oauth_auth_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_auth_codes (
    id character varying(100) NOT NULL,
    user_id bigint NOT NULL,
    client_id bigint NOT NULL,
    scopes text,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_auth_codes OWNER TO postgres;

--
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_clients (
    id bigint NOT NULL,
    user_id bigint,
    name character varying(255) NOT NULL,
    secret character varying(100),
    provider character varying(255),
    redirect text NOT NULL,
    personal_access_client boolean NOT NULL,
    password_client boolean NOT NULL,
    revoked boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_clients OWNER TO postgres;

--
-- Name: oauth_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oauth_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_clients_id_seq OWNER TO postgres;

--
-- Name: oauth_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oauth_clients_id_seq OWNED BY public.oauth_clients.id;


--
-- Name: oauth_personal_access_clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_personal_access_clients (
    id bigint NOT NULL,
    client_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_personal_access_clients OWNER TO postgres;

--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oauth_personal_access_clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth_personal_access_clients_id_seq OWNER TO postgres;

--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oauth_personal_access_clients_id_seq OWNED BY public.oauth_personal_access_clients.id;


--
-- Name: oauth_refresh_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oauth_refresh_tokens (
    id character varying(100) NOT NULL,
    access_token_id character varying(100) NOT NULL,
    revoked boolean NOT NULL,
    expires_at timestamp(0) without time zone
);


ALTER TABLE public.oauth_refresh_tokens OWNER TO postgres;

--
-- Name: order_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_details (
    id bigint NOT NULL,
    order_id bigint NOT NULL,
    product_id bigint NOT NULL,
    color_id bigint,
    unit_quantity integer NOT NULL,
    unit_price integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.order_details OWNER TO postgres;

--
-- Name: order_details_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_details_id_seq OWNER TO postgres;

--
-- Name: order_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_details_id_seq OWNED BY public.order_details.id;


--
-- Name: order_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_statuses (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.order_statuses OWNER TO postgres;

--
-- Name: order_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.order_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_statuses_id_seq OWNER TO postgres;

--
-- Name: order_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.order_statuses_id_seq OWNED BY public.order_statuses.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id bigint NOT NULL,
    payment_type_id bigint NOT NULL,
    delivery_type_id bigint NOT NULL,
    order_status_id bigint NOT NULL,
    payment_status_id bigint NOT NULL,
    user_id bigint,
    name character varying(255) NOT NULL,
    phone character varying(255) NOT NULL,
    email character varying(255),
    address character varying(255),
    country_city character varying(255),
    flat character varying(255),
    mail_index character varying(255),
    unit_price integer NOT NULL,
    unit_quantity integer NOT NULL,
    delivery_price integer NOT NULL,
    delivery_time timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pages (
    id integer NOT NULL,
    author_id integer NOT NULL,
    title character varying(255) NOT NULL,
    excerpt text,
    body text,
    image character varying(255),
    slug character varying(255) NOT NULL,
    meta_description text,
    meta_keywords text,
    status character varying(255) DEFAULT 'INACTIVE'::character varying NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT pages_status_check CHECK (((status)::text = ANY ((ARRAY['ACTIVE'::character varying, 'INACTIVE'::character varying])::text[])))
);


ALTER TABLE public.pages OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_id_seq OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pages_id_seq OWNED BY public.pages.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: payment_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_statuses (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.payment_statuses OWNER TO postgres;

--
-- Name: payment_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_statuses_id_seq OWNER TO postgres;

--
-- Name: payment_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_statuses_id_seq OWNED BY public.payment_statuses.id;


--
-- Name: payment_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_types (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.payment_types OWNER TO postgres;

--
-- Name: payment_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payment_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_types_id_seq OWNER TO postgres;

--
-- Name: payment_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payment_types_id_seq OWNED BY public.payment_types.id;


--
-- Name: permission_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permission_role (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.permission_role OWNER TO postgres;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    key character varying(255) NOT NULL,
    table_name character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.posts (
    id integer NOT NULL,
    author_id integer NOT NULL,
    category_id integer,
    title character varying(255) NOT NULL,
    seo_title character varying(255),
    excerpt text,
    body text NOT NULL,
    image character varying(255),
    slug character varying(255) NOT NULL,
    meta_description text,
    meta_keywords text,
    status character varying(255) DEFAULT 'DRAFT'::character varying NOT NULL,
    featured boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT posts_status_check CHECK (((status)::text = ANY ((ARRAY['PUBLISHED'::character varying, 'DRAFT'::character varying, 'PENDING'::character varying])::text[])))
);


ALTER TABLE public.posts OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: product_flags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_flags (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    flag_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.product_flags OWNER TO postgres;

--
-- Name: product_flags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_flags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_flags_id_seq OWNER TO postgres;

--
-- Name: product_flags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_flags_id_seq OWNED BY public.product_flags.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id bigint NOT NULL,
    shop_category_id bigint NOT NULL,
    display_product_type_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    stock boolean DEFAULT true NOT NULL,
    review_count integer DEFAULT 0 NOT NULL,
    review_point integer DEFAULT 0 NOT NULL,
    image text NOT NULL,
    slider_images text,
    description text,
    additional_attributes text,
    price integer NOT NULL,
    discount_price integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: purchase_returns; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchase_returns (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    product_id bigint NOT NULL,
    color_id bigint NOT NULL,
    order_id bigint NOT NULL,
    return_status_id bigint DEFAULT '1'::bigint NOT NULL,
    name character varying(255),
    phone character varying(255),
    return_reason character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.purchase_returns OWNER TO postgres;

--
-- Name: purchase_returns_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.purchase_returns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.purchase_returns_id_seq OWNER TO postgres;

--
-- Name: purchase_returns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.purchase_returns_id_seq OWNED BY public.purchase_returns.id;


--
-- Name: return_statuses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.return_statuses (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.return_statuses OWNER TO postgres;

--
-- Name: return_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.return_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.return_statuses_id_seq OWNER TO postgres;

--
-- Name: return_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.return_statuses_id_seq OWNED BY public.return_statuses.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    advantages character varying(255),
    disadvantages character varying(255),
    comments character varying(255),
    review integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    product_id bigint NOT NULL
);


ALTER TABLE public.reviews OWNER TO postgres;

--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq OWNER TO postgres;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_id_seq OWNED BY public.reviews.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    key character varying(255) NOT NULL,
    display_name character varying(255) NOT NULL,
    value text,
    details text,
    type character varying(255) NOT NULL,
    "order" integer DEFAULT 1 NOT NULL,
    "group" character varying(255)
);


ALTER TABLE public.settings OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO postgres;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: shop_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shop_categories (
    id bigint NOT NULL,
    parent_id integer,
    "order" integer DEFAULT 1 NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    image text NOT NULL,
    icon character varying(255)
);


ALTER TABLE public.shop_categories OWNER TO postgres;

--
-- Name: shop_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.shop_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shop_categories_id_seq OWNER TO postgres;

--
-- Name: shop_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.shop_categories_id_seq OWNED BY public.shop_categories.id;


--
-- Name: sizes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sizes (
    id bigint NOT NULL,
    is_shoes_size boolean DEFAULT false NOT NULL,
    size character varying(255) NOT NULL,
    in_stock boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.sizes OWNER TO postgres;

--
-- Name: sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sizes_id_seq OWNER TO postgres;

--
-- Name: sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sizes_id_seq OWNED BY public.sizes.id;


--
-- Name: translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.translations (
    id integer NOT NULL,
    table_name character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    locale character varying(255) NOT NULL,
    value text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.translations OWNER TO postgres;

--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.translations_id_seq OWNER TO postgres;

--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.translations_id_seq OWNED BY public.translations.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_roles (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.user_roles OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    avatar character varying(255) DEFAULT 'users/default.png'::character varying,
    role_id bigint,
    settings text
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: attribute_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.attribute_types ALTER COLUMN id SET DEFAULT nextval('public.attribute_types_id_seq'::regclass);


--
-- Name: banners id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banners ALTER COLUMN id SET DEFAULT nextval('public.banners_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: category_attributes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_attributes ALTER COLUMN id SET DEFAULT nextval('public.category_attributes_id_seq'::regclass);


--
-- Name: category_filters id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_filters ALTER COLUMN id SET DEFAULT nextval('public.category_filters_id_seq'::regclass);


--
-- Name: colors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.colors ALTER COLUMN id SET DEFAULT nextval('public.colors_id_seq'::regclass);


--
-- Name: data_rows id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_rows ALTER COLUMN id SET DEFAULT nextval('public.data_rows_id_seq'::regclass);


--
-- Name: data_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types ALTER COLUMN id SET DEFAULT nextval('public.data_types_id_seq'::regclass);


--
-- Name: delivery_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delivery_types ALTER COLUMN id SET DEFAULT nextval('public.delivery_types_id_seq'::regclass);


--
-- Name: display_product_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.display_product_types ALTER COLUMN id SET DEFAULT nextval('public.display_product_types_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: flags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flags ALTER COLUMN id SET DEFAULT nextval('public.flags_id_seq'::regclass);


--
-- Name: main_section_products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_section_products ALTER COLUMN id SET DEFAULT nextval('public.main_section_products_id_seq'::regclass);


--
-- Name: main_sections id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_sections ALTER COLUMN id SET DEFAULT nextval('public.main_sections_id_seq'::regclass);


--
-- Name: menu_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items ALTER COLUMN id SET DEFAULT nextval('public.menu_items_id_seq'::regclass);


--
-- Name: menus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menus ALTER COLUMN id SET DEFAULT nextval('public.menus_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: oauth_clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_clients_id_seq'::regclass);


--
-- Name: oauth_personal_access_clients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_personal_access_clients ALTER COLUMN id SET DEFAULT nextval('public.oauth_personal_access_clients_id_seq'::regclass);


--
-- Name: order_details id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_details ALTER COLUMN id SET DEFAULT nextval('public.order_details_id_seq'::regclass);


--
-- Name: order_statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_statuses ALTER COLUMN id SET DEFAULT nextval('public.order_statuses_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: pages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages ALTER COLUMN id SET DEFAULT nextval('public.pages_id_seq'::regclass);


--
-- Name: payment_statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_statuses ALTER COLUMN id SET DEFAULT nextval('public.payment_statuses_id_seq'::regclass);


--
-- Name: payment_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_types ALTER COLUMN id SET DEFAULT nextval('public.payment_types_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);


--
-- Name: product_flags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_flags ALTER COLUMN id SET DEFAULT nextval('public.product_flags_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: purchase_returns id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns ALTER COLUMN id SET DEFAULT nextval('public.purchase_returns_id_seq'::regclass);


--
-- Name: return_statuses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.return_statuses ALTER COLUMN id SET DEFAULT nextval('public.return_statuses_id_seq'::regclass);


--
-- Name: reviews id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: shop_categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shop_categories ALTER COLUMN id SET DEFAULT nextval('public.shop_categories_id_seq'::regclass);


--
-- Name: sizes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sizes ALTER COLUMN id SET DEFAULT nextval('public.sizes_id_seq'::regclass);


--
-- Name: translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations ALTER COLUMN id SET DEFAULT nextval('public.translations_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: attribute_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.attribute_types (id, name, value, created_at, updated_at) FROM stdin;
1	Выборочный аттрибут	select	2020-10-22 08:19:13	2020-10-22 08:19:13
2	Аттрибут да\\нет	checkbox	2020-10-22 08:19:50	2020-10-22 08:19:50
\.


--
-- Data for Name: banners; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banners (id, name, link, image, "order", created_at, updated_at, percent) FROM stdin;
1	на кухонные принадлежности	http://127.0.0.1:8000/shop/2	banners\\November2020\\ANbPXybS9D9xEo3SqvTX.png	1	2020-11-05 07:37:28	2020-11-05 11:27:31	10%
\.


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, parent_id, "order", name, slug, created_at, updated_at) FROM stdin;
1	\N	1	Category 1	category-1	2020-10-15 10:12:55	2020-10-15 10:12:55
2	\N	1	Category 2	category-2	2020-10-15 10:12:55	2020-10-15 10:12:55
\.


--
-- Data for Name: category_attributes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_attributes (id, shop_category_id, attribute_type_id, name, "values", created_at, updated_at, slug_name) FROM stdin;
1	2	1	Материал	Хлопок,Кожа,Дермантин	2020-10-22 08:21:27	2020-10-22 08:21:27	new_case
3	4	1	Тип масла	Синтетическое,Моторное	2020-11-05 11:32:47	2020-11-05 11:32:47	tip-masla
\.


--
-- Data for Name: category_filters; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_filters (id, shop_category_id, category_attribute_id, is_show, created_at, updated_at) FROM stdin;
1	2	1	t	2020-11-05 08:44:11	2020-11-05 08:44:11
2	4	3	t	2020-11-05 12:22:02	2020-11-05 12:22:02
\.


--
-- Data for Name: colors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.colors (id, product_id, size_id, texture_image, product_images, in_stock, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: data_rows; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_rows (id, data_type_id, field, type, display_name, required, browse, read, edit, add, delete, details, "order") FROM stdin;
1	1	id	number	ID	t	f	f	f	f	f	\N	1
2	1	name	text	Name	t	t	t	t	t	t	\N	2
3	1	email	text	Email	t	t	t	t	t	t	\N	3
4	1	password	password	Password	t	f	f	t	t	f	\N	4
5	1	remember_token	text	Remember Token	f	f	f	f	f	f	\N	5
6	1	created_at	timestamp	Created At	f	t	t	f	f	f	\N	6
7	1	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	7
8	1	avatar	image	Avatar	f	t	t	t	t	t	\N	8
9	1	user_belongsto_role_relationship	relationship	Role	f	t	t	t	t	f	{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"display_name","pivot_table":"roles","pivot":0}	10
10	1	user_belongstomany_role_relationship	relationship	Roles	f	t	t	t	t	f	{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsToMany","column":"id","key":"id","label":"display_name","pivot_table":"user_roles","pivot":"1","taggable":"0"}	11
11	1	settings	hidden	Settings	f	f	f	f	f	f	\N	12
12	2	id	number	ID	t	f	f	f	f	f	\N	1
13	2	name	text	Name	t	t	t	t	t	t	\N	2
14	2	created_at	timestamp	Created At	f	f	f	f	f	f	\N	3
15	2	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	4
16	3	id	number	ID	t	f	f	f	f	f	\N	1
17	3	name	text	Name	t	t	t	t	t	t	\N	2
18	3	created_at	timestamp	Created At	f	f	f	f	f	f	\N	3
19	3	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	4
20	3	display_name	text	Display Name	t	t	t	t	t	t	\N	5
21	1	role_id	text	Role	t	t	t	t	t	t	\N	9
22	4	id	number	ID	t	f	f	f	f	f	\N	1
23	4	parent_id	select_dropdown	Parent	f	f	t	t	t	t	{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}	2
24	4	order	text	Order	t	t	t	t	t	t	{"default":1}	3
25	4	name	text	Name	t	t	t	t	t	t	\N	4
26	4	slug	text	Slug	t	t	t	t	t	t	{"slugify":{"origin":"name"}}	5
27	4	created_at	timestamp	Created At	f	f	t	f	f	f	\N	6
28	4	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	7
29	5	id	number	ID	t	f	f	f	f	f	\N	1
30	5	author_id	text	Author	t	f	t	t	f	t	\N	2
31	5	category_id	text	Category	t	f	t	t	t	f	\N	3
32	5	title	text	Title	t	t	t	t	t	t	\N	4
33	5	excerpt	text_area	Excerpt	t	f	t	t	t	t	\N	5
34	5	body	rich_text_box	Body	t	f	t	t	t	t	\N	6
35	5	image	image	Post Image	f	t	t	t	t	t	{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}	7
36	5	slug	text	Slug	t	f	t	t	t	t	{"slugify":{"origin":"title","forceUpdate":true},"validation":{"rule":"unique:posts,slug"}}	8
37	5	meta_description	text_area	Meta Description	t	f	t	t	t	t	\N	9
38	5	meta_keywords	text_area	Meta Keywords	t	f	t	t	t	t	\N	10
39	5	status	select_dropdown	Status	t	t	t	t	t	t	{"default":"DRAFT","options":{"PUBLISHED":"published","DRAFT":"draft","PENDING":"pending"}}	11
40	5	created_at	timestamp	Created At	f	t	t	f	f	f	\N	12
41	5	updated_at	timestamp	Updated At	f	f	f	f	f	f	\N	13
42	5	seo_title	text	SEO Title	f	t	t	t	t	t	\N	14
43	5	featured	checkbox	Featured	t	t	t	t	t	t	\N	15
44	6	id	number	ID	t	f	f	f	f	f	\N	1
45	6	author_id	text	Author	t	f	f	f	f	f	\N	2
46	6	title	text	Title	t	t	t	t	t	t	\N	3
47	6	excerpt	text_area	Excerpt	t	f	t	t	t	t	\N	4
48	6	body	rich_text_box	Body	t	f	t	t	t	t	\N	5
49	6	slug	text	Slug	t	f	t	t	t	t	{"slugify":{"origin":"title"},"validation":{"rule":"unique:pages,slug"}}	6
50	6	meta_description	text	Meta Description	t	f	t	t	t	t	\N	7
51	6	meta_keywords	text	Meta Keywords	t	f	t	t	t	t	\N	8
52	6	status	select_dropdown	Status	t	t	t	t	t	t	{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}	9
53	6	created_at	timestamp	Created At	t	t	t	f	f	f	\N	10
54	6	updated_at	timestamp	Updated At	t	f	f	f	f	f	\N	11
55	6	image	image	Page Image	f	t	t	t	t	t	\N	12
56	8	id	text	Id	t	f	f	f	f	f	{}	1
57	8	parent_id	text	Родительская категория	f	t	t	t	t	t	{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}	2
58	8	order	text	Порядок	t	t	t	t	t	t	{"default":1}	3
59	8	name	text	Название	t	t	t	t	t	t	{}	4
60	8	slug	text	Уникальное имя	t	t	t	t	t	t	{"slugify":{"origin":"name"},"validation":{"rule":"unique:shop_categories,slug","messages":{"unique":"\\u041f\\u043e\\u0436\\u0430\\u043b\\u0443\\u0439\\u0441\\u0442\\u0430 \\u0432\\u0432\\u0435\\u0434\\u0438\\u0442\\u0435 \\u0443\\u043d\\u0438\\u043a\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435 \\u0438\\u043c\\u044f."}}}	5
61	8	created_at	timestamp	Создано	f	f	t	f	f	f	{}	7
62	8	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	8
63	8	image	image	Фото	t	t	t	t	t	t	{"resize":{"width":"1000","height":"1000"},"quality":"70%"}	6
64	8	shop_category_belongsto_shop_category_relationship	relationship	shop_categories	f	t	t	t	t	t	{"model":"App\\\\Models\\\\ShopCategory","table":"shop_categories","type":"belongsTo","column":"parent_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	9
65	10	id	text	Id	t	f	f	f	f	f	{}	1
69	10	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	5
70	12	id	text	Id	t	f	f	f	f	f	{}	1
66	10	name	text	Название аттрибута	t	t	t	t	t	t	{}	2
67	10	value	text	Значение	f	t	t	t	t	t	{}	3
68	10	created_at	timestamp	Created At	f	t	t	f	f	f	{}	4
75	12	created_at	timestamp	Created At	f	t	t	f	f	f	{}	6
76	12	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	7
71	12	shop_category_id	text	Категория	t	t	t	t	t	t	{}	2
73	12	name	text	Название	t	t	t	t	t	t	{}	4
112	19	created_at	timestamp	Создано	f	t	t	t	f	t	{}	7
113	19	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	8
72	12	attribute_type_id	text	Тип аттрибута	t	t	t	t	t	t	{}	3
74	12	values	text	Значение	f	t	t	t	t	t	{}	5
77	12	category_attribute_belongsto_attribute_type_relationship	relationship	Тип аттрибута	f	t	t	t	t	t	{"model":"App\\\\Models\\\\AttributeType","table":"attribute_types","type":"belongsTo","column":"attribute_type_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	8
78	12	category_attribute_belongsto_shop_category_relationship	relationship	Категория	f	t	t	t	t	t	{"model":"App\\\\Models\\\\ShopCategory","table":"shop_categories","type":"belongsTo","column":"shop_category_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	9
79	14	id	text	Id	t	f	f	f	f	f	{}	1
80	14	name	text	Название	t	t	t	t	t	t	{}	2
81	14	created_at	timestamp	Created At	f	t	t	t	f	t	{}	3
82	14	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	4
83	16	id	text	Id	t	f	f	f	f	f	{}	1
84	16	shop_category_id	text	Shop Category Id	t	t	t	t	t	t	{}	2
85	16	display_product_type_id	text	Display Product Type Id	t	t	t	t	t	t	{}	3
86	16	name	text	Название	t	t	t	t	t	t	{}	4
87	16	stock	checkbox	В наличии	t	t	t	t	t	t	{}	5
88	16	review_count	text	Количество отзывов	t	t	t	f	f	f	{}	6
89	16	review_point	text	Рейтинг отзывов	t	t	t	f	f	f	{}	7
94	16	price	number	Цена	t	t	t	t	t	t	{}	12
95	16	discount_price	number	Скидочная цена	f	t	t	t	t	t	{}	13
96	16	created_at	timestamp	Создано	f	t	t	t	f	t	{}	14
97	16	updated_at	timestamp	Обновлено	f	f	f	f	f	f	{}	15
90	16	image	image	Фото	t	t	t	t	t	t	{"resize":{"width":"1000","height":"1000"},"quality":"70%"}	8
91	16	slider_images	multiple_images	Фото слайдеров	f	t	t	t	t	t	{"resize":{"width":"1000","height":"1000"},"quality":"70%"}	9
98	16	product_belongsto_shop_category_relationship	relationship	Категория продукта	f	t	t	t	t	t	{"model":"App\\\\Models\\\\ShopCategory","table":"shop_categories","type":"belongsTo","column":"shop_category_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	16
99	16	product_belongsto_display_product_type_relationship	relationship	Тип отображения продукта	f	t	t	t	t	t	{"model":"App\\\\Models\\\\DisplayProductType","table":"display_product_types","type":"belongsTo","column":"display_product_type_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	17
100	18	id	text	Id	t	f	f	f	f	f	{}	1
101	18	is_shoes_size	checkbox	Размер обуви или нет?	t	t	t	t	t	t	{}	2
102	18	size	text	Размер	t	t	t	t	t	t	{}	3
105	18	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	6
103	18	in_stock	checkbox	In Stock	t	f	f	f	f	f	{}	4
104	18	created_at	timestamp	Создано	f	t	t	t	f	t	{}	5
106	19	id	text	Id	t	f	f	f	f	f	{}	1
107	19	product_id	text	Product Id	t	t	t	t	t	t	{}	2
108	19	size_id	text	Size Id	t	t	t	t	t	t	{}	3
109	19	texture_image	image	Фото текстуры	t	t	t	t	t	t	{}	4
110	19	product_images	multiple_images	Фото товара в цвете	t	t	t	t	t	t	{}	5
114	19	color_belongsto_product_relationship	relationship	Продукт	f	t	t	t	t	t	{"model":"App\\\\Models\\\\Product","table":"products","type":"belongsTo","column":"product_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	9
128	23	product_id	text	Product Id	t	t	t	t	t	t	{}	2
115	19	color_belongsto_size_relationship	relationship	Размер	f	t	t	t	t	t	{"model":"App\\\\Models\\\\Size","table":"sizes","type":"belongsTo","column":"size_id","key":"id","label":"size","pivot_table":"migrations","pivot":"0","taggable":"0"}	10
111	19	in_stock	checkbox	В наличии	t	t	t	t	t	t	{}	6
129	23	flag_id	text	Flag Id	t	t	t	t	t	t	{}	3
130	23	created_at	timestamp	Created At	f	f	f	f	f	f	{}	4
93	16	additional_attributes	text	Дополнительные аттрибуты	f	f	f	t	t	t	{}	11
116	21	id	text	Id	t	f	f	f	f	f	{}	1
117	21	color	color	Цвет	t	t	t	t	t	t	{}	2
118	21	name	text	Название	t	t	t	t	t	t	{}	3
119	21	order	text	Порядок	t	t	t	f	f	f	{}	4
120	21	created_at	timestamp	Created At	f	f	f	f	f	f	{}	5
121	21	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	6
122	22	id	text	Id	t	f	f	f	f	f	{}	1
123	22	name	text	Название	t	t	t	t	t	t	{}	2
124	22	order	text	Порядок	t	t	t	t	t	t	{}	3
125	22	created_at	timestamp	Created At	f	t	t	t	f	t	{}	4
126	22	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	5
127	23	id	text	Id	t	f	f	f	f	f	{}	1
131	23	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	5
135	25	product_id	text	Product Id	t	t	t	t	t	t	{}	2
133	23	product_flag_belongsto_flag_relationship	relationship	Флажки	f	t	t	t	t	t	{"model":"App\\\\Models\\\\Flag","table":"flags","type":"belongsTo","column":"flag_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	7
132	23	product_flag_belongsto_product_relationship	relationship	Продукт	f	t	t	t	t	t	{"model":"App\\\\Models\\\\Product","table":"products","type":"belongsTo","column":"product_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	6
134	25	id	text	Id	t	f	f	f	f	f	{}	1
136	25	main_section_id	text	Main Section Id	t	t	t	t	t	t	{}	3
137	25	created_at	timestamp	Created At	f	t	t	t	f	t	{}	4
138	25	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	5
92	16	description	rich_text_box	Описание	f	t	t	t	t	t	{}	10
139	25	main_section_product_belongsto_product_relationship	relationship	products	f	t	t	t	t	t	{"model":"App\\\\Models\\\\Product","table":"products","type":"belongsTo","column":"product_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	6
140	25	main_section_product_belongsto_main_section_relationship	relationship	main_sections	f	t	t	t	t	t	{"model":"App\\\\Models\\\\MainSection","table":"main_sections","type":"belongsTo","column":"main_section_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	7
141	26	id	text	Id	t	f	f	f	f	f	{}	1
142	26	name	text	Название	t	t	t	t	t	t	{}	2
143	26	link	text	Ссылка	f	t	t	t	t	t	{}	3
144	26	image	image	Фото	t	t	t	t	t	t	{}	4
145	26	order	text	Порядк	t	t	t	t	t	t	{}	5
146	26	created_at	timestamp	Created At	f	f	f	f	f	f	{}	7
147	26	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	8
148	26	percent	text	Процент	t	t	t	t	t	t	{}	6
149	28	id	text	Id	t	f	f	f	f	f	{}	1
150	28	shop_category_id	text	Shop Category Id	t	t	t	t	t	t	{}	2
151	28	category_attribute_id	text	Category Attribute Id	t	t	t	t	t	t	{}	3
153	28	created_at	timestamp	Created At	f	t	t	t	f	t	{}	5
154	28	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	6
155	28	category_filter_belongsto_shop_category_relationship	relationship	Категория	f	t	t	t	t	t	{"model":"App\\\\Models\\\\ShopCategory","table":"shop_categories","type":"belongsTo","column":"shop_category_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	7
156	28	category_filter_belongsto_category_attribute_relationship	relationship	Аттрибуты категории	f	t	t	t	t	t	{"model":"App\\\\Models\\\\CategoryAttribute","table":"category_attributes","type":"belongsTo","column":"category_attribute_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	8
152	28	is_show	checkbox	Показывать	t	t	t	t	t	t	{}	4
157	12	slug_name	text	Слаг	t	t	t	t	t	t	{"slugify":{"origin":"name"},"validation":{"rule":"unique:shop_categories,slug","messages":{"unique":"\\u041f\\u043e\\u0436\\u0430\\u043b\\u0443\\u0439\\u0441\\u0442\\u0430 \\u0432\\u0432\\u0435\\u0434\\u0438\\u0442\\u0435 \\u0443\\u043d\\u0438\\u043a\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435 \\u0438\\u043c\\u044f."}}}	8
158	29	id	text	Id	t	f	f	f	f	f	{}	1
159	29	name	text	Название	t	t	t	t	t	t	{}	2
160	29	created_at	timestamp	Created At	f	f	f	f	f	f	{}	3
161	29	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	4
162	30	id	text	Id	t	f	f	f	f	f	{}	1
163	30	name	text	Название	t	t	t	t	t	t	{}	2
164	30	created_at	timestamp	Created At	f	t	t	t	f	t	{}	3
165	30	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	4
166	31	id	text	Id	t	f	f	f	f	f	{}	1
167	31	name	text	Название	t	t	t	t	t	t	{}	2
168	31	created_at	timestamp	Created At	f	f	f	f	f	f	{}	3
169	31	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	4
170	33	id	text	Id	t	f	f	f	f	f	{}	1
171	33	name	text	Название	t	t	t	t	t	t	{}	2
172	33	created_at	timestamp	Created At	f	f	f	f	f	f	{}	3
173	33	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	4
174	34	id	text	Id	t	f	f	f	f	f	{}	1
175	34	payment_type_id	text	Payment Type Id	t	t	t	t	t	t	{}	2
176	34	delivery_type_id	text	Delivery Type Id	t	t	t	t	t	t	{}	3
177	34	order_status_id	text	Order Status Id	t	t	t	t	t	t	{}	4
178	34	payment_status_id	text	Payment Status Id	t	t	t	t	t	t	{}	5
179	34	user_id	text	User Id	f	t	t	t	t	t	{}	6
191	34	created_at	timestamp	Created At	f	f	f	f	f	f	{}	18
192	34	updated_at	timestamp	Updated At	f	f	f	f	f	f	{}	19
180	34	name	text	Имя	t	t	t	t	t	t	{}	7
181	34	phone	text	Телефон	t	t	t	t	t	t	{}	8
182	34	email	text	Почта	f	t	t	t	t	t	{}	9
183	34	address	text	Адрес	f	t	t	t	t	t	{}	10
184	34	country_city	text	Страна город	f	t	t	t	t	t	{}	11
185	34	flat	text	Подьезд	f	t	t	t	t	t	{}	12
186	34	mail_index	text	Почтовый индекс	f	t	t	t	t	t	{}	13
187	34	unit_price	text	Общая цена	t	t	t	t	t	t	{}	14
188	34	unit_quantity	text	Общое количество	t	t	t	t	t	t	{}	15
189	34	delivery_price	text	Цена доставки	t	t	t	t	t	t	{}	16
190	34	delivery_time	timestamp	Время доставки	f	t	t	t	t	t	{}	17
193	34	order_belongsto_payment_type_relationship	relationship	Тип оплаты	f	t	t	t	t	t	{"model":"App\\\\Models\\\\PaymentType","table":"payment_types","type":"belongsTo","column":"payment_type_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	20
194	34	order_belongsto_delivery_type_relationship	relationship	Тип доставки	f	t	t	t	t	t	{"model":"App\\\\Models\\\\DeliveryType","table":"delivery_types","type":"belongsTo","column":"delivery_type_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	21
195	34	order_belongsto_order_status_relationship	relationship	Статус заказа	f	t	t	t	t	t	{"model":"App\\\\Models\\\\OrderStatus","table":"order_statuses","type":"belongsTo","column":"order_status_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	22
196	34	order_belongsto_payment_status_relationship	relationship	Статус оплаты	f	t	t	t	t	t	{"model":"App\\\\Models\\\\PaymentStatus","table":"payment_statuses","type":"belongsTo","column":"payment_status_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	23
197	34	order_belongsto_user_relationship	relationship	Пользователь	f	t	t	t	t	t	{"model":"App\\\\Models\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"migrations","pivot":"0","taggable":"0"}	24
\.


--
-- Data for Name: data_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_types (id, name, slug, display_name_singular, display_name_plural, icon, model_name, description, generate_permissions, created_at, updated_at, server_side, controller, policy_name, details) FROM stdin;
1	users	users	User	Users	voyager-person	TCG\\Voyager\\Models\\User		t	2020-10-15 10:12:55	2020-10-15 10:12:55	0	TCG\\Voyager\\Http\\Controllers\\VoyagerUserController	TCG\\Voyager\\Policies\\UserPolicy	\N
2	menus	menus	Menu	Menus	voyager-list	TCG\\Voyager\\Models\\Menu		t	2020-10-15 10:12:55	2020-10-15 10:12:55	0		\N	\N
3	roles	roles	Role	Roles	voyager-lock	TCG\\Voyager\\Models\\Role		t	2020-10-15 10:12:55	2020-10-15 10:12:55	0	TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController	\N	\N
4	categories	categories	Category	Categories	voyager-categories	TCG\\Voyager\\Models\\Category		t	2020-10-15 10:12:55	2020-10-15 10:12:55	0		\N	\N
5	posts	posts	Post	Posts	voyager-news	TCG\\Voyager\\Models\\Post		t	2020-10-15 10:12:55	2020-10-15 10:12:55	0		TCG\\Voyager\\Policies\\PostPolicy	\N
6	pages	pages	Page	Pages	voyager-file-text	TCG\\Voyager\\Models\\Page		t	2020-10-15 10:12:56	2020-10-15 10:12:56	0		\N	\N
19	colors	colors	Цвет и размер одежды	Цвета и размеры одежды	\N	App\\Models\\Color	\N	t	2020-10-21 10:49:27	2020-10-21 11:02:32	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
8	shop_categories	shop-categories	Shop Category	Shop Categories	\N	App\\Models\\ShopCategory	\N	t	2020-10-15 10:31:10	2020-10-16 04:38:03	0	\N	\N	{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null,"scope":null}
16	products	products	Товар	Товары	\N	App\\Models\\Product	\N	t	2020-10-21 09:31:02	2020-11-05 17:44:15	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
10	attribute_types	attribute-types	Тип аттрибутов	Типы аттрибутов	\N	App\\Models\\AttributeType	\N	t	2020-10-16 04:47:00	2020-10-16 04:48:22	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
29	payment_statuses	payment-statuses	Статус оплаты	Статусы оплаты	\N	App\\Models\\PaymentStatus	\N	t	2020-11-05 19:38:30	2020-11-05 19:38:30	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
30	order_statuses	order-statuses	Статус заказа	Статусы заказа	\N	App\\Models\\OrderStatus	\N	t	2020-11-05 19:39:01	2020-11-05 19:39:01	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
14	display_product_types	display-product-types	Тип показа продукта	Тип показа продукта	\N	App\\Models\\DisplayProductType	\N	t	2020-10-16 04:55:27	2020-10-16 04:55:27	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
31	delivery_types	delivery-types	Тип получения товаров	Типы получения товаров	\N	App\\Models\\DeliveryType	\N	t	2020-11-05 19:40:07	2020-11-05 19:40:07	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
18	sizes	sizes	Размер	Размеры	\N	App\\Models\\Size	\N	t	2020-10-21 09:52:44	2020-10-21 09:53:23	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
28	category_filters	category-filters	Category Filter	Category Filters	\N	App\\Models\\CategoryFilter	\N	t	2020-11-05 08:23:18	2020-11-05 08:46:16	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
21	flags	flags	Флаг	Флаги	\N	App\\Models\\Flag	\N	t	2020-11-05 05:39:21	2020-11-05 05:39:21	0	\N	\N	{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null}
22	main_sections	main-sections	Секция в главной	Секции в главной	\N	App\\Models\\MainSection	\N	t	2020-11-05 05:40:53	2020-11-05 05:40:53	0	\N	\N	{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null}
12	category_attributes	category-attributes	Аттрибут категории	Атрибуты категории	\N	App\\Models\\CategoryAttribute	\N	t	2020-10-16 04:49:24	2020-11-05 11:32:18	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
23	product_flags	product-flags	Флаг Продукта	Флаги продукта	\N	App\\Models\\ProductFlag	\N	t	2020-11-05 05:42:00	2020-11-05 05:44:09	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
25	main_section_products	main-section-products	Продукт в секции	Продукты в секции	\N	App\\Models\\MainSectionProduct	\N	t	2020-11-05 06:03:23	2020-11-05 06:04:21	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
26	banners	banners	Баннер	Баннеры	\N	App\\Models\\Banner	\N	t	2020-11-05 07:34:40	2020-11-05 07:36:19	0	\N	\N	{"order_column":"order","order_display_column":"name","order_direction":"asc","default_search_key":null,"scope":null}
33	payment_types	payment-types	Тип оплаты	Типы оплаты	\N	App\\Models\\PaymentType	\N	t	2020-11-05 19:42:13	2020-11-05 19:42:13	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null}
34	orders	orders	Заказ	Заказы	\N	App\\Models\\Order	\N	t	2020-11-06 03:24:50	2020-11-06 03:37:21	0	\N	\N	{"order_column":null,"order_display_column":null,"order_direction":"asc","default_search_key":null,"scope":null}
\.


--
-- Data for Name: delivery_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.delivery_types (id, name, created_at, updated_at) FROM stdin;
1	Доставка	2020-11-05 19:40:22	2020-11-05 19:40:22
2	Самовывоз	2020-11-05 19:41:15	2020-11-05 19:41:15
\.


--
-- Data for Name: display_product_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.display_product_types (id, name, created_at, updated_at) FROM stdin;
2	Техника (отображение технических характеристик)	2020-10-21 09:34:58	2020-10-21 09:34:58
1	Одежда и обувь (отображение цвета и размера)	2020-10-21 09:34:00	2020-10-21 09:35:22
3	Книги (отображение описание)	2020-10-21 09:35:44	2020-10-21 09:35:44
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: flags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.flags (id, color, name, "order", created_at, updated_at) FROM stdin;
1	#454fd9	Новая	1	2020-11-05 05:39:42	2020-11-05 05:39:42
\.


--
-- Data for Name: main_section_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_section_products (id, product_id, main_section_id, created_at, updated_at) FROM stdin;
1	1	1	2020-11-05 06:04:00	2020-11-05 07:41:40
\.


--
-- Data for Name: main_sections; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_sections (id, name, "order", created_at, updated_at) FROM stdin;
1	Секция	1	2020-11-05 06:04:43	2020-11-05 06:04:43
\.


--
-- Data for Name: menu_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menu_items (id, menu_id, title, url, target, icon_class, color, parent_id, "order", created_at, updated_at, route, parameters) FROM stdin;
1	1	Dashboard		_self	voyager-boat	\N	\N	1	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.dashboard	\N
2	1	Media		_self	voyager-images	\N	\N	5	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.media.index	\N
3	1	Users		_self	voyager-person	\N	\N	3	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.users.index	\N
4	1	Roles		_self	voyager-lock	\N	\N	2	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.roles.index	\N
5	1	Tools		_self	voyager-tools	\N	\N	9	2020-10-15 10:12:55	2020-10-15 10:12:55	\N	\N
6	1	Menu Builder		_self	voyager-list	\N	5	10	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.menus.index	\N
7	1	Database		_self	voyager-data	\N	5	11	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.database.index	\N
8	1	Compass		_self	voyager-compass	\N	5	12	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.compass.index	\N
9	1	BREAD		_self	voyager-bread	\N	5	13	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.bread.index	\N
10	1	Settings		_self	voyager-settings	\N	\N	14	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.settings.index	\N
11	1	Categories		_self	voyager-categories	\N	\N	8	2020-10-15 10:12:55	2020-10-15 10:12:55	voyager.categories.index	\N
12	1	Posts		_self	voyager-news	\N	\N	6	2020-10-15 10:12:56	2020-10-15 10:12:56	voyager.posts.index	\N
13	1	Pages		_self	voyager-file-text	\N	\N	7	2020-10-15 10:12:56	2020-10-15 10:12:56	voyager.pages.index	\N
14	1	Hooks		_self	voyager-hook	\N	5	13	2020-10-15 10:12:56	2020-10-15 10:12:56	voyager.hooks	\N
16	1	Типы аттрибутов		_self	\N	\N	\N	16	2020-10-16 04:47:01	2020-10-16 04:47:01	voyager.attribute-types.index	\N
17	1	Атрибуты категории		_self	\N	\N	\N	17	2020-10-16 04:49:24	2020-10-16 04:49:24	voyager.category-attributes.index	\N
18	1	Тип показа продукта		_self	\N	\N	\N	18	2020-10-16 04:55:27	2020-10-16 04:55:27	voyager.display-product-types.index	\N
19	1	Товары		_self	\N	\N	\N	19	2020-10-21 09:31:04	2020-10-21 09:31:04	voyager.products.index	\N
20	1	Размеры		_self	\N	\N	\N	20	2020-10-21 09:52:44	2020-10-21 09:52:44	voyager.sizes.index	\N
21	1	Цвета и размеры одежды		_self	\N	\N	\N	21	2020-10-21 10:49:27	2020-10-21 10:49:27	voyager.colors.index	\N
15	1	Категории товаров		_self	\N	#000000	\N	15	2020-10-15 10:31:11	2020-10-21 11:03:43	voyager.shop-categories.index	null
22	1	Флаги		_self	\N	\N	\N	22	2020-11-05 05:39:23	2020-11-05 05:39:23	voyager.flags.index	\N
23	1	Секции в главной		_self	\N	\N	\N	23	2020-11-05 05:40:53	2020-11-05 05:40:53	voyager.main-sections.index	\N
24	1	Флаги продукта		_self	\N	\N	\N	24	2020-11-05 05:42:00	2020-11-05 05:42:00	voyager.product-flags.index	\N
25	1	Продукты в секции		_self	\N	\N	\N	25	2020-11-05 06:03:23	2020-11-05 06:03:23	voyager.main-section-products.index	\N
26	1	Баннеры		_self	\N	\N	\N	26	2020-11-05 07:34:40	2020-11-05 07:34:40	voyager.banners.index	\N
27	1	Category Filters		_self	\N	\N	\N	27	2020-11-05 08:23:18	2020-11-05 08:23:18	voyager.category-filters.index	\N
28	1	Статусы оплаты		_self	\N	\N	\N	28	2020-11-05 19:38:30	2020-11-05 19:38:30	voyager.payment-statuses.index	\N
29	1	Статусы заказа		_self	\N	\N	\N	29	2020-11-05 19:39:01	2020-11-05 19:39:01	voyager.order-statuses.index	\N
30	1	Типы получения товаров		_self	\N	\N	\N	30	2020-11-05 19:40:07	2020-11-05 19:40:07	voyager.delivery-types.index	\N
31	1	Типы оплаты		_self	\N	\N	\N	31	2020-11-05 19:42:13	2020-11-05 19:42:13	voyager.payment-types.index	\N
32	1	Заказы		_self	\N	\N	\N	32	2020-11-06 03:24:50	2020-11-06 03:24:50	voyager.orders.index	\N
\.


--
-- Data for Name: menus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menus (id, name, created_at, updated_at) FROM stdin;
1	admin	2020-10-15 10:12:55	2020-10-15 10:12:55
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2016_01_01_000000_add_voyager_user_fields	1
4	2016_01_01_000000_create_data_types_table	1
5	2016_01_01_000000_create_pages_table	1
6	2016_01_01_000000_create_posts_table	1
7	2016_02_15_204651_create_categories_table	1
8	2016_05_19_173453_create_menu_table	1
9	2016_06_01_000001_create_oauth_auth_codes_table	1
10	2016_06_01_000002_create_oauth_access_tokens_table	1
11	2016_06_01_000003_create_oauth_refresh_tokens_table	1
12	2016_06_01_000004_create_oauth_clients_table	1
13	2016_06_01_000005_create_oauth_personal_access_clients_table	1
14	2016_10_21_190000_create_roles_table	1
15	2016_10_21_190000_create_settings_table	1
16	2016_11_30_135954_create_permission_table	1
17	2016_11_30_141208_create_permission_role_table	1
18	2016_12_26_201236_data_types__add__server_side	1
19	2017_01_13_000000_add_route_to_menu_items_table	1
20	2017_01_14_005015_create_translations_table	1
21	2017_01_15_000000_make_table_name_nullable_in_permissions_table	1
22	2017_03_06_000000_add_controller_to_data_types_table	1
23	2017_04_11_000000_alter_post_nullable_fields_table	1
24	2017_04_21_000000_add_order_to_data_rows_table	1
25	2017_07_05_210000_add_policyname_to_data_types_table	1
26	2017_08_05_000000_add_group_to_settings_table	1
27	2017_11_26_013050_add_user_role_relationship	1
28	2017_11_26_015000_create_user_roles_table	1
29	2018_03_11_000000_add_user_settings	1
30	2018_03_14_000000_add_details_to_data_types_table	1
31	2018_03_16_000000_make_settings_value_nullable	1
32	2019_08_19_000000_create_failed_jobs_table	1
33	2020_10_10_092119_create_attribute_types_table	1
34	2020_10_10_093845_create_display_product_types_table	1
35	2020_10_15_062834_create_shop_categories_table	1
36	2020_10_15_063006_create_category_attributes_table	1
37	2020_10_15_082812_update_shop_category	1
38	2020_10_15_092553_create_products_table	1
39	2020_10_16_093417_create_sizes_table	2
40	2020_10_16_093701_create_colors_table	2
41	2020_10_16_104053_create_category_filters_table	2
42	2020_10_19_090102_create_payment_types_table	2
43	2020_10_19_090117_create_delivery_types_table	2
44	2020_10_19_090138_create_order_statuses_table	2
45	2020_10_19_091601_create_payment_statuses_table	2
46	2020_10_19_091744_create_orders_table	2
47	2020_10_19_091825_create_order_details_table	2
48	2020_10_19_095219_create_reviews_table	2
49	2020_10_19_095459_create_return_statuses_table	2
50	2020_10_19_095509_create_purchase_returns_table	2
51	2020_10_22_102755_update_attribute	3
52	2020_11_03_055312_add_product_id_to_reviews_table	4
54	2020_11_04_061410_add_icon_to_shop_categories_table	5
55	2020_11_03_065544_create_flags_table	6
56	2020_11_05_052006_create_main_sections_table	7
57	2020_11_05_053508_create_main_section_products_table	7
58	2020_11_05_053623_create_product_flags_table	8
59	2020_11_05_073245_create_banners_table	9
60	2020_11_05_073519_add_percent_to_banners_table	10
\.


--
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_access_tokens (id, user_id, client_id, name, scopes, revoked, created_at, updated_at, expires_at) FROM stdin;
\.


--
-- Data for Name: oauth_auth_codes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_auth_codes (id, user_id, client_id, scopes, revoked, expires_at) FROM stdin;
\.


--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_clients (id, user_id, name, secret, provider, redirect, personal_access_client, password_client, revoked, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: oauth_personal_access_clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_personal_access_clients (id, client_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: oauth_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oauth_refresh_tokens (id, access_token_id, revoked, expires_at) FROM stdin;
\.


--
-- Data for Name: order_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_details (id, order_id, product_id, color_id, unit_quantity, unit_price, created_at, updated_at) FROM stdin;
1	1	1	\N	1	50000	2020-11-06 03:32:46	2020-11-06 03:32:46
\.


--
-- Data for Name: order_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_statuses (id, name, created_at, updated_at) FROM stdin;
1	Новый заказ	2020-11-05 19:40:45	2020-11-05 19:40:45
2	Заказ закрыт	2020-11-05 19:40:53	2020-11-05 19:40:53
\.


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (id, payment_type_id, delivery_type_id, order_status_id, payment_status_id, user_id, name, phone, email, address, country_city, flat, mail_index, unit_price, unit_quantity, delivery_price, delivery_time, created_at, updated_at) FROM stdin;
1	1	1	1	1	\N	asyl tas	87073039917	tomboffos@gmail.com	Kablukogo 264,65	Almaty	22	\N	50000	1	400	\N	2020-11-06 03:32:46	2020-11-06 03:32:46
\.


--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pages (id, author_id, title, excerpt, body, image, slug, meta_description, meta_keywords, status, created_at, updated_at) FROM stdin;
1	0	Hello World	Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.	<p>Hello World. Scallywag grog swab Cat o'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>	pages/page1.jpg	hello-world	Yar Meta Description	Keyword1, Keyword2	ACTIVE	2020-10-15 10:12:56	2020-10-15 10:12:56
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: payment_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_statuses (id, name, created_at, updated_at) FROM stdin;
1	Не оплачено	2020-11-05 19:40:36	2020-11-05 19:40:36
2	Оплачено	2020-11-05 19:41:03	2020-11-05 19:41:03
\.


--
-- Data for Name: payment_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_types (id, name, created_at, updated_at) FROM stdin;
1	Наличными	2020-11-05 19:42:25	2020-11-05 19:42:25
2	Оплата картой	2020-11-05 19:42:33	2020-11-05 19:42:33
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permission_role (permission_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	1
35	1
36	1
37	1
38	1
39	1
40	1
42	1
43	1
44	1
45	1
46	1
47	1
48	1
49	1
50	1
51	1
52	1
53	1
54	1
55	1
56	1
57	1
58	1
59	1
60	1
61	1
62	1
63	1
64	1
65	1
66	1
67	1
68	1
69	1
70	1
71	1
72	1
73	1
74	1
75	1
76	1
77	1
78	1
79	1
80	1
81	1
82	1
83	1
84	1
85	1
86	1
87	1
88	1
89	1
90	1
91	1
92	1
93	1
94	1
95	1
96	1
97	1
98	1
99	1
100	1
101	1
102	1
103	1
104	1
105	1
106	1
107	1
108	1
109	1
110	1
111	1
112	1
113	1
114	1
115	1
116	1
117	1
118	1
119	1
120	1
121	1
122	1
123	1
124	1
125	1
126	1
127	1
128	1
129	1
130	1
131	1
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, key, table_name, created_at, updated_at) FROM stdin;
1	browse_admin	\N	2020-10-15 10:12:55	2020-10-15 10:12:55
2	browse_bread	\N	2020-10-15 10:12:55	2020-10-15 10:12:55
3	browse_database	\N	2020-10-15 10:12:55	2020-10-15 10:12:55
4	browse_media	\N	2020-10-15 10:12:55	2020-10-15 10:12:55
5	browse_compass	\N	2020-10-15 10:12:55	2020-10-15 10:12:55
6	browse_menus	menus	2020-10-15 10:12:55	2020-10-15 10:12:55
7	read_menus	menus	2020-10-15 10:12:55	2020-10-15 10:12:55
8	edit_menus	menus	2020-10-15 10:12:55	2020-10-15 10:12:55
9	add_menus	menus	2020-10-15 10:12:55	2020-10-15 10:12:55
10	delete_menus	menus	2020-10-15 10:12:55	2020-10-15 10:12:55
11	browse_roles	roles	2020-10-15 10:12:55	2020-10-15 10:12:55
12	read_roles	roles	2020-10-15 10:12:55	2020-10-15 10:12:55
13	edit_roles	roles	2020-10-15 10:12:55	2020-10-15 10:12:55
14	add_roles	roles	2020-10-15 10:12:55	2020-10-15 10:12:55
15	delete_roles	roles	2020-10-15 10:12:55	2020-10-15 10:12:55
16	browse_users	users	2020-10-15 10:12:55	2020-10-15 10:12:55
17	read_users	users	2020-10-15 10:12:55	2020-10-15 10:12:55
18	edit_users	users	2020-10-15 10:12:55	2020-10-15 10:12:55
19	add_users	users	2020-10-15 10:12:55	2020-10-15 10:12:55
20	delete_users	users	2020-10-15 10:12:55	2020-10-15 10:12:55
21	browse_settings	settings	2020-10-15 10:12:55	2020-10-15 10:12:55
22	read_settings	settings	2020-10-15 10:12:55	2020-10-15 10:12:55
23	edit_settings	settings	2020-10-15 10:12:55	2020-10-15 10:12:55
24	add_settings	settings	2020-10-15 10:12:55	2020-10-15 10:12:55
25	delete_settings	settings	2020-10-15 10:12:55	2020-10-15 10:12:55
26	browse_categories	categories	2020-10-15 10:12:55	2020-10-15 10:12:55
27	read_categories	categories	2020-10-15 10:12:55	2020-10-15 10:12:55
28	edit_categories	categories	2020-10-15 10:12:55	2020-10-15 10:12:55
29	add_categories	categories	2020-10-15 10:12:55	2020-10-15 10:12:55
30	delete_categories	categories	2020-10-15 10:12:55	2020-10-15 10:12:55
31	browse_posts	posts	2020-10-15 10:12:56	2020-10-15 10:12:56
32	read_posts	posts	2020-10-15 10:12:56	2020-10-15 10:12:56
33	edit_posts	posts	2020-10-15 10:12:56	2020-10-15 10:12:56
34	add_posts	posts	2020-10-15 10:12:56	2020-10-15 10:12:56
35	delete_posts	posts	2020-10-15 10:12:56	2020-10-15 10:12:56
36	browse_pages	pages	2020-10-15 10:12:56	2020-10-15 10:12:56
37	read_pages	pages	2020-10-15 10:12:56	2020-10-15 10:12:56
38	edit_pages	pages	2020-10-15 10:12:56	2020-10-15 10:12:56
39	add_pages	pages	2020-10-15 10:12:56	2020-10-15 10:12:56
40	delete_pages	pages	2020-10-15 10:12:56	2020-10-15 10:12:56
41	browse_hooks	\N	2020-10-15 10:12:56	2020-10-15 10:12:56
42	browse_shop_categories	shop_categories	2020-10-15 10:31:11	2020-10-15 10:31:11
43	read_shop_categories	shop_categories	2020-10-15 10:31:11	2020-10-15 10:31:11
44	edit_shop_categories	shop_categories	2020-10-15 10:31:11	2020-10-15 10:31:11
45	add_shop_categories	shop_categories	2020-10-15 10:31:11	2020-10-15 10:31:11
46	delete_shop_categories	shop_categories	2020-10-15 10:31:11	2020-10-15 10:31:11
47	browse_attribute_types	attribute_types	2020-10-16 04:47:01	2020-10-16 04:47:01
48	read_attribute_types	attribute_types	2020-10-16 04:47:01	2020-10-16 04:47:01
49	edit_attribute_types	attribute_types	2020-10-16 04:47:01	2020-10-16 04:47:01
50	add_attribute_types	attribute_types	2020-10-16 04:47:01	2020-10-16 04:47:01
51	delete_attribute_types	attribute_types	2020-10-16 04:47:01	2020-10-16 04:47:01
52	browse_category_attributes	category_attributes	2020-10-16 04:49:24	2020-10-16 04:49:24
53	read_category_attributes	category_attributes	2020-10-16 04:49:24	2020-10-16 04:49:24
54	edit_category_attributes	category_attributes	2020-10-16 04:49:24	2020-10-16 04:49:24
55	add_category_attributes	category_attributes	2020-10-16 04:49:24	2020-10-16 04:49:24
56	delete_category_attributes	category_attributes	2020-10-16 04:49:24	2020-10-16 04:49:24
57	browse_display_product_types	display_product_types	2020-10-16 04:55:27	2020-10-16 04:55:27
58	read_display_product_types	display_product_types	2020-10-16 04:55:27	2020-10-16 04:55:27
59	edit_display_product_types	display_product_types	2020-10-16 04:55:27	2020-10-16 04:55:27
60	add_display_product_types	display_product_types	2020-10-16 04:55:27	2020-10-16 04:55:27
61	delete_display_product_types	display_product_types	2020-10-16 04:55:27	2020-10-16 04:55:27
62	browse_products	products	2020-10-21 09:31:03	2020-10-21 09:31:03
63	read_products	products	2020-10-21 09:31:03	2020-10-21 09:31:03
64	edit_products	products	2020-10-21 09:31:03	2020-10-21 09:31:03
65	add_products	products	2020-10-21 09:31:03	2020-10-21 09:31:03
66	delete_products	products	2020-10-21 09:31:03	2020-10-21 09:31:03
67	browse_sizes	sizes	2020-10-21 09:52:44	2020-10-21 09:52:44
68	read_sizes	sizes	2020-10-21 09:52:44	2020-10-21 09:52:44
69	edit_sizes	sizes	2020-10-21 09:52:44	2020-10-21 09:52:44
70	add_sizes	sizes	2020-10-21 09:52:44	2020-10-21 09:52:44
71	delete_sizes	sizes	2020-10-21 09:52:44	2020-10-21 09:52:44
72	browse_colors	colors	2020-10-21 10:49:27	2020-10-21 10:49:27
73	read_colors	colors	2020-10-21 10:49:27	2020-10-21 10:49:27
74	edit_colors	colors	2020-10-21 10:49:27	2020-10-21 10:49:27
75	add_colors	colors	2020-10-21 10:49:27	2020-10-21 10:49:27
76	delete_colors	colors	2020-10-21 10:49:27	2020-10-21 10:49:27
77	browse_flags	flags	2020-11-05 05:39:23	2020-11-05 05:39:23
78	read_flags	flags	2020-11-05 05:39:23	2020-11-05 05:39:23
79	edit_flags	flags	2020-11-05 05:39:23	2020-11-05 05:39:23
80	add_flags	flags	2020-11-05 05:39:23	2020-11-05 05:39:23
81	delete_flags	flags	2020-11-05 05:39:23	2020-11-05 05:39:23
82	browse_main_sections	main_sections	2020-11-05 05:40:53	2020-11-05 05:40:53
83	read_main_sections	main_sections	2020-11-05 05:40:53	2020-11-05 05:40:53
84	edit_main_sections	main_sections	2020-11-05 05:40:53	2020-11-05 05:40:53
85	add_main_sections	main_sections	2020-11-05 05:40:53	2020-11-05 05:40:53
86	delete_main_sections	main_sections	2020-11-05 05:40:53	2020-11-05 05:40:53
87	browse_product_flags	product_flags	2020-11-05 05:42:00	2020-11-05 05:42:00
88	read_product_flags	product_flags	2020-11-05 05:42:00	2020-11-05 05:42:00
89	edit_product_flags	product_flags	2020-11-05 05:42:00	2020-11-05 05:42:00
90	add_product_flags	product_flags	2020-11-05 05:42:00	2020-11-05 05:42:00
91	delete_product_flags	product_flags	2020-11-05 05:42:00	2020-11-05 05:42:00
92	browse_main_section_products	main_section_products	2020-11-05 06:03:23	2020-11-05 06:03:23
93	read_main_section_products	main_section_products	2020-11-05 06:03:23	2020-11-05 06:03:23
94	edit_main_section_products	main_section_products	2020-11-05 06:03:23	2020-11-05 06:03:23
95	add_main_section_products	main_section_products	2020-11-05 06:03:23	2020-11-05 06:03:23
96	delete_main_section_products	main_section_products	2020-11-05 06:03:23	2020-11-05 06:03:23
97	browse_banners	banners	2020-11-05 07:34:40	2020-11-05 07:34:40
98	read_banners	banners	2020-11-05 07:34:40	2020-11-05 07:34:40
99	edit_banners	banners	2020-11-05 07:34:40	2020-11-05 07:34:40
100	add_banners	banners	2020-11-05 07:34:40	2020-11-05 07:34:40
101	delete_banners	banners	2020-11-05 07:34:40	2020-11-05 07:34:40
102	browse_category_filters	category_filters	2020-11-05 08:23:18	2020-11-05 08:23:18
103	read_category_filters	category_filters	2020-11-05 08:23:18	2020-11-05 08:23:18
104	edit_category_filters	category_filters	2020-11-05 08:23:18	2020-11-05 08:23:18
105	add_category_filters	category_filters	2020-11-05 08:23:18	2020-11-05 08:23:18
106	delete_category_filters	category_filters	2020-11-05 08:23:18	2020-11-05 08:23:18
107	browse_payment_statuses	payment_statuses	2020-11-05 19:38:30	2020-11-05 19:38:30
108	read_payment_statuses	payment_statuses	2020-11-05 19:38:30	2020-11-05 19:38:30
109	edit_payment_statuses	payment_statuses	2020-11-05 19:38:30	2020-11-05 19:38:30
110	add_payment_statuses	payment_statuses	2020-11-05 19:38:30	2020-11-05 19:38:30
111	delete_payment_statuses	payment_statuses	2020-11-05 19:38:30	2020-11-05 19:38:30
112	browse_order_statuses	order_statuses	2020-11-05 19:39:01	2020-11-05 19:39:01
113	read_order_statuses	order_statuses	2020-11-05 19:39:01	2020-11-05 19:39:01
114	edit_order_statuses	order_statuses	2020-11-05 19:39:01	2020-11-05 19:39:01
115	add_order_statuses	order_statuses	2020-11-05 19:39:01	2020-11-05 19:39:01
116	delete_order_statuses	order_statuses	2020-11-05 19:39:01	2020-11-05 19:39:01
117	browse_delivery_types	delivery_types	2020-11-05 19:40:07	2020-11-05 19:40:07
118	read_delivery_types	delivery_types	2020-11-05 19:40:07	2020-11-05 19:40:07
119	edit_delivery_types	delivery_types	2020-11-05 19:40:07	2020-11-05 19:40:07
120	add_delivery_types	delivery_types	2020-11-05 19:40:07	2020-11-05 19:40:07
121	delete_delivery_types	delivery_types	2020-11-05 19:40:07	2020-11-05 19:40:07
122	browse_payment_types	payment_types	2020-11-05 19:42:13	2020-11-05 19:42:13
123	read_payment_types	payment_types	2020-11-05 19:42:13	2020-11-05 19:42:13
124	edit_payment_types	payment_types	2020-11-05 19:42:13	2020-11-05 19:42:13
125	add_payment_types	payment_types	2020-11-05 19:42:13	2020-11-05 19:42:13
126	delete_payment_types	payment_types	2020-11-05 19:42:13	2020-11-05 19:42:13
127	browse_orders	orders	2020-11-06 03:24:50	2020-11-06 03:24:50
128	read_orders	orders	2020-11-06 03:24:50	2020-11-06 03:24:50
129	edit_orders	orders	2020-11-06 03:24:50	2020-11-06 03:24:50
130	add_orders	orders	2020-11-06 03:24:50	2020-11-06 03:24:50
131	delete_orders	orders	2020-11-06 03:24:50	2020-11-06 03:24:50
\.


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.posts (id, author_id, category_id, title, seo_title, excerpt, body, image, slug, meta_description, meta_keywords, status, featured, created_at, updated_at) FROM stdin;
1	0	\N	Lorem Ipsum Post	\N	This is the excerpt for the Lorem Ipsum Post	<p>This is the body of the lorem ipsum post</p>	posts/post1.jpg	lorem-ipsum-post	This is the meta description	keyword1, keyword2, keyword3	PUBLISHED	f	2020-10-15 10:12:56	2020-10-15 10:12:56
2	0	\N	My Sample Post	\N	This is the excerpt for the sample Post	<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>	posts/post2.jpg	my-sample-post	Meta Description for sample post	keyword1, keyword2, keyword3	PUBLISHED	f	2020-10-15 10:12:56	2020-10-15 10:12:56
3	0	\N	Latest Post	\N	This is the excerpt for the latest post	<p>This is the body for the latest post</p>	posts/post3.jpg	latest-post	This is the meta description	keyword1, keyword2, keyword3	PUBLISHED	f	2020-10-15 10:12:56	2020-10-15 10:12:56
4	0	\N	Yarr Post	\N	Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.	<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>	posts/post4.jpg	yarr-post	this be a meta descript	keyword1, keyword2, keyword3	PUBLISHED	f	2020-10-15 10:12:56	2020-10-15 10:12:56
\.


--
-- Data for Name: product_flags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_flags (id, product_id, flag_id, created_at, updated_at) FROM stdin;
1	1	1	2020-11-05 05:43:51	2020-11-05 05:43:51
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, shop_category_id, display_product_type_id, name, stock, review_count, review_point, image, slider_images, description, additional_attributes, price, discount_price, created_at, updated_at) FROM stdin;
7	4	2	KIXX	t	0	0	products\\November2020\\FGp634e5Lj5UdVpF2isS.jpg	["products\\\\November2020\\\\2Hu7F6BI2EVh0I69kmn9.jpg","products\\\\November2020\\\\GGMjAFxfTVPIEjVumvGX.png"]	<p>qwdwqdwqd</p>	["\\u0422\\u0438\\u043f \\u043c\\u0430\\u0441\\u043b\\u0430:\\u0421\\u0438\\u043d\\u0442\\u0435\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435"]	2222	1200	2020-11-05 12:17:00	2020-11-05 17:50:08
1	2	1	Одежда	t	0	0	products\\October2020\\J8xPyCx9ENwbi09ZhcKr.jpg	["products\\\\October2020\\\\kVfHnKKa4gJNRTLAJKrJ.jpg","products\\\\October2020\\\\y4NhyLBj9ykGlLgMgGwe.jpg","products\\\\October2020\\\\ebVVmPXEBFWAK2Ilcrmv.png"]	<p>Новая одежда</p>	["\\u041c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b:\\u041a\\u043e\\u0436\\u0430"]	50000	2000	2020-10-21 09:42:00	2020-11-05 18:29:15
2	2	2	wdwd	t	0	0	products\\October2020\\lW9m1yfZ2op6RhgJEDW2.png	["products\\\\October2020\\\\jvOjKg2U6TkZwbT0kvQ9.jpg","products\\\\October2020\\\\qRYjHpjVSMkVjm84YIGe.jpg","products\\\\October2020\\\\dgtBhSQuYDmpT2jLrv2M.png"]	<p>Facebook</p>	["\\u041c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b:\\u0414\\u0435\\u0440\\u043c\\u0430\\u043d\\u0442\\u0438\\u043d"]	2222	222	2020-10-26 10:45:00	2020-11-05 18:29:27
6	4	2	KIXX	t	0	0	products\\November2020\\DfUgiqtgrjYcl1mYjmfO.jpg	["products\\\\November2020\\\\NGAZh0rFFPOIrHiwNZva.jpg"]	<p><span style="color: #ffffff; font-family: Consolas, 'Lucida Console', 'Courier New', monospace; font-size: 12px; white-space: pre-wrap; background-color: #242424;">additional_attributes</span></p>	["tip-masla:\\u0421\\u0438\\u043d\\u0442\\u0435\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435"]	2222	35	2020-11-05 12:04:00	2020-11-05 12:15:46
\.


--
-- Data for Name: purchase_returns; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.purchase_returns (id, user_id, product_id, color_id, order_id, return_status_id, name, phone, return_reason, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: return_statuses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.return_statuses (id, name, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews (id, user_id, advantages, disadvantages, comments, review, created_at, updated_at, product_id) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, display_name, created_at, updated_at) FROM stdin;
1	admin	Administrator	2020-10-15 10:12:55	2020-10-15 10:12:55
2	user	Normal User	2020-10-15 10:12:55	2020-10-15 10:12:55
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.settings (id, key, display_name, value, details, type, "order", "group") FROM stdin;
1	site.title	Site Title	Site Title		text	1	Site
2	site.description	Site Description	Site Description		text	2	Site
3	site.logo	Site Logo			image	3	Site
4	site.google_analytics_tracking_id	Google Analytics Tracking ID			text	4	Site
5	admin.bg_image	Admin Background Image			image	5	Admin
6	admin.title	Admin Title	Voyager		text	1	Admin
7	admin.description	Admin Description	Welcome to Voyager. The Missing Admin for Laravel		text	2	Admin
8	admin.loader	Admin Loader			image	3	Admin
9	admin.icon_image	Admin Icon Image			image	4	Admin
10	admin.google_analytics_client_id	Google Analytics Client ID (used for admin dashboard)			text	1	Admin
\.


--
-- Data for Name: shop_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.shop_categories (id, parent_id, "order", name, slug, created_at, updated_at, image, icon) FROM stdin;
2	\N	1	Одежда	odezhda	2020-10-21 09:44:52	2020-10-21 11:23:01	shop-categories\\October2020\\IHIxmKVMTJcLMhmHfkT6.png	\N
1	\N	2	Еда	eda	2020-10-16 04:37:15	2020-10-21 11:23:01	shop-categories\\October2020\\0OPiAZ2buEaooJCLmV08.png	\N
3	2	1	Блузки	bluzki	2020-10-27 09:09:30	2020-10-27 09:09:30	shop-categories\\October2020\\p9OEALlcInW4TWsC5jts.jpg	\N
4	\N	1	Автомасла	avtomasla	2020-11-05 11:28:28	2020-11-05 11:28:28	shop-categories\\November2020\\2IRraYKQWv93lwZQJtVR.png	\N
\.


--
-- Data for Name: sizes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sizes (id, is_shoes_size, size, in_stock, created_at, updated_at) FROM stdin;
1	f	52	t	2020-10-21 09:52:57	2020-10-21 09:52:57
\.


--
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.translations (id, table_name, column_name, foreign_key, locale, value, created_at, updated_at) FROM stdin;
1	data_types	display_name_singular	5	pt	Post	2020-10-15 10:12:56	2020-10-15 10:12:56
2	data_types	display_name_singular	6	pt	Página	2020-10-15 10:12:56	2020-10-15 10:12:56
3	data_types	display_name_singular	1	pt	Utilizador	2020-10-15 10:12:56	2020-10-15 10:12:56
4	data_types	display_name_singular	4	pt	Categoria	2020-10-15 10:12:56	2020-10-15 10:12:56
5	data_types	display_name_singular	2	pt	Menu	2020-10-15 10:12:56	2020-10-15 10:12:56
6	data_types	display_name_singular	3	pt	Função	2020-10-15 10:12:56	2020-10-15 10:12:56
7	data_types	display_name_plural	5	pt	Posts	2020-10-15 10:12:56	2020-10-15 10:12:56
8	data_types	display_name_plural	6	pt	Páginas	2020-10-15 10:12:56	2020-10-15 10:12:56
9	data_types	display_name_plural	1	pt	Utilizadores	2020-10-15 10:12:56	2020-10-15 10:12:56
10	data_types	display_name_plural	4	pt	Categorias	2020-10-15 10:12:56	2020-10-15 10:12:56
11	data_types	display_name_plural	2	pt	Menus	2020-10-15 10:12:56	2020-10-15 10:12:56
12	data_types	display_name_plural	3	pt	Funções	2020-10-15 10:12:56	2020-10-15 10:12:56
13	categories	slug	1	pt	categoria-1	2020-10-15 10:12:56	2020-10-15 10:12:56
14	categories	name	1	pt	Categoria 1	2020-10-15 10:12:56	2020-10-15 10:12:56
15	categories	slug	2	pt	categoria-2	2020-10-15 10:12:56	2020-10-15 10:12:56
16	categories	name	2	pt	Categoria 2	2020-10-15 10:12:56	2020-10-15 10:12:56
17	pages	title	1	pt	Olá Mundo	2020-10-15 10:12:56	2020-10-15 10:12:56
18	pages	slug	1	pt	ola-mundo	2020-10-15 10:12:56	2020-10-15 10:12:56
19	pages	body	1	pt	<p>Olá Mundo. Scallywag grog swab Cat o'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>	2020-10-15 10:12:56	2020-10-15 10:12:56
20	menu_items	title	1	pt	Painel de Controle	2020-10-15 10:12:56	2020-10-15 10:12:56
21	menu_items	title	2	pt	Media	2020-10-15 10:12:56	2020-10-15 10:12:56
22	menu_items	title	12	pt	Publicações	2020-10-15 10:12:56	2020-10-15 10:12:56
23	menu_items	title	3	pt	Utilizadores	2020-10-15 10:12:56	2020-10-15 10:12:56
24	menu_items	title	11	pt	Categorias	2020-10-15 10:12:56	2020-10-15 10:12:56
25	menu_items	title	13	pt	Páginas	2020-10-15 10:12:56	2020-10-15 10:12:56
26	menu_items	title	4	pt	Funções	2020-10-15 10:12:56	2020-10-15 10:12:56
27	menu_items	title	5	pt	Ferramentas	2020-10-15 10:12:56	2020-10-15 10:12:56
28	menu_items	title	6	pt	Menus	2020-10-15 10:12:56	2020-10-15 10:12:56
29	menu_items	title	7	pt	Base de dados	2020-10-15 10:12:56	2020-10-15 10:12:56
30	menu_items	title	10	pt	Configurações	2020-10-15 10:12:56	2020-10-15 10:12:56
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_roles (user_id, role_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, avatar, role_id, settings) FROM stdin;
1	Admin	admin@admin.com	\N	$2y$10$AsDZs9ILc9poQOSkcEHeGukH7pab0T5sTks5DftD0rOEWIN09175K	w2Exv9o9mFKcHmarooT61HyzH6edi3iV383PBy0Cl3Fk6AbLQrUCw7GR3CgZ	2020-10-15 10:12:55	2020-10-15 10:15:40	users/default.png	1	{"locale":"ru"}
\.


--
-- Name: attribute_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.attribute_types_id_seq', 2, true);


--
-- Name: banners_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banners_id_seq', 1, true);


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 2, true);


--
-- Name: category_attributes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_attributes_id_seq', 3, true);


--
-- Name: category_filters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_filters_id_seq', 2, true);


--
-- Name: colors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.colors_id_seq', 1, false);


--
-- Name: data_rows_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.data_rows_id_seq', 197, true);


--
-- Name: data_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.data_types_id_seq', 34, true);


--
-- Name: delivery_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.delivery_types_id_seq', 2, true);


--
-- Name: display_product_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.display_product_types_id_seq', 3, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: flags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.flags_id_seq', 1, true);


--
-- Name: main_section_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_section_products_id_seq', 1, true);


--
-- Name: main_sections_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_sections_id_seq', 1, true);


--
-- Name: menu_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menu_items_id_seq', 32, true);


--
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menus_id_seq', 1, true);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 60, true);


--
-- Name: oauth_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_clients_id_seq', 1, false);


--
-- Name: oauth_personal_access_clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oauth_personal_access_clients_id_seq', 1, false);


--
-- Name: order_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_details_id_seq', 1, true);


--
-- Name: order_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.order_statuses_id_seq', 2, true);


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 1, true);


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pages_id_seq', 1, true);


--
-- Name: payment_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payment_statuses_id_seq', 2, true);


--
-- Name: payment_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payment_types_id_seq', 2, true);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 131, true);


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.posts_id_seq', 4, true);


--
-- Name: product_flags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_flags_id_seq', 1, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 7, true);


--
-- Name: purchase_returns_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.purchase_returns_id_seq', 1, false);


--
-- Name: return_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.return_statuses_id_seq', 1, false);


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.settings_id_seq', 10, true);


--
-- Name: shop_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.shop_categories_id_seq', 4, true);


--
-- Name: sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sizes_id_seq', 1, true);


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.translations_id_seq', 30, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: attribute_types attribute_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.attribute_types
    ADD CONSTRAINT attribute_types_pkey PRIMARY KEY (id);


--
-- Name: banners banners_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banners
    ADD CONSTRAINT banners_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: categories categories_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_slug_unique UNIQUE (slug);


--
-- Name: category_attributes category_attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_attributes
    ADD CONSTRAINT category_attributes_pkey PRIMARY KEY (id);


--
-- Name: category_attributes category_attributes_slug_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_attributes
    ADD CONSTRAINT category_attributes_slug_name_unique UNIQUE (slug_name);


--
-- Name: category_filters category_filters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_filters
    ADD CONSTRAINT category_filters_pkey PRIMARY KEY (id);


--
-- Name: colors colors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.colors
    ADD CONSTRAINT colors_pkey PRIMARY KEY (id);


--
-- Name: data_rows data_rows_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_rows
    ADD CONSTRAINT data_rows_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_name_unique UNIQUE (name);


--
-- Name: data_types data_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_slug_unique UNIQUE (slug);


--
-- Name: delivery_types delivery_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.delivery_types
    ADD CONSTRAINT delivery_types_pkey PRIMARY KEY (id);


--
-- Name: display_product_types display_product_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.display_product_types
    ADD CONSTRAINT display_product_types_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: flags flags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flags
    ADD CONSTRAINT flags_pkey PRIMARY KEY (id);


--
-- Name: main_section_products main_section_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_section_products
    ADD CONSTRAINT main_section_products_pkey PRIMARY KEY (id);


--
-- Name: main_sections main_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_sections
    ADD CONSTRAINT main_sections_pkey PRIMARY KEY (id);


--
-- Name: menu_items menu_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_pkey PRIMARY KEY (id);


--
-- Name: menus menus_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_name_unique UNIQUE (name);


--
-- Name: menus menus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth_auth_codes oauth_auth_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_auth_codes
    ADD CONSTRAINT oauth_auth_codes_pkey PRIMARY KEY (id);


--
-- Name: oauth_clients oauth_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_personal_access_clients oauth_personal_access_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_personal_access_clients
    ADD CONSTRAINT oauth_personal_access_clients_pkey PRIMARY KEY (id);


--
-- Name: oauth_refresh_tokens oauth_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oauth_refresh_tokens
    ADD CONSTRAINT oauth_refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: order_details order_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_details
    ADD CONSTRAINT order_details_pkey PRIMARY KEY (id);


--
-- Name: order_statuses order_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_statuses
    ADD CONSTRAINT order_statuses_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: pages pages_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_slug_unique UNIQUE (slug);


--
-- Name: payment_statuses payment_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_statuses
    ADD CONSTRAINT payment_statuses_pkey PRIMARY KEY (id);


--
-- Name: payment_types payment_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_types
    ADD CONSTRAINT payment_types_pkey PRIMARY KEY (id);


--
-- Name: permission_role permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: posts posts_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_slug_unique UNIQUE (slug);


--
-- Name: product_flags product_flags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_flags
    ADD CONSTRAINT product_flags_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: purchase_returns purchase_returns_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns
    ADD CONSTRAINT purchase_returns_pkey PRIMARY KEY (id);


--
-- Name: return_statuses return_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.return_statuses
    ADD CONSTRAINT return_statuses_pkey PRIMARY KEY (id);


--
-- Name: reviews reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: roles roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: settings settings_key_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_key_unique UNIQUE (key);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: shop_categories shop_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shop_categories
    ADD CONSTRAINT shop_categories_pkey PRIMARY KEY (id);


--
-- Name: shop_categories shop_categories_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shop_categories
    ADD CONSTRAINT shop_categories_slug_unique UNIQUE (slug);


--
-- Name: sizes sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sizes
    ADD CONSTRAINT sizes_pkey PRIMARY KEY (id);


--
-- Name: translations translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: translations translations_table_name_column_name_foreign_key_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.translations
    ADD CONSTRAINT translations_table_name_column_name_foreign_key_locale_unique UNIQUE (table_name, column_name, foreign_key, locale);


--
-- Name: user_roles user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: oauth_access_tokens_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_access_tokens_user_id_index ON public.oauth_access_tokens USING btree (user_id);


--
-- Name: oauth_auth_codes_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_auth_codes_user_id_index ON public.oauth_auth_codes USING btree (user_id);


--
-- Name: oauth_clients_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_clients_user_id_index ON public.oauth_clients USING btree (user_id);


--
-- Name: oauth_refresh_tokens_access_token_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX oauth_refresh_tokens_access_token_id_index ON public.oauth_refresh_tokens USING btree (access_token_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: permission_role_permission_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX permission_role_permission_id_index ON public.permission_role USING btree (permission_id);


--
-- Name: permission_role_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX permission_role_role_id_index ON public.permission_role USING btree (role_id);


--
-- Name: permissions_key_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX permissions_key_index ON public.permissions USING btree (key);


--
-- Name: user_roles_role_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_roles_role_id_index ON public.user_roles USING btree (role_id);


--
-- Name: user_roles_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_roles_user_id_index ON public.user_roles USING btree (user_id);


--
-- Name: categories categories_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public.categories(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: category_attributes category_attributes_attribute_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_attributes
    ADD CONSTRAINT category_attributes_attribute_type_id_foreign FOREIGN KEY (attribute_type_id) REFERENCES public.attribute_types(id);


--
-- Name: category_attributes category_attributes_shop_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_attributes
    ADD CONSTRAINT category_attributes_shop_category_id_foreign FOREIGN KEY (shop_category_id) REFERENCES public.shop_categories(id);


--
-- Name: category_filters category_filters_category_attribute_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_filters
    ADD CONSTRAINT category_filters_category_attribute_id_foreign FOREIGN KEY (category_attribute_id) REFERENCES public.category_attributes(id);


--
-- Name: category_filters category_filters_shop_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_filters
    ADD CONSTRAINT category_filters_shop_category_id_foreign FOREIGN KEY (shop_category_id) REFERENCES public.shop_categories(id);


--
-- Name: colors colors_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.colors
    ADD CONSTRAINT colors_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: colors colors_size_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.colors
    ADD CONSTRAINT colors_size_id_foreign FOREIGN KEY (size_id) REFERENCES public.sizes(id);


--
-- Name: data_rows data_rows_data_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_rows
    ADD CONSTRAINT data_rows_data_type_id_foreign FOREIGN KEY (data_type_id) REFERENCES public.data_types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: main_section_products main_section_products_main_section_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_section_products
    ADD CONSTRAINT main_section_products_main_section_id_foreign FOREIGN KEY (main_section_id) REFERENCES public.main_sections(id);


--
-- Name: main_section_products main_section_products_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_section_products
    ADD CONSTRAINT main_section_products_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: menu_items menu_items_menu_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES public.menus(id) ON DELETE CASCADE;


--
-- Name: order_details order_details_color_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_details
    ADD CONSTRAINT order_details_color_id_foreign FOREIGN KEY (color_id) REFERENCES public.colors(id);


--
-- Name: order_details order_details_order_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_details
    ADD CONSTRAINT order_details_order_id_foreign FOREIGN KEY (order_id) REFERENCES public.orders(id);


--
-- Name: order_details order_details_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_details
    ADD CONSTRAINT order_details_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: orders orders_delivery_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_delivery_type_id_foreign FOREIGN KEY (delivery_type_id) REFERENCES public.delivery_types(id);


--
-- Name: orders orders_order_status_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_order_status_id_foreign FOREIGN KEY (order_status_id) REFERENCES public.order_statuses(id);


--
-- Name: orders orders_payment_status_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_payment_status_id_foreign FOREIGN KEY (payment_status_id) REFERENCES public.payment_statuses(id);


--
-- Name: orders orders_payment_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_payment_type_id_foreign FOREIGN KEY (payment_type_id) REFERENCES public.payment_types(id);


--
-- Name: orders orders_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: product_flags product_flags_flag_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_flags
    ADD CONSTRAINT product_flags_flag_id_foreign FOREIGN KEY (flag_id) REFERENCES public.flags(id);


--
-- Name: product_flags product_flags_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_flags
    ADD CONSTRAINT product_flags_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: products products_display_product_type_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_display_product_type_id_foreign FOREIGN KEY (display_product_type_id) REFERENCES public.display_product_types(id);


--
-- Name: products products_shop_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_shop_category_id_foreign FOREIGN KEY (shop_category_id) REFERENCES public.shop_categories(id);


--
-- Name: purchase_returns purchase_returns_color_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns
    ADD CONSTRAINT purchase_returns_color_id_foreign FOREIGN KEY (color_id) REFERENCES public.colors(id);


--
-- Name: purchase_returns purchase_returns_order_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns
    ADD CONSTRAINT purchase_returns_order_id_foreign FOREIGN KEY (order_id) REFERENCES public.orders(id);


--
-- Name: purchase_returns purchase_returns_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns
    ADD CONSTRAINT purchase_returns_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: purchase_returns purchase_returns_return_status_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns
    ADD CONSTRAINT purchase_returns_return_status_id_foreign FOREIGN KEY (return_status_id) REFERENCES public.return_statuses(id);


--
-- Name: purchase_returns purchase_returns_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase_returns
    ADD CONSTRAINT purchase_returns_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: reviews reviews_product_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_product_id_foreign FOREIGN KEY (product_id) REFERENCES public.products(id);


--
-- Name: reviews reviews_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: user_roles user_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: user_roles user_roles_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: users users_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- PostgreSQL database dump complete
--

