console.log('sdsds')
function tinymce_init_callback(editor)
{
    editor.remove();
    editor = null;

    tinymce.init({
        selector: 'textarea.richTextBox',
        skin: 'voyager',
        min_height: 100,
        height: 200,
        language: 'ru',
        resize: 'vertical',
        plugins: "textcolor, table",
        extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
        file_browser_callback: function (field_name, url, type, win) {
            if (type == 'image') {
                $('#upload_file').trigger('click');
            }
        },
        toolbar:
            "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | undo redo | styleselect | forecolor backcolor | fontselect | fontsizeselect  |bold italic | alignleft aligncenter alignright alignjustify | outdent indent",
        convert_urls: false,
        image_caption: true,
        image_title: true,
        valid_elements:'div[*],p[*],span[*],ul[*],li[*],ol[*],hr,br,img[*],-b,-i,-u',
        valid_children:'+li[span|p|div]'
    });
}
