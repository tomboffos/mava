/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/product.js":
/*!*********************************!*\
  !*** ./resources/js/product.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  var arrayOfBlocks = [];
  var product_desc = $('.product__description-content')[0];
  var tab_class = $('.product__tabs')[0];
  var curItem = 1;
  $(".".concat(curItem)).addClass('activeClass');

  for (var x = 0; x < product_desc.children.length; x++) {
    arrayOfBlocks.push("".concat(product_desc.children[x].classList[0]));
  }

  for (var _x = 1; _x < arrayOfBlocks.length; _x++) {
    $(".".concat(arrayOfBlocks[_x])).fadeOut();
  }

  var last = arrayOfBlocks[0];

  for (var _x2 = 0; _x2 < tab_class.children.length; _x2++) {
    $(".".concat(tab_class.children[_x2].classList[0])).on('click', function (id) {
      var curElement = arrayOfBlocks[parseInt(id.currentTarget.classList[0]) - 1];
      $(".".concat(last)).fadeOut(100);
      $(".".concat(id.currentTarget.classList[0])).addClass('activeClass');
      $(".".concat(id.currentTarget.classList[0], " span")).css('color', 'white');

      if (curItem !== id.currentTarget.classList[0]) {
        $(".".concat(curItem)).removeClass('activeClass');
        $(".".concat(curItem, " span")).css('color', 'gray');
      }

      $(".".concat(curElement)).fadeIn(100);
      last = curElement;
      curItem = id.currentTarget.classList[0];
    });
  }

  var men = $('.header__catalog1-link');

  var _loop = function _loop(_x3) {
    $(".".concat(men[_x3].classList[1])).on("mouseover", function () {
      $('.header__catalog1-second').css("display", "inline-block");
      $('.header__catalog1-second .header__catalog1-link').text(_x3);
    });
  };

  for (var _x3 = 0; _x3 < men.length / 2; _x3++) {
    _loop(_x3);
  }
});

/***/ }),

/***/ 2:
/*!***************************************!*\
  !*** multi ./resources/js/product.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\OpenServer\domains\mava\resources\js\product.js */"./resources/js/product.js");


/***/ })

/******/ });