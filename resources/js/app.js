require('./bootstrap');
window.Vue = require("vue");
import attribute_component from '../../resources/js/components/admin/AttributeComponent'
Vue.component(
    "attribute-component",
    require("./components/admin/AttributeComponent").default
);
const app = new Vue({
    el: "#attribute-component",
    attribute_component
});
