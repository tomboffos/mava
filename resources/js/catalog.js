require('./bootstrap');
window.Vue = require("vue");
import catalog_component from '../../resources/js/components/CatalogComponent'
Vue.component(
    "catalog_component",
    require("./components/CatalogComponent").default
);
const app = new Vue({
    el: "#catalog",
    catalog_component
});
