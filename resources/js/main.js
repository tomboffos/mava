import Swal from "sweetalert2";
$(function () {

    let flag = 0;
    const array = [
        ["kollege", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["nassad", "niusha", "what is up", "asda", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["lkaskldklas", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["andzmnxmnczrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["askdka", "niusha", "what is up", "asdsad", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["zmxcmnz", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["askdnmsa", "niusha", "what is up", "asds", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["zxmcnmz", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["asqkwja", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["asadzxc", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["dsfsd", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"],
        ["qweqw", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up", "andrey", "niusha", "what is up"]
    ];
    const second_array = [
        ["besties", "niusha"],
        ["why not", "niusha", "what is up"],
        ["college", "niusha"],
        ["school", "niusha"],
        ["SDU", "niusha"],
        ["Number One", "niusha", "what is up"],
        ["askdnmsa", "niusha"],
        ["zxmcnmz", "niusha"],
        ["asqkwja", "niusha"],
        ["asadzxc", "niusha"],
        ["dsfsd", "niusha"],
        ["qweqw", "niusha"]]


    $(".catalog__item-favorite img").click(function () {
        if (flag === 0) $('.catalog__item-favorite img')[0].src = "img/catalog/favorite-item.svg"
        else $('.catalog__item-favorite img')[0].src = "img/catalog/favorite-item-active.svg"

        flag = flag === 0 ? 1 : 0
    });

    const disappear = () => {
        $('.header__catalog1-next').css("display", "none")
        $('.header__catalog1-next2').css("display", "none")
    }
    for (let x = 0; x < $('.header__catalog1-menu')[0].children.length; x++) {
        const child = $('.header__catalog1-menu')[0].children[x]
        for (let y = 0; y < child.children.length; y++)
            child.children[y].classList.add(y)
    }
    var x = window.matchMedia("(max-width: 1099px)")
    if (x.matches){
        $('.header__catalog1-inner a').attr('href','#')
    }
    const clickAndMouse = async (media, tags, el, x) => {
        if (media.matches)
            tags.children[x].onclick = async () => {
                const val = $(el)

                const response = await fetch(`/api/category/name?category=${tags.children[x].getAttribute('data-category')}`)
                let data = await response.json()

                let curArr = data.categories

                if (curArr.length) {
                    val.css("display", "inline-block")


                    for (let y = 0; y < val[0].children.length; y++) {
                        if (curArr[y] != null) {
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].text = curArr[y].name
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].setAttribute('data-category', curArr[y].id)
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].setAttribute('href', `/shop/${curArr[y].id}`)

                        } else {
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].text = ''
                        }

                    }
                }
            }
        else
            tags.children[x].onmouseover = async () => {
                const val = $(el)

                const response = await fetch(`/api/category/name?category=${tags.children[x].getAttribute('data-category')}`)
                let data = await response.json()

                let curArr = data.categories

                if (curArr.length) {
                    val.css("display", "inline-block")


                    for (let y = 0; y < val[0].children.length; y++) {
                        if (curArr[y] != null) {
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].text = curArr[y].name
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].setAttribute('data-category', curArr[y].id)
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].setAttribute('href', `/shop/${curArr[y].id}`)

                        } else {
                            $(`${el} .${val[0].children[y].classList[1]}`)[0].text = ''
                        }

                    }
                }
            }

    }
    $('.header__catalog1-menu').hover(function(){

    },function(){
        $(this).css('height','0')
        handler(media, height)
        media.addListener(handler)
        height = height === 0 ? size : 0
        // disappear()
    })
    const eventsForShow = (tags, x, el) => {
        $(el).css("display", "none")
        var media = window.matchMedia("(max-width: 1199px)")
        clickAndMouse(media, tags, el, x)
        media.addListener(clickAndMouse)
    }

    const tags = $('.header__catalog1-inner')[0]
    const menu_appear = $('.header__catalog1-next')[0]
    for (let x = 0; x < tags.children.length; x++) {
        eventsForShow(tags, x, '.header__catalog1-next')
    }
    for (let x = 0; x < menu_appear.children.length; x++) {
        eventsForShow(menu_appear, x, '.header__catalog1-next2')
    }

    const menu = $('.header__catalog1-menu')
    let media = window.matchMedia("(max-width: 1199px)")
    let size = menu[0].offsetHeight
    let height = size
    menu.css({ "height": 0 })
    $(".header__catalog-btn").click(function () {
        handler(media, height)
        media.addListener(handler)
        height = height === 0 ? size : 0
        // disappear()
    });

    const handler = (media, height) => {
        if (media.matches) {
            $(".header__catalog1-menu").css({
                "position": "fixed",
                "width": "100%",
                "height": "100%",
                "left": 0,
                "overflow": "scroll",
                "top": 0,
                "z-index": 1001
            })
        }
        else {
            if (height !== 0) $(".header__catalog1-menu").css("height", `${height}px`)
            else $(".header__catalog1-menu").css("height", `${height}px`)
        }
    }

    let curAmount = 1
    const amount = $('.amount')


    $(".header__catalog").click(function () {
        $(".header__catalog-btn").toggleClass("clicked");
    })

    $('.main__items').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.brand-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    // $('.zoo-item').ZooMove();
    let icon_img = $('.choose_image')
    for(let x = 0;x < icon_img.length;x++){
        icon_img[x].addEventListener("click",()=>{
            $('#image_main').attr('src', icon_img[x].getAttribute("src"))
            // $('.zoo-img').css('backgroundImage', 'url(' + icon_img[x].getAttribute("src") + ')')
        })
    }

    $('.catalog__slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    // -----------------Modal Оставить Отзыв ----------------------
    $('[data-modal=reviewModal]').on('click', function () {
        $('.overlay, #reviewModal').fadeIn('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay, #reviewModal').fadeOut('slow')
    });

    // ----------------Modal Вход ---------------------
    $('[data-modal=login]').on('click', function () {
        $('.overlay2, #login').fadeIn('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay2, #login').fadeOut('slow')
    });

    // --------------Modal Регистрация -------------------
    $('[data-modal=registration]').on('click', function () {
        $('.overlay3, #registration').fadeIn('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay3, #registration').fadeOut('slow')
    });

    // -------------Modal Возврат Товара -------------------
    $('[data-modal=return]').on('click', function () {
        $('.overlay4, #return').fadeIn('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay4, #return').fadeOut('slow')
    });

    //-------------Modal Вы не приобрели этот товар ---------------------
    $('[data-modal=closeModal]').on('click', function () {
        $('.overlay5, #closeModal').fadeIn('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay5, #closeModal').fadeOut('slow')
    });

    $('.hamburger').on('click', function (e) {
        e.preventDefault;
        $(this).toggleClass('hamburger_active');
        return;
    });

    $('.hamburger').click(function () {
        if ($(".active__menu").is(":visible") == true) {
            $('.active__menu').hide();
            $('.header').removeClass("slick_active")
        } else {
            $('.active__menu').show();
            $(".header").addClass("slick_active")
        }
    });

    $('.account-click').on('click', function (e) {
        e.preventDefault;
        $(this).toggleClass('.popup_active');
        return;
    });

    $('.header__catalog1-close').on('click', function () {
        $('.header__catalog1-menu').css('height', '0'),
            $('.header__catalog1-link img').css('display', 'inline')
    });

    $('.header__catalog1-link').on('mouseover', function () {
        $('.header__catalog1-next').css('display', 'none')
    });
    $(document).on('click', function (){
        $('#dropdown_main_search').css('display', 'none')
    });
    $('.header__catalog1-link').on('click', function () {
        $('.header__catalog1-link img').css('display', 'none')
    });

    let flag1 = 1
    $('.ClickAcc').on('click', () => {
        if (flag1 === 1) $('.popup').css("display", "block")
    })

    $('.popup').on('mouseleave', () => {
        if (flag1 === 1) $('.popup').css("display", "none")
    })

    $('.popup__btn1, .popup__btn2').on('click', () => {
        if (flag1 === 1) $('.popup').css("display", "none")
    })

    // $('.product_add_basket').click(function(e){
    //     var butWrap = $(this).parents('.catalog__item-buy');
    //     butWrap.append('<div class="animtocart"></div>');
    //     $('.animtocart').css({
    //       'position' : 'absolute',
    //       'background' : '#FF0000',
    //       'width' :  '25px',
    //       'height' : '25px',
    //       'border-radius' : '100px',
    //       'z-index' : '9999999999',
    //       'left' : e.pageX-25,
    //       'top' : e.pageY-25,
    //     });
    //     var cart = $('.product_add_basket').offset();
    //     $('.animtocart').animate({ top: cart.top + 'px', left: cart.left + 'px', width: 0, height: 0 }, 800, function(){
    //       $(this).remove();
    //     });
    //   });
    function numberWithSpaces(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
    $(document).mouseup(function (e){
        var block = $(".popup");
        if (!block.is(e.target)
            && block.has(e.target).length === 0) {
            block.hide();
        }
    });
    $('#image_main').ezPlus();
    $('.select_choose').on('change',function(){
        let variation = JSON.parse($(this).val())
        let product_id = $(this).attr('data-product-id')
        $(`.product_image_${product_id}`).attr('src',`/storage/${variation.image}`)
        $(`.product_add_basket__${product_id}`).attr('data-variation-id',variation.id)
        $(`.product_span_price_${product_id}`).html(numberWithSpaces(variation.price))
    })

    $('.product_add_basket').on('click', async function () {
        console.log($(this))
        let product_id = $(this).attr('data-product-id')
        let variation_id = $(this).attr('data-variation-id')
        let quantity = $(this).attr('data-quantity')

        if (variation_id != null){
            const response = await fetch(`/add/basket/${product_id}?variation_id=${variation_id}${quantity != null ? '&quantity='+quantity : ''}`)
            if (response.status === 200){
                let data = await response.json()
                Swal.fire(data['message'])
                if ($(this).hasClass('basket_button')){
                    setTimeout(()=>{
                        window.location.href='/basket'

                    },1000)
                }


            }
        }else{
            const response = await fetch(`/add/basket/${product_id}`)
            if (response.status === 200){
                let data = await response.json()

                if ($(this).hasClass('basket_button')){
                    setTimeout(()=>{
                        window.location.href='/basket'

                    },500)
                }else{
                    Swal.fire(data['message'])
                }

            }

        }

    })

    $('.product_add_favourite').on('click', async function () {
        let product_id = $(this).attr('data-product-id')
        const response = await fetch(`/add/favourite/${product_id}`)
        if (response.status === 200) {
            $(this).attr('src','/img/product-page/red_heart.svg')
            let data = await response.json()

            Swal.fire(data['message'])
        } else {
            let data = await response.json()
            $(this).attr('src','/img/catalog/favorite-item.svg')

            Swal.fire(data['message'])
        }
    })

    $('.favorite_link').on('click', function (e) {
        e.preventDefault()
        Swal.fire('Нужно зарегистрироваться чтобы посмотреть избранное')
    })

    let user_id = $('#user_id')
    var frm = $('#orderForm');

    frm.submit(function (e) {

        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/create/order',
            data: frm.serialize(),
            success: function (data) {
                Swal.fire(data['message'])


                if(!user_id.length){

                    setTimeout(()=>{
                        window.location.href=data['redirect_url']

                    },1000)
                }else{
                    setTimeout(()=>{
                        window.location.href=data['redirect_url']

                    },1000)

                }
            },
            error: function (data) {
                Swal.fire(data['message'])
            },
        });
    });

    $('.review_point').on('click', function () {
        let star = $(this).attr('data-star')
        $('star_input').attr('data-star', star)
    })

    // $('.catalog__item2').slick({
    //     slidesToShow: 5,
    //     slidesToScroll: 5,
    //     arrows: false
    // });
    $('#submit_return').submit(function(e){
        e.preventDefault()
        $.ajax({
            type:'POST',
            url:'/api/submit/return',
            data:$('#submit_return').serialize(),
            success:function(data){
                $('.overlay4').css('display','none')
                $('#return').css('display','none')
                Swal.fire(data['message'])

            },
            error:function(data){
                $('.overlay4').css('display','none')
                $('#return').css('display','none')
                Swal.fire(data['message'])
            }
        })
    })



});
