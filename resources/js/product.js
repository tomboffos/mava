$(function () {
    let arrayOfBlocks = []
    const product_desc = $('.product__description-content')[0]
    const tab_class = $('.product__tabs')[0]
    let curItem = 1
    $(`.${curItem}`).addClass('activeClass')
    for (let x = 0; x < product_desc.children.length; x++) arrayOfBlocks.push(`${product_desc.children[x].classList[0]}`);
    for (let x = 1; x < arrayOfBlocks.length; x++) $(`.${arrayOfBlocks[x]}`).fadeOut()
    let last = arrayOfBlocks[0]
    for (let x = 0; x < tab_class.children.length; x++)
        $(`.${tab_class.children[x].classList[0]}`).on('click', (id) => {
            let curElement = arrayOfBlocks[parseInt(id.currentTarget.classList[0]) - 1];
            $(`.${last}`).fadeOut(100)
            $(`.${id.currentTarget.classList[0]}`).addClass('activeClass')
            $(`.${id.currentTarget.classList[0]} span`).css('color', 'white')
            if (curItem !== id.currentTarget.classList[0]) {
                $(`.${curItem}`).removeClass('activeClass')
                $(`.${curItem} span`).css('color', 'gray')
            }
            $(`.${curElement}`).fadeIn(100)
            last = curElement
            curItem = id.currentTarget.classList[0]
        })
    let men = $('.header__catalog1-link')
    for (let x = 0; x < men.length / 2; x++) {
        $(`.${men[x].classList[1]}`).on("mouseover", () => {
            $('.header__catalog1-second').css("display", "inline-block")
            $('.header__catalog1-second .header__catalog1-link').text(x)
        })

    }

})