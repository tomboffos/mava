@extends('layout.app')
@section('content')
    @php $user = session()->get('user'); @endphp
    <div class="container-fluid">

        <section class="account">
            <div class="account__inner">
                <div class="account__navbar">
                    <div class="account__photo">
                        <img src="{{asset('storage/'.$user->avatar)}}" alt="" height="150px" width="150px"/>
                    </div>
                    <div class="account__name">{{$user->name}}</div>
                    <div class="account__links">
                        <a href="{{route('user')}}"><div class="account__link">Личные данные</div></a>
                        <a href="{{route('orders')}}">
                            <div class="account__link">Мои заказы</div>

                        </a>
                        <a href="{{route('favourite')}}"><div class="account__link">Избранное</div></a>
                        <a href="{{route('logout')}}"><div class="account__link">Выход</div></a>

                    </div>
                </div>
                <div class="account__wrap">
                    <div class="account__favorite">
                <div class="account__favorite-inner">
                  <div class="catalog__favorite-items">
                      @foreach($basketMain as $product)
                        <div class="catalog__favorite-item">
                          <div class="catalog__favorite-item-wrap">
                            <div class="catalog__favorite-item-favorite">
                              <img
                                class="favorite-block product_add_favourite"
                                src="/img/product-page/red_heart.svg"
                                alt=""
                                data-product-id="{{$product->id}}"

                              />
                            </div>
                          </div>
                          <div class="catalog__favorite-item-img">
                              <a href="{{route('product',$product->id)}}">

                                <img src="{{asset('storage/'.$product->image)}}" width="100%" alt="product" />
                              </a>
                          </div>
                            <a href="{{route('product',$product->id)}}">
                                <div class="catalog__favorite-item-title">
                                {{$product->name}}
                              </div>
                            </a>
                          <div
                            class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                          >
                              @if($product->stock)
                                  В наличии
                                  @else
                                Нету в складе
                              @endif
                          </div>
                          <div class="catalog__favorite-item-wrap2">
                            <div class="catalog__favorite-item-cost">
                              <span>{{$product->price}}</span> тг.
                            </div>
                            <div class="catalog__favorite-item-buy">
                              <img src="img/catalog/buy.png" alt="basket" class="product_add_basket" data-product-id="{{$product->id}}"/>
                            </div>
                          </div>
                        </div>
                      @endforeach


                  </div>
                </div>
              </div>

                </div>
            </div>
        </section>
@endsection
