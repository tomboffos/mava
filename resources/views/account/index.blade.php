@extends('layout.app')
@section('content')
<div class="container-fluid">

    <section class="account">
        <div class="account__inner">
            <div class="account__navbar">
                <div class="account__photo">
                    <img src="{{asset('storage/'.$user->avatar)}}" alt="" height="150px" width="150px"/>
                </div>
                <div class="account__name">{{$user->name}}</div>
                <div class="account__links">
                    <a href="{{route('user')}}"><div class="account__link">Личные данные</div></a>
                    <a href="{{route('orders')}}"><div class="account__link">Мои заказы</div></a>
                    <a href="{{route('favourite')}}"><div class="account__link">Избранное</div></a>
                    <a href="{{route('logout')}}"><div class="account__link">Выход</div></a>
                </div>
            </div>
            <div class="account__wrap">
                <!-- =============== Раздел - Личные данные ================ -->

           <div class="account__personal-date">
                  <div class="account__personal-date-title">Контактные данные</div>
                  <form action="{{route('update')}}" method="post" class="account__personal-date-form">
                      {{csrf_field()}}
                    <div class="account__personal-date-items">
                        <input type="hidden" value="{{$user->id}}" name="user_id">
                      <div class="account__personal-date-box">
                        Имя
                        <input type="text" minlength="4" name="name" class="account__personal-date-input"  value="{{$user->name}}"/>
                      </div>
                      <div class="account__personal-date-box">
                        Фамилия
                        <input type="text" name="surname" class="account__personal-date-input" value="{{$user->surname}}"/>
                      </div>
                      <div class="account__personal-date-box">
                        Телефон
                        <input type="number" name="phone" class="account__personal-date-input" value="{{$user->phone}}"/>
                      </div>
                      <div class="account__personal-date-box">
                        E-mail
                        <input type="email" name="email" class="account__personal-date-input" value="{{$user->email}}"/>
                      </div>
                    </div>
                    <div class="account__personal-date-male">
                      <span>Пол:</span>
                      <div class="account__personal-date-check">
                        <input
                          type="checkbox"
                          class="account__personal-date-maleInput"
                          name="is_male"
                          id="is_male"
                          onclick="document.getElementById('is_female').checked=false"

                            {{$user->is_male == 1 ? 'checked': ''}}
                        />
                        Мужской
                      </div>
                      <div class="account__personal-date-check">
                        <input
                          type="checkbox"
                          class="account__personal-date-maleInput"
                          name="is_female"
                          id="is_female"
                          onclick="document.getElementById('is_male').checked=false"
                          {{$user->is_male == 0 ? 'checked': ''}}

                        />
                        Женский
                      </div>
                    </div>
                    <button type="submit" class="button account__personal-date-btn">
                      Сохранить изменения
                    </button>
                  </form>
                </div>
                <!-- =============== Раздел - Мои заказы =================== -->

                <!-- <div class="account__order">
                  <div class="account__order-items">
                    <div class="account__order-item">
                      <div class="account__order-wrap">
                        <div class="account__order-number">№323-31-81</div>
                        <div class="account__order-delivery">
                          Доставлено <span>18.09.2019 00:06</span>
                        </div>
                        <div class="account__order-cost">Сумма 9 591 тг.</div>
                      </div>
                      <div class="account__order-wrap">
                        <div class="account__order-productPhoto">
                          <img src="img/account-page/order-photo.png" alt="" />
                        </div>
                      </div>
                    </div>

                    <div class="account__order-item">
                      <div class="account__order-wrap">
                        <div class="account__order-number">№323-31-81</div>
                        <div class="account__order-delivery">
                          Доставлено <span>18.09.2019 00:06</span>
                        </div>
                        <div class="account__order-cost">Сумма 9 591 тг.</div>
                      </div>
                      <div class="account__order-wrap">
                        <div class="account__order-productPhoto">
                          <img src="img/account-page/order-photo.png" alt="" />
                        </div>
                      </div>
                    </div>

                    <div class="account__order-item">
                      <div class="account__order-wrap">
                        <div class="account__order-number">№323-31-81</div>
                        <div class="account__order-delivery">
                          Доставлено <span>18.09.2019 00:06</span>
                        </div>
                        <div class="account__order-cost">Сумма 9 591 тг.</div>
                      </div>
                      <div class="account__order-wrap">
                        <div class="account__order-productPhoto">
                          <img src="img/account-page/order-photo.png" alt="" />
                        </div>
                      </div>
                    </div>

                    <div class="account__order-item">
                      <div class="account__order-wrap">
                        <div class="account__order-number">№323-31-81</div>
                        <div class="account__order-delivery">
                          Доставлено <span>18.09.2019 00:06</span>
                        </div>
                        <div class="account__order-cost">Сумма 9 591 тг.</div>
                      </div>
                      <div class="account__order-wrap">
                        <div class="account__order-productPhoto">
                          <img src="img/account-page/order-photo.png" alt="" />
                        </div>
                      </div>
                    </div>

                    <div class="account__order-item">
                      <div class="account__order-wrap">
                        <div class="account__order-number">№323-31-81</div>
                        <div class="account__order-delivery">
                          Доставлено <span>18.09.2019 00:06</span>
                        </div>
                        <div class="account__order-cost">Сумма 9 591 тг.</div>
                      </div>
                      <div class="account__order-wrap">
                        <div class="account__order-productPhoto">
                          <img src="img/account-page/order-photo.png" alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->

                <!-- =============== Раздел - Мой заказ ==================== -->

                <!-- <div class="account__product">
                  <div class="account__product-inner">
                    <div class="account__product-wrap">
                      <div class="account__product-link">
                        <img src="img/account-page/arrow.svg" alt="" /> Мой заказ
                      </div>
                      <div class="account__product-wraps">
                        <div class="account__product-number">№323-31-81</div>
                        <div class="account__product-status">
                          Статус заявки: рассмотрение заявки возврат
                        </div>
                        <div class="account__product-box">
                          <img
                            class="account__product-img"
                            src="img/account-page/order-photo.png"
                            alt=""
                          />
                          <div class="account__product-descr">
                            Уильямс Р.: Аниматор: набор для выживания. Секреты и
                            методы создания анимации, 3D-графики и компьютерных игр
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="account__product-items">
                      <div class="account__product-item">
                        <div class="account__product-title">
                          Информация о доставке
                        </div>
                        <div class="account__product-itemWrap">
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Доставлено:
                            </div>
                            <div class="account__product-itemSubtitle">
                              18.09.2019 00:06
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">Сумма:</div>
                            <div class="account__product-itemSubtitle">
                              9 591 тг.
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">Продавец:</div>
                            <div class="account__product-itemSubtitle">Mava</div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Получатель:
                            </div>
                            <div class="account__product-itemSubtitle">
                              Купря Резонанс
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">Телефон:</div>
                            <div class="account__product-itemSubtitle">
                              8 777 454 55 22
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">Почта:</div>
                            <div class="account__product-itemSubtitle">
                              123456@gmail.com
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Адрес доставки:
                            </div>
                            <div class="account__product-itemSubtitle">
                              Алматы, Курмангазы 124А, кв 4
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="account__product-item">
                        <div class="account__product-title">
                          Информация об оплате
                        </div>
                        <div class="account__product-itemWrap">
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Стоимость товаров:
                            </div>
                            <div class="account__product-itemSubtitle">
                              9561 тг.
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Стоимость доставки:
                            </div>
                            <div class="account__product-itemSubtitle">
                              Бесплатно
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">Итого:</div>
                            <div class="account__product-itemSubtitle">
                              9561 тг.
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Способ оплаты:
                            </div>
                            <div class="account__product-itemSubtitle">
                              Платежная карта
                            </div>
                          </div>
                          <div class="account__product-itemBox">
                            <div class="account__product-itemTitle">
                              Статус оплаты:
                            </div>
                            <div class="account__product-itemSubtitle">Оплачен</div>
                          </div>
                          <button
                            data-modal="return"
                            class="account__product-itemBtn button"
                          >
                            Возврат товара
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->

                <!-- =============== Раздел - Избранные ==================== -->
                <!-- <div class="account__favorite">
                  <div class="account__favorite-inner">
                    <div class="catalog__favorite-items">
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                      <div class="catalog__favorite-item">
                        <div class="catalog__favorite-item-wrap">
                          <div class="catalog__favorite-item-favorite">
                            <img
                              class="favorite-block"
                              src="img/catalog/favorite-item.svg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div class="catalog__favorite-item-img">
                          <img src="img/catalog/tovar.png" alt="product" />
                        </div>
                        <div class="catalog__favorite-item-title">
                          Наушники Elari NanoPods BT черный
                        </div>
                        <div
                          class="catalog__favorite-item-subtitle catalog__favorite-item-NoProduct"
                        >
                          Нету в складе
                        </div>
                        <div class="catalog__favorite-item-wrap2">
                          <div class="catalog__favorite-item-cost">
                            <span>4500</span> тг.
                          </div>
                          <div class="catalog__favorite-item-buy">
                            <img src="img/catalog/buy.png" alt="basket" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
            </div>
        </div>
    </section>
</div>

<!-- ------------ Модалка возврат товара --------------------- -->
<div class="overlay4">
    <div class="modalReturn" id="return">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Заявка на возврат товара</div>
        <form action="" class="modal__form">
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">Имя</div>
                <input type="text" minlength="4" class="modal__input" />
            </div>
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">Телефон</div>
                <input type="number" class="modal__input" />
            </div>

            <div class="modal__wrap" style="margin-top: 24px">
                <div class="modal__form-title">Причина возврата</div>
                <textarea name="" id="textareaModal"></textarea>
            </div>
            <div class="modal__btns">
                <button class="popup__btn1">Отправить заявку</button>
            </div>
        </form>
    </div>
</div>

@endsection

