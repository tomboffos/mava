@extends('layout.app')
@section('content')
    @php $user = session()->get('user'); @endphp
    <div class="container-fluid">

        <section class="account">
            <div class="account__inner">
                <div class="account__navbar">
                    <div class="account__photo">
                        <img src="{{asset('storage/'.$user->avatar)}}" alt="" height="150px" width="150px"/>
                    </div>
                    <div class="account__name">{{$user->name}}</div>
                    <div class="account__links">
                        <a href="{{route('user')}}"><div class="account__link">Личные данные</div></a>
                        <a href="{{route('orders')}}">
                            <div class="account__link">Мои заказы</div>

                        </a>
                        <a href="{{route('favourite')}}"><div class="account__link">Избранное</div></a>
                        <a href="{{route('logout')}}"><div class="account__link">Выход</div></a>

                    </div>
                </div>
                <div class="account__wrap">
                    <div class="account__personal-data">
                         <div class="account__product">
                          <div class="account__product-inner">
                            <div class="account__product-wrap">
                              <div class="account__product-link">
                                <img src="img/account-page/arrow.svg" alt="" /> Мой заказ
                              </div>
                              <div class="account__product-wraps">
                                <div class="account__product-number">{{$order->order_number != null ? $order->order_number : '#'.$order->created_at->day.$order->created_at->month.'-'.$order->created_at->year.'-'.$order->id}}</div>
                                <div class="account__product-status">
                                  Статус заявки: @if($order->order_status ){{$order->order_status->name}} @endif
                                </div>
                                  @foreach($order->details as $detail)
                                      <a href="{{route('product',$detail->product->id)}}">

                                          <div class="account__product-box">
                                              @if($detail->product_variation_id != null )
                                                  <img
                                                      class="account__product-img"
                                                      src="{{'/storage/'.
                                                        \App\Models\ProductVariation::find($detail->product_variation_id)->image
                                                        }}"
                                                      alt=""
                                                  />
                                                  @else
                                                  <img
                                                      class="account__product-img"
                                                      src="{{'/storage/'.$detail->product->image}}"
                                                      alt=""
                                                  />
                                              @endif

                                              @if($detail->product)

                                                  <div class="account__product-descr">
                                                      {!!$detail->product->name!!}

                                                  </div>
                                                  <div class="account__product-descr">
                                                      {!!$detail->unit_quantity!!} шт

                                                  </div>
                                                  <div class="account__product-descr">
                                                      {!!
                                                        $detail->product_variation_id != null ?
                                                         \App\Models\ProductVariation::where('id',$detail->product_variation_id)->first()->name
                                                         :
                                                         ''
                                                      !!}

                                                  </div>
                                                  <div class="account__product-descr">
                                                      @php
                                                          $name = $order->returns->where('product_id',$detail->product->id)->first()
                                                      @endphp
                                                      @if(isset($name))
                                                          Статус возврата : {{$name['status']['name']}}

                                                      @endif
                                                  </div>
                                              @endif
                                          </div>

                                      </a>

                                  @endforeach
                              </div>
                            </div>
                            <div class="account__product-items">
                              <div class="account__product-item">
                                <div class="account__product-title">
                                  Информация о доставке
                                </div>
                                <div class="account__product-itemWrap">
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Доставлено:
                                    </div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->created_at}}
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">Сумма:</div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->unit_price}} тг.
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">Продавец:</div>
                                    <div class="account__product-itemSubtitle">Mava</div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Получатель:
                                    </div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->name}}
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">Телефон:</div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->phone}}
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">Почта:</div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->email}}
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Адрес доставки:
                                    </div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->country_city}} {{$order->address}}
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="account__product-item">
                                <div class="account__product-title">
                                  Информация об оплате
                                </div>
                                <div class="account__product-itemWrap">
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Стоимость товаров:
                                    </div>
                                    <div class="account__product-itemSubtitle">
                                      {{$order->unit_price}} тг.
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Стоимость доставки:
                                    </div>
                                    <div class="account__product-itemSubtitle">
                                      Бесплатно
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">Итого:</div>
                                    <div class="account__product-itemSubtitle">
                                        {{$order->unit_price}} тг.
                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Способ оплаты:
                                    </div>
                                    <div class="account__product-itemSubtitle">
                                        {{$order->payment_type->name}}

                                    </div>
                                  </div>
                                  <div class="account__product-itemBox">
                                    <div class="account__product-itemTitle">
                                      Статус оплаты:
                                    </div>
                                    <div class="account__product-itemSubtitle">{{$order->payment_status->name}}</div>
                                  </div>
                                    <button
                                        data-modal="return"
                                        class="account__product-itemBtn button"
                                    >
                                        Возврат товара
                                    </button>


                                </div>
                              </div>
                            </div>
                          </div>
                        </div>


                    </div>

                </div>
            </div>
        </section>
    </div>

@endsection
