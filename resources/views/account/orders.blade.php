@extends('layout.app')
@section('content')
    @php $user = session()->get('user'); @endphp
    <div class="container-fluid">

        <section class="account">
            <div class="account__inner">
                <div class="account__navbar">
                    <div class="account__photo">
                        <img src="{{asset('storage/'.$user->avatar)}}" alt="" height="150px" width="150px"/>
                    </div>
                    <div class="account__name">{{$user->name}}</div>
                    <div class="account__links">
                        <a href="{{route('user')}}"><div class="account__link">Личные данные</div></a>
                        <a href="{{route('orders')}}">
                            <div class="account__link">Мои заказы</div>

                        </a>
                        <a href="{{route('favourite')}}"><div class="account__link">Избранное</div></a>
                        <a href="{{route('logout')}}"><div class="account__link">Выход</div></a>

                    </div>
                </div>
                <div class="account__wrap">
                    <div class="account__personal-data">
                        <div class="account__order">
                            <div class="account__order-items">

                                @foreach($orders as $order)
                                    <a href="{{route('orderMain',$order->id)}}">
                                        <div class="account__order-item">
                                            <div class="account__order-wrap">
                                                <div class="account__order-number">
                                                    {{$order->order_number != null ? $order->order_number : '#'.$order->created_at->day.$order->created_at->month.'-'.$order->created_at->year.'-'.$order->id}}
                                                </div>
                                                <div class="account__order-delivery">
                                                    @if($order->order_status)
                                                        {{$order->order_status->name}}
                                                    @endif
                                                </div>
                                                <div class="account__order-cost">Сумма {{$order->unit_price}} тг.</div>
                                            </div>
                                            <div class="account__order-wrap">

                                                <div class="account__order-productPhoto" style="width: 100px;height: 100px">
                                                    @if(isset($order->details[0]))
                                                        @if($order->details[0]->product_variation_id != null)
{{--                                                        <img src="{{asset('storage/'.\App\Models\ProductVariation::find($order->details[0]->product_variation_id)  != null ? \App\Models\ProductVariation::find($order->details[0]->product_variation_id)->image : null)}}" alt=""  style="width: 100px;height: 100px;"/>--}}
                                                        @else
                                                            @if(isset($order->details[0]->product->image))

                                                            <img src="{{asset('storage/'.$order->details[0]->product->image)}}" alt=""  style="width: 100px;height: 100px;"/>
                                                                @endif
                                                            @endif
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach


                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>
@endsection
