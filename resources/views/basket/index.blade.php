@extends('layout.app')
@section('content')
<div class="container-fluid">

    <section class="basket">
        <div class="basket__inner">
            <div class="basket__title">Корзина товаров</div>
            <div class="row">
                <div class="basket__items col-xl-9 col-md-8">
                    @foreach($basketMain as $product)
                    <div class="basket__item">
                        <div class="basket__item-inner">
                            <a href="{{route('product',$product->id)}}">
                                <input type="checkbox" checked>
                                @if($product['variation'] != null)

                                    <img class="basket__item-img" src="{{asset('storage/'.$product['variation']['image'])}}" style="width: 100px;" alt="">

                                @else
                                    <img class="basket__item-img" src="{{asset('storage/'.$product->image)}}" style="width: 100px;" alt="">
                                @endif
                            </a>


                        </div>
                        <div class="basket__item-wrap">
                            <a href="{{route('product',$product->id)}}">

                            <p style="color: #000;">
                                {{$product->name}}
                            </p>
                                <p style="color: #000;">
                                    {{$product['variation'] != null ? $product['variation']['name'] :''}}
                                </p>
                            </a>
                            <div class="basket__item-clicks">
                                <div class="basket__item-click product_add_favourite" data-product-id="{{$product->id}}">В избранное</div>
                                @if($product['variation'] != null)
                                    <a class="basket__item-click" href="{{route('deleteBasket',$product->id)}}?variation_id={{$product['variation']['id']}}" >Удалить</a>
                                @else
                                    <a class="basket__item-click" href="{{route('deleteBasket',$product->id)}}" >Удалить</a>
                                @endif
                            </div>
                        </div>
                        @if($product['variation'] != null)
                            <div class="basket__item-cost"><span>{{number_format($product['variation']['price'] * $product->quantity,0,'.',' ')}}</span>тг.</div>

                        @else
                        <div class="basket__item-cost"><span>{{number_format($product->price * $product->quantity,0,'.',' ')}}</span>тг.</div>
                        @endif
                        <div class="basket__item-counter">
                            <div class="buttons">
                                @if($product['variation'] != null)
                                <button class="minusButton" onclick="window.location.href='{{route('addMinus',$product->id)}}?variation_id={{$product['variation']['id']}}'"><img src="img/product-page/minus.svg" alt=""></button>
                                @else
                                    <button class="minusButton" onclick="window.location.href='{{route('addMinus',$product->id)}}'"><img src="img/product-page/minus.svg" alt=""></button>
                                @endif
                                <span class="amount">{{$product['quantity']}}</span>
                                @if($product['variation'] != null)

                                <button class="plusButton" onclick="window.location.href='{{route('addPlus',$product->id)}}?variation_id={{$product['variation']['id']}}'"><img src="img/product-page/plus.svg" alt=""></button>
                                @else
                                    <button class="plusButton" onclick="window.location.href='{{route('addPlus',$product->id)}}'"><img src="img/product-page/plus.svg" alt=""></button>

                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                <div class="basket__order col-xl-3 col-md-4">
                    <button @if(count($basketMain) === 0 )data-modal="closeModal" @else onclick="window.location.href='{{route('order')}}'" @endif class="button basket__btn">Оформить заказ</button>
                    <div class="basket__order-title">Ваша корзина</div>
                    <div class="basket__order-wrap">
                        <div>Товары <span>{{$quantitySum}}</span></div>
                        <div><span>{{number_format($sum,0,'.',' ')}}</span>тг.</div>
                    </div>
                    <div class="basket__order-wrap">
                        <div>Доставка </div>
                        <div>{{$sum >= 5000 ? 0 : 500}} тг</div>
                    </div>
                    <div class="basket__order-allCost">
                        <div>Общая стоимость</div>
                        <div><span>{{number_format($sum >= 5000 ? $sum : $sum+500,0,'.',' ')}}</span>тг.</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="catalog">
        <div class="catalog__inner">
            <div class="catalog__blocks catalog__blocks-product">
                <div class="catalog__wrap">
                    <div class="catalog__title">Рекомендованные товары</div>
                    <div class="catalog__showAll" onclick="window.location.href='{{route('shop')}}'">показать все</div>
                </div>
                <div class="catalog__items">
                    @foreach(\App\Models\RelatedProduct::with('product.product_flags.flag')->orderBy('order','asc')->paginate(5) as $related_product )
                        <div class="catalog__item">
                                <div class="catalog__item-wrap">
                                    @if(isset($product->products->product_flags[0]))

                                    <div class="catalog__item-discound" style="background-color: {{$related_product->product->product_flags[0]->flag->color}}')">
                                        {{$related_product->product->product_flags[0]->flag->name}}
                                    </div>
                                    @endif
                                    <div class="catalog__item-favorite product_add_favourite" data-product-id="{{$related_product->product->id}}">
                                        <img src="/img/catalog/favorite-item.svg" alt="" />
                                    </div>
                                </div>
                                <div class="catalog__item-img">
                                    <a href="{{route('product',$related_product->product->id)}}">

                                        @if(count($related_product->product->variations) > 0 )
                                            <img src="{{asset('storage/'.$related_product->product->variations[0]->image)}}" alt="" width="100%" />
                                        @else
                                            <img src="{{asset('storage/'.$related_product->product->image)}}" alt="" width="100%" />
                                        @endif
                                    </a>
                                </div>
                                <div class="catalog__item-title">
                                    <a href="{{route('product',$related_product->product->id)}}">

                                    {{$related_product->product->name}}
                                    </a>
                                </div>
                                <div class="catalog__item-subtitle catalog__item-NoProduct">
                                    @if($related_product->product->stock != 1)
                                        Нету в складе
                                    @else
                                        Есть в наличии

                                    @endif
                                </div>
                                <div class="catalog__item-wrap2">
                                    @if(count($related_product->product->variations)>0)

                                        <div  id="" class="form-control " style="border:none;font-weight:bold;font-size:12px;padding:0px;border-bottom:1px solid #31AC80;width: 35%;border-radius: 0px;">
                                            {{$related_product->product->variations[0]->name}}
                                        </div>
                                        <div class="catalog__item-cost"><span>{{number_format($related_product->product->variations[0]->price, 0, '.', ' ')}}</span> тг.</div>
                                    @else
                                        <div class="catalog__item-cost"><span>{{number_format($related_product->product->price,0,'.',' ')}}</span> тг.</div>
                                    @endif
                                    <div class="catalog__item-buy product_add_basket"  data-product-id="{{$related_product->product->id}}" @if(count($related_product->product->variations)>0) data-variation-id="{{$related_product->product->variations[0]->id}}" @endif>
                                        <img src="/img/catalog/buy.png" alt="basket" />
                                    </div>
                                </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
</div>

<!-- ------------- Modal Вы не приобрели этот товар ------------- -->
<div class="overlay5">
    <div class="modalClose" id="closeModal">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Вы не приобрели этот товар</div>
        <div class="modal__descr">Данные товар не был найден в списке того что вы купили. Отзыв можно остававлять только на те товары которые вы приобрели</div>
        <div class="modal__btns">
            <div class="modal__wrapp">
                <button class="popup__btn1">Отправить заявку</button>
            </div>
        </div>
    </div>
</div>
@endsection
