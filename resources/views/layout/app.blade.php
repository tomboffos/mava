<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Mava</title>
    <link rel="shortcut icon" href="{{asset('img/logowhite.svg')}}" type="image/svg">
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap"
        rel="stylesheet"
    />
    {{-- <link rel="stylesheet" href="{{asset('css/jquery.ez-plus.css')}}" /> --}}
    <link rel="stylesheet" href="{{asset('css/magnifier.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}" />
</head>
<body>

<section class="active__header">

    <div class="active__menu active__menu-left">
        <div class="container">
            <div class="active__header-inner">
                <div class="active__header-wrap">
                    <a href="{{route('about')}}" class="active__header-link">О компании</a>
                    <a href="{{route('delivery')}}" class="active__header-link">Условия доставки</a>
                    <a href="{{route('contacts')}}" class="active__header-link">Контакты</a>
                    <a href="" class="active__header-link active__header-loc"
                    >Алматы <img src="{{asset('img/main-page/location.svg')}}" alt="location"
                        /></a>


                </div>
            </div>
        </div>
    </div>
</section>
<header class="header">
    <div class="container-fluid">
        <div class="header__inner">
            <nav class="header__nav">
                <div class="row">
                    <div class="header__nav-wrap col-xl-4 col-lg-6 col-md-6">
                        <a href="{{route('about')}}" class="header__nav-link">О компании</a>
                        <a href="{{route('delivery')}}" class="header__nav-link">Условия доставки</a>
                        <a href="{{route('contacts')}}" class="header__nav-link">Контакты</a>
                    </div>
                    <div
                        class="header__nav-wrap2 offset-xl-6 col-xl-2 offset-lg-3 col-lg-3 col-md-4 offset-md-2"
                    >
                        <a href="" class="header__nav-location"
                        >Алматы <img src="{{asset('img/main-page/location.svg')}}" alt="location"
                            /></a>
                        <a href="tel:{{setting('site.phone')}}" class="header__nav-link"
                        >{{setting('site.phone')}}</a
                        >
                    </div>
                    <div class="header__menuMob">
                        <a href="/" class="header__nav-logoMobile">
                            <img src="{{asset('img/logo.svg')}}" alt="" />
                        </a>
                        <div class="hamburger">
                            <span></span>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="header__menu">
                <div class="header__catalog col-md-2">
                    <button class="header__catalog-btn header__catalog-btn-active"><span></span> Каталог</button>
                    <div class="header__catalog-mobileIn">
                        @foreach(\App\Models\ShopCategory::where('parent_id',null)->get() as $category)
                            <button class="header__catalog-btn header__catalog-mobileBtn" onclick="window.location.href='{{route('shop',$category->id)}}'">{{$category->name}}</button>
                        @endforeach
                    </div>
                </div>
                <div class="header__logo col-xl-2 col-lg-3 col-md-4">
                    <a href="/"
                    ><img src="{{asset('img/logo.svg')}}" alt="mava-logo"
                        /></a>
                </div>
                <div class="header__search col-xl-6 col-lg-5 col-md-3">
                    <form action="{{route('shop')}}">
                        <input type="text" placeholder="" name="search" value="" oninput="mainSearchFunction()" id="searchName"/><input class="submit_search_button" value="поиск" type="submit" style="display: block;">
                    </form>
                    <div class="dropdown_main_search" id="dropdown_main_search">

                    </div>
                </div>
                <div class="header__btns col-lg-2 col-md-3">
                    <a href="/favourite" @if(session()->get('user') == null) class="favorite_link" @endif><img src="{{asset('img/main-page/favorite.svg')}}" alt="" /></a>
                    <a href="/basket">
                        {{-- Иконка SVG код для заливки --}}
                        <div class="header__icon">
                        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.4 22.4C6.86 22.4 5.6 23.66 5.6 25.2C5.6 26.74 6.86 28 8.4 28C9.94 28 11.2 26.74 11.2 25.2C11.2 23.66 9.94 22.4 8.4 22.4ZM0 0V2.8H2.8L7.84 13.44L5.88 16.8C5.74 17.22 5.6 17.78 5.6 18.2C5.6 19.74 6.86 21 8.4 21H25.2V18.2H8.96C8.82 18.2 8.68 18.06 8.68 17.92V17.7799L9.94 15.3999H20.3C21.42 15.3999 22.26 14.8399 22.68 13.9999L27.72 4.9C28 4.62 28 4.48 28 4.2C28 3.36 27.44 2.8 26.6 2.8H5.88L4.62 0H0ZM22.4 22.4C20.86 22.4 19.6 23.66 19.6 25.2C19.6 26.74 20.86 28 22.4 28C23.94 28 25.2 26.74 25.2 25.2C25.2 23.66 23.94 22.4 22.4 22.4Z"/>
                            </svg>
                        </div>
                        {{-- Конец --}}
                    </a>
                    <a style="color:black;display: flex;justify-content: start;flex-direction: column;align-items: center;" @if(session()->get('user') != null) href="{{route('user')}}" @else class="ClickAcc" @endif
                    >
                        @if(session()->has('user'))
                            <img src="{{asset('/storage/'.session()->get('user')->avatar)}}" style="width: 35px;height: 35px;border-radius: 50%;" alt=""/>


                        @else
                            {{-- Иконка SVG код для заливки --}}
                            <div class="header__icon">
                                <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M28 27.9999V24.4102C28 22.5061 27.2625 20.68 25.9497 19.3335C24.637 17.9871 22.8565 17.2307 21 17.2307H7C5.14348 17.2307 3.36301 17.9871 2.05025 19.3335C0.737498 20.68 0 22.5061 0 24.4102V27.9999"/>
                                    <path d="M14.0001 12.9231C17.5687 12.9231 20.4617 10.0301 20.4617 6.46154C20.4617 2.89293 17.5687 0 14.0001 0C10.4315 0 7.53857 2.89293 7.53857 6.46154C7.53857 10.0301 10.4315 12.9231 14.0001 12.9231Z"/>
                                    </svg>
                                </div>
                                {{-- Конец --}}

                        @endif
                            {{session()->has('user') ? strtoupper(substr(session()->get('user')->name,0,2)) : ''}}
                    </a>
                </div>
            </div>
            <div class="header__catalog1-menu">
                <div class="header__catalog1-inner">
                    <div class="header__catalog1-close">&times;</div>
                    @foreach(\App\Models\ShopCategory::with('children.children')->where('parent_id',null)->get() as $category)

                    <a class="header__catalog1-link " href="{{route('shop',$category->id)}}" data-category="{{$category->id}}">
                        <img src="{{asset('storage/'.$category->icon)}}"  alt="" />{{$category->name}}</a
                    >
                    @endforeach

                </div>
                <div class="header__catalog1-next" >
                    <a class="header__catalog1-next-link"> Корейская косметика </a>

                    <a class="header__catalog1-next-link">Натуральная косметика</a>
                    <a class="header__catalog1-next-link">Дермокосметика</a>
                    <a class="header__catalog1-next-link">Уход и гигиена</a>
                    <a class="header__catalog1-next-link">Парфюмерия</a>
                    <a class="header__catalog1-next-link">Краска для волос</a>
                    <a class="header__catalog1-next-link">Подарочные наборы</a>
                    <a class="header__catalog1-next-link">Аксессуары и инструменты</a>
                    <a class="header__catalog1-next-link">Аксессуары и инструменты</a>
                    <a class="header__catalog1-next-link">Аксессуары и инструменты</a>
                    <a class="header__catalog1-next-link">Аксессуары и инструменты</a>
                    <a class="header__catalog1-next-link">Аксессуары и инструменты</a>
                </div>
                <div class="header__catalog1-next2">
                    <a href="" class="header__catalog1-next2-link">
                        <img src="img/catalog-menu/icon1.png" alt="" /> Продукты
                        питания</a
                    >
                    <a href="" class="header__catalog1-next2-link">
                        <img src="img/catalog-menu/icon2.png" alt="" /> Детский мир</a
                    >
                </div>
            </div>
        </div>
    </div>
</header>
<section class="popup">
    <div class="popup__inner">
        <div class="popup__title">Войдите или зарегистрируйтесь</div>
        <button data-modal="login" class="popup__btn1">Войти</button>
        <button data-modal="registration" class="popup__btn2">
            Регистрация
        </button>
    </div>
</section>
@yield('content')
<!-- ----------------- Модалка Регистрация-------------- -->
<div class="overlay3">
    <div class="modalRegistration" id="registration">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Регистрация</div>

        <form action="{{route('register')}}" class="modal__form" method="post">
            {{csrf_field()}}

            <div class="modal__wrap2">
                <div class="modal__wrap2-title">E-mail</div>
                <input type="email" name="email" class="modal__input" required>
            </div>
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">Телефон</div>
                <input type="number" name="phone" class="modal__input" required>
            </div>
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">Пароль</div>
                <input type="password" minlength="6" name="password" class="modal__input" required>
            </div>
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">Повторите Пароль</div>
                <input type="password" name="password_confirm" class="modal__input" required>
            </div>
            <div class="modal__btns">
                <button type="submit" class="popup__btn1">Регистрация</button>
                <button class="popup__btn2">Войти</button>
            </div>
        </form>
    </div>
</div>
<div class="overlay2">
    <div class="modalLogin" id="login">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Вход</div>
        <form action="{{route('login')}}" class="modal__form" method="post">
            {{csrf_field()}}
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">E-mail</div>
                <input type="email" name="email" class="modal__input">
            </div>
            <div class="modal__wrap2">
                <div class="modal__wrap2-title">Пароль</div>
                <input type="password" name="password" class="modal__input">
            </div>
            <div class="modal__btns">
                <button class="popup__btn1">Войти</button>
                <button class="popup__btn2">Регистрация</button>
            </div>
        </form>
    </div>
</div>

<!-- ---------------- Модалка Вход -------------------- -->
<footer class="footer">
    <div class="container-fluid">
        <div class="footer__inner">
            <a id="upbutton" class="footer__up" href="#" onclick="smoothJumpUp(); return false;">
                    <img src="{{asset('/img/main-page/arrow-up.svg')}}" alt="" />
                </a>

                <script>
                    var smoothJumpUp = function() {
                        if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
                            window.scrollBy(0,-50);
                            setTimeout(smoothJumpUp, 10);
                        }
                    }

                    window.onscroll = function() {
                      var scrolled = window.pageYOffset || document.documentElement.scrollTop;
                      if (scrolled > 100) {
                          document.getElementById('upbutton').style.display = 'block';
                      } else {
                          document.getElementById('upbutton').style.display = 'none';
                      }
                    }
                </script>
            </a>
            <div class="footer__links offset-md-2 col-md-8">
                <a href="{{route('delivery')}}" class="footer__link">Доставка и оплата</a>
                <a href="{{route('conditions')}}" class="footer__link">Условия возврата</a>
                <a href="{{route('about')}}" class="footer__link">О компании</a>
                <a href="{{route('contacts')}}" class="footer__link">Контакты</a>
            </div>
            <div class="footer__wrap">
                <a href="" class="footer__social"
                ><img src="{{asset('/img/main-page/vk.svg')}}" alt=""
                    /></a>
                <a href="" class="footer__social">
                    <img src="{{asset('/img/main-page/instagram.svg')}}" alt="" /> </a
                ><a href="" class="footer__social"
                ><img src="{{asset('/img/main-page/facebook.svg')}}" alt=""
                    /></a>
            </div>
            <div class="footer__title">© 2007-{{ date('Y') }} ТОО «Mava» (Казахстан)</div>
        </div>
    </div>
    @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'orderMain')
    <div class="overlay4">
        <div class="modalReturn" id="return">
            <div class="modal__close">&times;</div>
            <div class="modal__title">Заявка на возврат товара</div>
            <form id="submit_return" class="modal__form">
                @if(session()->get('user') != null)
                <input type="hidden" name="user_id" value="{{session()->get('user')->id}}">
                @endif
                @if(isset($order))
                        <input type="hidden" name="order_id" value="{{$order->id}}">
                    @endif
                <div class="modal__wrap2">
                    <div class="modal__wrap2-title">Выберите товар </div>
                    <select name="product_id" class="modal__input" id="">
                        @if(isset($order))
                            @foreach($order->details as $detail)
                                <option value="{{$detail->product->id}}">{{$detail->product->name}}</option>

                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="modal__wrap2">
                    <div class="modal__wrap2-title" id="name_form_label">Имя</div>
                    <input type="text" name="name" class="modal__input" />
                </div>
                <div class="modal__wrap2">
                    <div class="modal__wrap2-title" id="phone_form_label">Телефон</div>
                    <input type="tel" name="phone" class="modal__input" />
                </div>

                <div class="modal__wrap" style="margin-top: 24px">
                    <div class="modal__form-title" id="return_reason">Причина возврата</div>
                    <textarea name="reason" id="textareaModal" ></textarea>
                </div>
                <div class="modal__btns">
                    <button class="popup__btn1" id="return_submit_button"  >Отправить заявку</button>
                </div>
            </form>
        </div>
    </div>
        @endif


</footer>
<script>
    // $('#product_add_basket').on('click', function(){
    //     $('#product_add_basket').addClass('test')
    // })
    async function mainSearchFunction(){
        let search = document.getElementById('searchName').value
        const response = await fetch(`/search/${search}`,)
        let dropdown_search = document.getElementById('dropdown_main_search')
        if (search == ''){
            document.getElementById('dropdown_main_search').setAttribute('style','display:none;')
            return true;

        }
        dropdown_search.innerHTML = ''

        if (response.status===200){
            let data = await response.json()

            document.getElementById('dropdown_main_search').setAttribute('style','display:block;')

            if(data.names.length){
                data.names.forEach(name=>{
                    dropdown_search.innerHTML += `<li><a href='/shop?search=${name}'>${name}</a></li>`
                })
            }else{
                document.getElementById('dropdown_main_search').setAttribute('style','display:none;')

            }
        }
    }


</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<style>
    .product_add_basket:hover{
        cursor: pointer;
    }
    .product_add_favourite:hover{
        cursor: pointer;
    }
    .account__favorite .catalog__favorite-item{
        padding-right: 0px;
    }
    .account__favorite .catalog__favorite-item-wrap2{
        padding-right: 15px;
    }
    .catalog__favorite-item-favorite{
        padding-right: 15px;
    }
    .catalog__item{
        padding-right: 0px;
    }
    .catalog__item-favorite{
        padding-right: 15px;
    }
    .catalog__item-buy{
        padding-right: 15px;
    }
    .submit_search_button:before{
        content: '';
    }
    .header__search form{
        display: flex !important;
    }
    .submit_search_button{
        border:none !important;
        position: absolute;
        right: 50px; !important;
        background: none !important;
        color: #31AC80;
        width: 150px !important;

    }
    .header__catalog1-inner{
        border: 1px solid #31AC80;
        z-index: 100;
    }
    .catalog__item-subtitle {
        position: absolute;
        top: 10px;
    }
    .header__catalog1-next{
        border: 1px solid #31AC80;
        z-index: 100;
        height: 100%;
        background-color: #ae9f9f11;

    }
    .dropdown_main_search{
        position: absolute;
        z-index: 100;
        width: 90%;

        margin-top: 10px;
        display: none;
        /* padding-top: 10px; */
        background-color: #fff;
        border-radius: 20px;
        opacity: 1;
        border: 1px solid #31AC80;
        outline: none;

    }
    .dropdown_main_search li{
        list-style:none;


    }
    .dropdown_main_search li a{
        display: block;
        color: #40695B;
        border-radius: 20px;
        font-size: 14px;
        border-bottom: 1px solid #8d9793;
        line-height: 25px;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        padding: 8px 40px 6px 15px;
        -webkit-transition: all 0.2s;
        transition: all 0.2s;
    }
    @media screen and (max-width: 767px) {
        .header__search input.submit_search_button{
            width: 150px !important;
            right: 0px !important;
        }
        .dropdown_main_search{
            width: 100% !important;
            border-radius: 0px !important;
        }
    }

    @media screen and (min-width: 767px) {
        .header__catalog1-inner {
            min-height: 500px;
        }
    }
</style>

{{-- Стили by Volodya --}}

<style>
    .button:hover {
        background: #1e805c;
    }
    .button {
        transition: 0.5s all;
    }
    .popup__btn1 {
        transition: 0.5s all;
    }
    .popup__btn1:hover {
        background: #1e805c;
    }
    .footer__link {
        transition: 0.5s all;
    }
    .footer__link:hover {
        color: #383838;
    }
    .footer__social {
        width: 44px;
        height: 44px;
        background: #31AC80;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: 0.5s all;
    }
    .footer__social:hover {
        background: #1e805c;
    }
    .footer__up {
        width: 40px;
        height: 42px;
        background: #31AC80;
        transition: 0.5s all;
        z-index: 100;
    }
    .footer__up:hover {
        background: #1e805c;
    }
    .header__icon svg {
        fill: #383838;
        transition: 0.5s all;
    }
    .header__icon svg:hover {
        fill: #31AC80;
    }
    .popup__btn2 {
        transition: 0.3s all;
    }
    .popup__btn2:hover {
        background: #31AC80;
        color: #fff;
    }
    .catalog__item-buy {
        width: 38px;
        height: 40px;
        background: #31AC80;
        border-radius: 6px;
    }
    .catalog__item-buy:hover {
        background: #1e805c;
    }
    .modalLogin {
        overflow: hidden;
    }
    .product_add_basket_component {
        cursor: pointer;
    }
    .modalRegistration {
        overflow: hidden;
    }
    .catalog__item-buy {
        padding-left: 12px;
        margin-right: 16px;
        justify-content: center;
        align-items: center;
        display: flex;
        position: absolute;
        bottom: 16px;
        right: 0;
    }
    .catalog__item-title {
        margin-bottom: 24px;
    }
    .catalog__item {
        position: relative;
        margin: 15px 7.5px;
    }
    /* .catalog__item-cost {
        position: absolute;
        left: 43px;
    } */
    .catalog__item-costs {
        position: absolute;
        bottom: 58px;
    }
    .zoo-item .zoo-img {
        background-size: inherit;
    }
    .product__about-images__choose {
        z-index: 100;
    }
    .account__favorite .catalog__favorite-item-cost {
        margin-left: 0px;
        /* margin-bottom: 46px; */
        margin-top: 10px;
        z-index: 1;
    }
    .main__item img {
        height: inherit;
    }
    .form-control {
        font-size: 11px;
        padding-top: 12px;
    }
    .header__nav-location {
        margin-left: 165px;
    }
    .account__favorite .catalog__favorite-item-NoProduct {
        position: absolute;
        top: 10px;
        color: #31AC80;
    }
    .dropdown_main_search li a:hover {
        background: #31AC80;
        color: #fff;
    }
    .account__favorite .catalog__favorite-item:hover {
        box-shadow: 0px 2px 12px rgba(113,113,113,0.75);
        transform: scale(1.1);
        z-index: 1
    }
    .account__favorite .catalog__favorite-item {
        transition: 0.5s all;
    }
    .header__catalog1-next, .header__catalog1-next2 {
        border: 1px solid #31AC80;
        background-color: #ae9f9f11;
    }
    .catalog__item {
        display: flex;
        flex-direction: column;
        transition: 0.5s all;
    }
    @media(max-width: 577px) {
        .catalog__item {
            flex: 0 1 45%;
        }
        .account__favorite .catalog__favorite-item {
            flex: 0 1 45%;
            margin: 0 14px;
        }
    }
    .catalog__item2-img {
        min-height: 120px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        max-height: 120px;
    }
    .catalog__item2-img img {
        max-height: 120px;
    }
    .main .main__blocks__buttons {
        bottom: inherit;
        background-color: inherit;
        left: 55%;
        transform: translateX(-50%);
        max-width: 800px;
    }
    .main .main__blocks__buttons li a {
        display: inline-block;
    transition: 0.3s;
    background: white;
    /* margin: 10px; */
    padding: 10px 15px;
    cursor: pointer;
    color: black;
    /* border: 1px solid #31AC80; */
    }
    .main .main__blocks__buttons li.active a {
        color: #111;
        background: #F5F5F5;
        border-radius: 5px;
    }
    .choose_image {
        cursor: pointer;
    }
    .main .main__blocks__buttons li a {
        color: inherit;
    }
    .main .main__blocks__buttons li a:hover {
        color: white;
    }
    @media (max-width: 577px) {
    .main__blocks__buttons[data-v-0c212d62] {
        text-align: center;
        padding: initial;
    }
    .main .main__blocks__buttons {
        position: inherit;
        left: inherit;
        transform: inherit;
    }
}
    .main .main__blocks__buttons li:hover a {
        color: #111;
        background: #F5F5F5;
        border-radius: 5px;
    }
    .catalog__item-title {
        margin-right: 16px;
    }
    .dropdown_main_search {
        overflow: scroll;
    max-height: 200px;
    overflow-x: hidden;
    }
    .header__catalog1-next, .header__catalog1-next2 {
        overflow: scroll;
        overflow-x: hidden
    }
    .catalog__item:hover {
        box-shadow: 0px 2px 12px rgba(113,113,113,0.75);
        transform: scale(1.1);
        z-index: 1;
}
    .catalog__item-img img {
        height: 25vh;
    }
    .catalog__item-img {
        padding-top: 0;
    }
    .catalog__item-wrap {
        margin-bottom: 0;
    }
    .catalog__item-title {
        text-align: center;
        margin-bottom: 17px;
    }
    .main__blocks__divide {
        padding-left: 0!important;
        padding-right: 0!important;
    }
    .main__blocks {
        padding-left: 0!important;
        padding-right: 0!important;
    }
    /* catalog__favorite-item-title {
        padding: 0 5px;
    } */
    /* .form-control {
        margin-bottom: 11px;
    } */

    /* #product_add_basket:focus {
        color: #fff;
    } */
    /* ::-webkit-scrollbar{width:0px;background:transparent}html{-ms-overflow-style:none;scrollbar-width:none}
    НЕ ЗАБУДЬ УДАЛИТЬ ЭТУ СТРОЧКУ В SASS ЧТОБЫ ВЕРНУТЬ СКРОЛЛ (Я УБРАЛ ТОЛЬКО В style.min.css)
    */
</style>
<script>
    $('#product_add_basket').on('click', function(){
        $(this).css('color', '#fff')
        $(this).css('background', '#31AC80')
        $(this).text('Добавлено в корзину')
    });
</script>

<script src="{{asset('js/libs.min.js')}}"></script>
<script src="{{asset('js/product.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
{{-- <script src="{{asset('js/Magnifier.js')}}"></script>
<script src="{{asset('js/Event.js')}}"></script>
<script>
    var evt = new Event(),
    m = new Magnifier(evt);
</script> --}}
<script src="{{asset('js/jquery.ez-plus.js')}}"></script>
</body>
</html>
