@extends('layout.app')
@section('content')
<div class="container-fluid">
    <form id="orderForm" class="" method="post">
        {{csrf_field()}}
    <div class="ordering">
        <div class="ordering__title">Оформление заказа</div>
        <div class="ordering__wrap">
            <div class="ordering__wrap-title">Контактные данные</div>

                <div class="ordering__wrap-box">
                    <div cl>Телефон</div>
                    <input type="text" name="phone" value="{{session()->has('user') ? session()->get('user')->phone : ''}}" class="ordering__input">
                </div>
                <div class="ordering__wrap-box">
                    <div cl>Имя</div>
                    <input type="text" value="{{session()->has('user') ? session()->get('user')->name : ''}}" name="name" class="ordering__input">
                </div>
                <div class="ordering__wrap-box">
                    <div>E-mail <span>необязательно</span></div>
                    <input type="email" value="{{session()->has('user') ? session()->get('user')->email : ''}}" name="email" class="ordering__input">
                </div>
                @if(session()->has('user'))
                    @php $user = session()->get('user'); @endphp
                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                @endif
        </div>
        <div class="ordering__delivery">
            <div class="ordering__delivery-descr">
                ВНИМАНИЕ:  бесплатная доставка осуществляется от 5 000 тенге. До 5000 тенге, сумма составляет 500 тенге.
            </div>
            <div class="ordering__delivery-inner">
                <div class="ordering__delivery-wrap">
                    <div class="ordering__delivery-title">Доставка курьером</div>

                        <div class="ordering__delivery-pb">
                            <div>Адрес</div>
                            <input type="text" name="address" class="ordering__input">
                        </div>
                        <div class="ordering__delivery-pb">
                            <div>Страна, Город</div>
                            <input type="text" name="country_city" class="ordering__input">
                        </div>
                        <div class="ordering__delivery-box ordering__delivery-pb">
                            <div>
                                <div class="ordering__delivery-rmb">Подъезд <span>* необязательно</span></div>
                                <input type="text" name="flat" class="ordering__input">
                            </div>
                            <div>
                                <div class="ordering__delivery-rmb">Индекс <span>* необязательно</span></div>
                                <input type="text" name="index" class="ordering__input">
                            </div>
                        </div>

                </div>
                <div class="ordering__delivery-maps">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2906.4041239886733!2d76.88994911546268!3d43.24295147913751!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836914550a2389%3A0x8371034b772eb513!2z0YPQu9C40YbQsCDQqNC10LLRh9C10L3QutC-IDE2NdCxLzcyLCDQkNC70LzQsNGC0YsgMDUwMDAw!5e0!3m2!1sru!2skz!4v1603097456691!5m2!1sru!2skz" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
        <div class="ordering__payment">
            <div class="ordering__payment-inner">
                <div class="ordering__payment-wrap">
                    <div class="ordering__payment-title">Способ оплаты</div>
                    <div class="ordering__payment-checkbox">
                        <input type="checkbox">
                        Наличные
                    </div>

                </div>
                <div class="ordering__payment-wrap2">
                    <div class="ordering__payment-title">Итого</div>
                    <div class="ordering__payment-wrap3">
                        <div class="ordering__payment-link">Сумма товара: <span>{{$sum}} тг.</span></div>
                        <div class="ordering__payment-link">Доставка: <span>{{$sum >= 5000 ? 0 : 500}} тг.</span></div>
                        <div class="ordering__payment-link">Итого к оплате: <span>{{$sum >= 5000 ? $sum : $sum+500}}тг.</span></div>
                    </div>
                </div>
            </div>
            <button id="mainSubmit" class="button ordering__payment-btn">Подтвердить заказ</button>

        </div>

    </div>
    </form>
    <section class="catalog">
        <div class="catalog__inner">
            <div class="catalog__blocks catalog__blocks-product">
                <div class="catalog__wrap">
                    <div class="catalog__title">Рекомендованные товары</div>
                    <div class="catalog__showAll" onclick="window.location.href='{{route('shop')}}'">показать все</div>
                </div>
                <div class="catalog__items">
                    @foreach(\App\Models\RelatedProduct::with('product.product_flags.flag')->orderBy('order','asc')->paginate(5) as $related_product )
                        <div class="catalog__item">
                            <div class="catalog__item-wrap">
                                @if(isset($product->products->product_flags[0]))

                                    <div class="catalog__item-discound" style="background-color: {{$related_product->product->product_flags[0]->flag->color}}')">
                                        {{$related_product->product->product_flags[0]->flag->name}}
                                    </div>
                                @endif
                                <div class="catalog__item-favorite product_add_favourite" data-product-id="{{$related_product->product->id}}">
                                    <img src="/img/catalog/favorite-item.svg" alt="" />
                                </div>
                            </div>
                            <div class="catalog__item-img">
                                <a href="{{route('product',$related_product->product->id)}}">

                                    @if(count($related_product->product->variations) > 0 )
                                        <img src="{{asset('storage/'.$related_product->product->variations[0]->image)}}" alt="" width="100%" />
                                    @else
                                        <img src="{{asset('storage/'.$related_product->product->image)}}" alt="" width="100%" />
                                    @endif
                                </a>
                            </div>
                            <div class="catalog__item-title">
                                <a href="{{route('product',$related_product->product->id)}}">

                                    {{$related_product->product->name}}
                                </a>
                            </div>
                            <div class="catalog__item-subtitle catalog__item-NoProduct">
                                @if($related_product->product->stock != 1)
                                    Нету в складе
                                @else
                                    Есть в наличии

                                @endif
                            </div>
                            <div class="catalog__item-wrap2">
                                @if(count($related_product->product->variations)>0)

                                    <div  id="" class="form-control " style="border:none;font-weight:bold;font-size:12px;padding:0px;border-bottom:1px solid #31AC80;width: 35%;border-radius: 0px;">
                                        {{$related_product->product->variations[0]->name}}
                                    </div>
                                    <div class="catalog__item-cost"><span>{{number_format($related_product->product->variations[0]->price, 0, '.', ' ')}}</span> тг.</div>
                                @else
                                    <div class="catalog__item-cost"><span>{{number_format($related_product->product->price,0,'.',' ')}}</span> тг.</div>
                                @endif
                                <div class="catalog__item-buy product_add_basket"  data-product-id="{{$related_product->product->id}}" @if(count($related_product->product->variations)>0) data-variation-id="{{$related_product->product->variations[0]->id}}" @endif>
                                    <img src="/img/catalog/buy.png" alt="basket" />
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>

</div>
@endsection
