@extends('layout.app')
@section('content')
<div class="circles">
    <div class="container-fluid">
        <div class="circles__wrap">
            <div class="circles__item">
                <div class="circles__icon">
                    <img src="{{asset('img/gps.svg')}}" alt="">
                </div>
                <div class="circles__text">
                    Адрес и способ доставки
                </div>
            </div>
            <div class="circles__item">
                <div class="circles__icon">
                    <img src="{{asset('img/card.svg')}}" alt="">
                </div>
                <div class="circles__text">
                    Способ оплаты
                </div>
            </div>
            <div class="circles__item">
                <div class="circles__icon">
                    <img src="{{asset('img/backpack.svg')}}" alt="">
                </div>
                <div class="circles__text">
                    Проверка и подтверждение
                </div>
            </div>
            <div class="circles__item">
                <div class="circles__icon">
                    <img src="{{asset('img/star.svg')}}" alt="">
                </div>
                <div class="circles__text">
                    Готово
                </div>
            </div>
        </div>
        <div class="payment">

            <div class="container-fluid">
            <div class="payment__number">
                Оформлен заказ
                <span>
                    {{$order->order_number != null ? $order->order_number : '#'.$order->created_at->day.$order->created_at->month.'-'.$order->created_at->year.'-'.$order->id}}

                </span>
            </div>
            <div class="payment__descr">Ваш заказ в ближайшее время будет проверен менеджером.</div>
            <div class="payment__end">{{$order->name}}, благодарим Вас за выбор интернет-магазина Mava.kz</div>
        </div>
        </div>
    </div>
</div>

<style>
    .container-fluid {
        overflow: hidden;
    }
    .payment {
        margin-top: 67px;
    }
    .payment__number {
        font-family: Roboto;
        font-style: normal;
        font-weight: 500;
        font-size: 23px;
        color: #31AC80;
    }
    .payment__number span {
        font-weight: 600;
    }
    .payment__end {
        padding-bottom: 15rem;
        font-weight: bold;
        margin-top: 15px;
    }
    .circles__wrap {
        display: flex;
        max-width: 1000px;
        margin: 0 auto;
        justify-content: space-between;
        margin-top: 99px
    }
    .circles__item {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        position: relative;
    }
    .circles__item img {
        cursor: initial;
    }
    .circles__text {
        font-family: Roboto;
        font-style: normal;
        font-weight: 300;
        font-size: 13px;
        align-items: center;
        color: #000000;
        margin-top: 12px;
        text-align: center;
    }
    .circles__icon {
        width: 40px;
        height: 40px;
        background: #31AC80;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
    }
    .circles__item::after {
        content: "";
        width: 300px;
        height: 1px;
        left: 61px;
        position: absolute;
        left: 61px;
        background: #31AC80;
        bottom: 52px;
    }
    .circles__item:last-child::after {
        display: none;
    }
</style>
@endsection
