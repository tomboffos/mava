@extends('layout.app')
@section('content')


    <div id="catalog">

        <catalog_component :brand="`{{ $brand != null  ? $brand : '' }}`"
                           :favourite_cart="`{{json_encode(session()->get('favourite'))}}`"
                           :category_id="`{{$category != null ? $category->id : null}}`"
                           :search="`{{ $search }}`"
                           :category_name="`{{$category != null ? $category->name: null}}`"></catalog_component>
    </div>
    <script src="{{asset('js/catalog.js')}}"></script>
@endsection
