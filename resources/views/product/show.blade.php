@extends('layout.app')
@section('content')
<div class="container-fluid">

    <section class="product">
        <div class="product__header">
            <div class="product__header-inner">
                <div>
                    <a href="/shop/{{$product->category->id}}" class="product__header-link">{{$product->category->name}}</a>
                </div>
                <div class="product__header-btns">
                    @foreach($product->product_flags as $flag)
                        <div class="product__header-btn product__header-new" style="color: {{$flag->flag->color}};border-color: {{$flag->flag->color}};">{{$flag->flag->name}}</div>
                    @endforeach
                </div>
                <div class="product__header-content">
                    <div class="product__header-title">
                        {{$product->name}}
                    </div>
                    <div class="row">
                        <div class="product__header-wrap col-xl-4 col-lg-6 col-md-7">
                            @if($product->stock == 1)
                            <div class="product__header-productStocks">Товар в налиии</div>
                            @else
                                <div class="product__header-productStocks">Нет в налиии</div>
                            @endif
                            <div class="product__header-review">
                                <div class="product__header-reviewStar">
                                    @if($product['review_point'] == 0 && $product['review_count'] == 0)
                                        @else
                                            @for($i = 1;$i<=5;$i++)
                                                @if($i<=$product['review_point']/$product['review_count'])
                                            <img src="{{asset('img/product-page/star.png')}}" alt="">
                                            @else
                                                <img src="{{asset('img/product-page/start2.png')}}" alt="">
                                            @endif
                                          @endfor
                                        @endif

                                </div>
                                <div class="product__header-reviewTxt"><span>{{count($reviews)}}</span> отзывов</div>
                            </div>
                        </div>
                        <div class="product__header-productCode offset-xl-6 col-xl-2 offset-lg-4 col-lg-2 offset-md-3 col-md-2">код: <span>{{$product->id}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ---------------------- Товар ------------------------ -->

        <div class="product__about">
            <div class="product__about-images">
                <div class="product__counter">
                    <div class="buttons">
                        <button class="minusButton" id="minusButton"><img src="{{asset('img/product-page/minus.svg')}}" alt=""></button>
                        <span class="amount" id="amount">1</span>
                        <button class="plusButton" id="plusButton"><img src="{{asset('img/product-page/plus.svg')}}" alt=""></button>
                    </div>
                </div>
                <div class="product__about-images__inner">
                    <div class="product__about-images__choose">

                        @if($product['slider_images'] != null && $product['slider_images'] != [])
                        @foreach($product['slider_images'] as $image)
                            <img src="{{asset('storage/'.$image)}}" alt="" class="choose_image">
                        @endforeach
                        @endif

                    </div>
                    <div class="product__about-images__chooseClick">
                        @if(count($product->variations)>0)
                            <img id="image_main" src="{{asset('storage/'.$product->variations[0]->image)}}" alt="">

                        @else
                        <img id="image_main" src="{{asset('storage/'.$product['image'])}}" alt="">
                            @endif
                    </div>
                </div>
            </div>
            <!-- -------------- Телефон -------------- -->
            @if(count($product->variations) > 0)
                <input type="hidden" id="price" value="{{$product->variations[0]->price}}">
            @else
                <input type="hidden" id="price" value="{{$product->price}}">
            @endif
            @if($product->display_product_type_id == 2)
          <div class="product__about-descr">
              @if(count($product->variations) >0)

                  <div class="product__about-descr__title">Выберите вариацию</div>
                  <div class="product__about-descr__outline">
                      <div class="product__about-descr__select">
                          <select name="" id="product_variation" onchange="variationChange()">

                              @foreach($product->variations as $variation)
                                  <option value="{{$variation->id}}" id="variation-{{$variation->id}}" data-price="{{$variation->price}}" data-image="{{asset('storage/'.$variation->image)}}">{{$variation->name}}</option>

                              @endforeach

                          </select>
                      </div>
                  </div>
              @endif
                <div class="product__about-descr__title">Характеристики</div>
                <div class="product__about-descr__outline">
                    @foreach($attributes as $attribute)
                        @if($loop->index <=5)
                            @if(isset($attribute[1]) && $attribute[1] != 'undefined')
                    <div class="product__about-descr__wrap">
                        <p>{{$attribute[0]}}</p>
                        <div>{{isset($attribute[1]) ?$attribute[1] != 'undefined' ?  $attribute[1] : '' : ''}}</div>
                    </div>
                                @endif
                        @endif
                    @endforeach

{{--                    @if($product['discount_price'] != null || $product['discount_price'] != 0)--}}

{{--                        <div class="product__about-descr__cost">--}}
{{--                            Цена: {{number_format($product->discount_price,  0, '.', ' ')}} тг. <span>{{number_format($product->price, 0, '.', ' ')}} тг.</span>--}}
{{--                        </div>--}}
{{--                    @else--}}
                        @if(count($product->variations)>0)
                            <div class="product__about-descr__cost" id="price_technical">
                                Цена: {{$product->variations[0]->price}} тг.
                            </div>
                            @else
                        <div class="product__about-descr__cost" id="price_technical">
                            Цена: {{$product->price}} тг.
                        </div>
                        @endif
{{--                    @endif--}}
                    <div class="product__about-descr__btns">
                        <button class="button product__about-descr__btn1 product_add_basket basket_button" id="product_buy" data-product-id="{{$product->id}}" @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif data-quantity="1">Купить сейчас</button>
                        <button class="product__about-descr__btn2"><img src="/img/product-page/favorite.png" alt="" class="product_add_favourite" data-product-id="{{$product->id}}"> </button>
                        <button class="product__about-descr__btn3 product_add_basket" id="product_add_basket" data-product-id="{{$product->id}}" data-quantity="1" @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif>Добавить в корзину</button>
                    </div>
                </div>
            </div>
            @elseif($product->display_product_type_id == 1)
            <!-- -------------- Кроссовки ------------ -->
            <div class="product__about-descr">
                @if(count($product->variations) >0)

                <div class="product__about-descr__title">Выберите вариацию</div>
                <div class="product__about-descr__outline">
                        <div class="product__about-descr__select">
                            <select name="" id="product_variation" onchange="variationChange()">

                                @foreach($product->variations as $variation)
                                    <option value="{{$variation->id}}" data-image="{{asset('storage/'.$variation->image)}}" id="variation-{{$variation->id}}" data-price="{{$variation->price}}">{{$variation->name}}</option>
                                @endforeach

                            </select>
                        </div>
                </div>
                @endif

              <div class="product__about-descr__title">Выберите размер</div>
              <div class="product__about-descr__outline">

                  <div class="product__about-descr__select">
                    <select name="" id="">

                    @foreach($product->color_size as $sizes)
                            <option value="">{{$sizes->size->size}} </option>
                        @endforeach

                    </select>
                  </div>
                  <div class="product__about-descr__colors">
                    <div class="product__about-descr__color-title">Цвет: <span>красный</span></div>
                    <div class="product__about-descr__colorCol">
                        @foreach($product->color_size as $color)
                          <div class="product__about-descr__color" onclick="changeSize()" style="background: url('{{asset('storage/'.str_replace('\\','/',$color->texture_image))}}');background-size: cover;"></div>
                        @endforeach

                    </div>
                  </div>
                  @if(count($product->variations)>0)
                      <div class="product__about-descr__costNew" id="price_size">Цена: <span>{{number_format($product->variations[0]->price, 0, '.', ' ')}} тг</span>.</div>
                    @else
                  <div class="product__about-descr__costNew" id="price_size">Цена: <span>{{number_format($product->price, 0, '.', ' ')}} тг</span>.</div>
                  @endif
                      <div class="product__about-descr__btns">
                      <button class="button product__about-descr__btn1 product_add_basket basket_button" id="product_buy" @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif data-product-id="{{$product->id}}" data-quantity="1">Купить сейчас</button>
                      <button class="product__about-descr__btn2 product_add_favourite" data-product-id="{{$product->id}}"><img src="{{asset('img/product-page/favorite.png')}}" alt=""></button>
                      <button class="product__about-descr__btn3 product_add_basket"
                              id="product_add_basket" @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif data-product-id="{{$product->id}}">Добавить в корзину</button>
                  </div>
              </div>
            </div>

            @elseif($product->display_product_type_id == 3)
            <!-- -------------- Книги ---------------- -->
            <div class="product__about-descr">
                @if(count($product->variations) >0)

                    <div class="product__about-descr__title">Выберите вариацию</div>
                    <div class="product__about-descr__outline" >
                        <div class="product__about-descr__select">
                            <select name="" id="product_variation" onchange="variationChange()">

                                @foreach($product->variations as $variation)
                                    <option value="{{$variation->id}}" data-image="{{asset('storage/'.$variation->image)}}" id="variation-{{$variation->id}}" data-price="{{$variation->price}}">{{$variation->name}}<!--  --></option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                @endif
                <div class="product__about-descr__title">Описание</div>
                <div class="product__about-descr__outline">
                    <div class="product__about-descr__txt">
                        {!! $product->description!!}
                    </div>
                    @if(count($product->variations)>0)
                        <div class="product__about-descr__costNew" id="price_description">
                            Цена: <span> {{number_format($product->variations[0]->price,0,'.',' ')}} </span> тг.
                        </div>
                    @else
                    <div class="product__about-descr__costNew" id="price_description">Цена: <span>{{number_format($product->price, 0, '.', ' ')}} тг</span>.</div>
                    @endif
                    <div class="product__about-descr__btns">
                        <button class="button product__about-descr__btn1 product_add_basket basket_button" @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif id="product_buy" data-product-id="{{$product->id}}" data-quantity="1">Купить сейчас</button>
                        <button class="product__about-descr__btn2"><img src="img/product-page/favorite.png" alt=""></button>
                        <button class="product__about-descr__btn3 product_add_basket" @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif id="product_add_basket" data-product-id="{{$product->id}}" data-quantity="1">Добавить в корзину</button>
                    </div>
                </div>
            </div>
            @endif

        </div>

        <div class="product__description">
            <div class="product__description-items product__tabs">
                <div class="1 product__description-item product__description-item_active">
                    Описание
                </div>

                <div class="2 product__tab product__description-item">
                    Отзывы <span>{{count($reviews)}}</span>
                </div>
            </div>
            <div class="product__description-content">
                <div class="product__description-outline">
                    <table class="table table-bordered">
                        @foreach($attributes as $attribute)
                            @if($attribute[0] != '' && isset($attribute[1]))
                                @if(isset($attribute[1]) && $attribute[1] != 'undefined')
                                    <tr>
                                        <td>
                                            {{$attribute[0]}}
                                        </td>
                                        <td>
                                            {{$attribute[1]}}
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </table>




                    {!!  $product->description!!}
                </div>

                 <div class="product__description-reviews">
                    <div class="row">
                        <div class="product__description-reviews-descr col-lg-8 col-md-7">
                            @foreach($product->reviews as $review)
                            <div class="product__description-reviews-item">
                                <div class="product__description-reviews-acc">
                                    <img src="img/product-page/acc.png" alt="">
                                    <div>
                                        <div class="product__description-reviews-name">{{$review->user->email}}</div>
                                        <div class="product__description-reviews-date">{{$review->created_at->day.'.'.$review->created_at->month.'.'.$review->created_at->year}}</div>
                                    </div>
                                </div>
                                <div class="product__description-reviews-star">
                                    @for($i = 1; $i<=5;$i++)
                                        @if($i<=$review->review)
                                    <img src="{{asset('img/product-page/star.png')}}" alt="">
                                        @else
                                    <img src="{{asset('img/product-page/start2.png')}}" alt="">
                                        @endif

                                    @endfor

                                </div>
                                <div class="product__description-reviews-txt">
                                    {{$review->comments}}
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="product__description-reviews-raiting offset-lg-1 col-lg-3 offset-md-1 col-md-4">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="product__description-reviews-star2">
                                    @if($product->review_count != 0)
                                        @for($i = 1; $i<=5;$i++)
                                            @if($i<=ceil($product->review_point/$product->review_count))
                                                <img src="{{asset('img/product-page/star.png')}}" alt="">
                                            @else
                                                <img src="{{asset('img/product-page/start2.png')}}" alt="">
                                            @endif

                                        @endfor
                                        @endif
                                </div>
                                <div class="product__description-reviews-rate">
                                    <span>{{$product->review_count == 0 ? '0': $product->review_point/$product->review_count}}</span>/ <span>5</span>
                                </div>
                            </div>
                            <div class="product__description-reviews-analysis">
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>5 звезд</span>  <div class="w">{{\App\Models\Review::where('product_id',$product->id)->where('review',5)->count()}}</div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>4 звезды</span>  <div class="w">{{\App\Models\Review::where('product_id',$product->id)->where('review',4)->count()}}</div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>3 звезды</span> <div class="w">{{\App\Models\Review::where('product_id',$product->id)->where('review',3)->count()}}</div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>2 звезды</span> <div class="w">{{\App\Models\Review::where('product_id',$product->id)->where('review',2)->count()}}</div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>1 звезда</span> <div class="w">{{\App\Models\Review::where('product_id',$product->id)->where('review',1)->count()}}</div>
                                </div>
                            </div>
                            <button data-modal="reviewModal" class="button product__description-reviews-btn">Написать отзыв</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="catalog">
        <div class="catalog__inner">
            <div class="catalog__blocks catalog__blocks-product">
                <div class="catalog__wrap">
                    <div class="catalog__title">Рекомендованные товары</div>
                    <div class="catalog__showAll" onclick="window.location.href='{{route('shop')}}'">показать все</div>
                </div>
                <div class="catalog__items">
                    @foreach(\App\Models\RelatedProduct::with('product.product_flags.flag')->orderBy('order','asc')->paginate(5) as $related_product )
                        <div class="catalog__item">
                            <div class="catalog__item-wrap">
                                @if(isset($product->products->product_flags[0]))

                                <div class="catalog__item-discound" style="background-color: {{$related_product->product->product_flags[0]->flag->color}}')">
                                    {{$related_product->product->product_flags[0]->flag->name}}
                                </div>
                                @endif
                                <div class="catalog__item-favorite product_add_favourite" data-product-id="{{$related_product->product->id}}">
                                    <img src="/img/catalog/favorite-item.svg" alt="" />
                                </div>
                            </div>
                            <div class="catalog__item-img">
                                <a href="{{route('product',$related_product->product->id)}}">
                                    @if(count($related_product->product->variations) > 0 )
                                        <img src="{{asset('storage/'.$related_product->product->variations[0]->image)}}" alt="" width="100%" />
                                    @else
                                    <img src="{{asset('storage/'.$related_product->product->image)}}" alt="" width="100%" />
                                        @endif
                                </a>
                            </div>
                            <div class="catalog__item-title">
                                <a href="{{route('product',$related_product->product->id)}}">

                                    {{$related_product->product->name}}
                                </a>
                            </div>
                            <div class="catalog__item-subtitle catalog__item-NoProduct">
                                @if($related_product->product->stock != 1)
                                    Нету в складе
                                @else
                                    Есть в наличии

                                @endif
                            </div>
                            <div class="catalog__item-wrap2">
                                @if(count($related_product->product->variations)>0)

                                <div  id="" class="form-control " style="border:none;font-weight:bold;font-size:12px;padding:0px;border-bottom:1px solid #31AC80;width: 40%;border-radius: 0px;">
                                    {{$related_product->product->variations[0]->name}}
                                </div>
                                <div class="catalog__item-cost"><span>{{number_format($related_product->product->variations[0]->price, 0, '.', ' ')}}</span> тг.</div>
                                @else
                                <div class="catalog__item-cost"><span>{{number_format($related_product->product->price,0,'.',' ')}}</span> тг.</div>
                                @endif
                                <div class="catalog__item-buy product_add_basket"  data-product-id="{{$related_product->product->id}}"
                                     @if(count($product->variations)>0) data-variation-id="{{$product->variations[0]->id}}" @endif>
                                    <img src="/img/catalog/buy.png" alt="basket" />
                                </div>
                            </div>

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>

</div>
<div class="overlay">
    <div class="modal" id="reviewModal">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Отзыв</div>
        <form action="/create/review" class="modal__form" method="post">
            {{csrf_field()}}
            <div class="modal__wrap">
                <div class="modal__form-title">Достоинства</div>
                <textarea name="advantages" id=""></textarea>
            </div>
            <div class="modal__wrap">
                <div class="modal__form-title">Недостатки</div>
                <textarea name="disadvantages"  id=""></textarea>
            </div>
            <div class="modal__wrap">
                <div class="modal__form-title">Напишите ваш отзыв</div>
                <textarea name="comment" required id="textareaModal"></textarea>
            </div>
        <div class="modal__reviewDescription">
            <div class="modal__reviewDescription-title">Поставьте заслуженную оценку данному продукту</div>
            <div class="modal__reviewDescription-stars" id="stars">
                <img src="/img/modal/star.png" alt="" class="review_point" id="star_1" onclick="starChoose(1)" data-star = "1">
                <img src="/img/modal/star.png" alt="" class="review_point" id="star_2" onclick="starChoose(2)" data-star="2">
                <img src="/img/modal/star.png" alt="" class="review_point" id="star_3" onclick="starChoose(3)" data-star="3">
                <img src="/img/modal/star.png" alt="" class="review_point" id="star_4" onclick="starChoose(4)" data-star="4">
                <img src="/img/modal/star.png" alt="" class="review_point" id="star_5" onclick="starChoose(5)" data-star="5">
            </div>
            <input type="hidden" name="star" value="1" id="star_input">
            <input type="hidden" name="product_id"  value="{{$product->id}}">
            @if(session()->get('user') != null)
            <input type="hidden" name="user_id" value="{{session()->get('user')->id}}">
                @endif
        </div>
            <button type="submit" class="button modal__btn">Отправить отзыв</button>

        </form>

    </div>
</div>
<style>
    .product__description-item{
        width: 50%;
    }
</style>
<script>
    function variationChange() {
        let variation_id = document.getElementById('product_variation').value
        document.getElementById('product_buy').setAttribute('data-variation-id',variation_id)
        document.getElementById('product_add_basket').setAttribute('data-variation-id',variation_id)
        let htmlPrice = document.getElementById('price_description')
        let htmlPriceSize = document.getElementById('price_size')
        let htmlPriceTech = document.getElementById('price_technical')
        let getVariationPrice = document.getElementById(`variation-${variation_id}`).getAttribute('data-price')
        console.log(getVariationPrice)
        let inputPrice = document.getElementById('price')
        let quantityProduct = document.getElementById('product_buy').getAttribute('data-quantity')
        inputPrice.value = getVariationPrice*parseInt(quantityProduct)
        document.getElementById('image_main').setAttribute('src',document.getElementById(`variation-${variation_id}`).getAttribute('data-image'))
        let inputString = formatNumber(inputPrice.value)

        if (htmlPrice){
            htmlPrice.innerHTML = `Цена :<span> ${inputString} тг</span>`
        }

        if (htmlPriceSize){
            htmlPriceSize.innerHTML = `Цена : <span>${inputString} тг</span>`
        }

        if (htmlPriceTech){
            htmlPriceTech.innerHTML = `Цена : ${inputString} тг`
        }
    }


    let plusButton = document.getElementById('plusButton')
    let minusButton = document.getElementById('minusButton')

    let count = 1;
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
    }
    plusButton.addEventListener("click",()=>{
        let inputPrice = parseInt(document.getElementById('price').value)
        count += 1;
        inputPrice = inputPrice*count
        document.getElementById('amount').innerText = count

        let inputString = formatNumber(inputPrice)
        let htmlPrice = document.getElementById('price_description')
        let htmlPriceSize = document.getElementById('price_size')
        let htmlPriceTech = document.getElementById('price_technical')
        document.getElementById('product_buy').setAttribute('data-quantity',count)
        document.getElementById('product_add_basket').setAttribute('data-quantity',count)
        let text = document.createTextNode(`Цена : ${inputString}`)
        if (htmlPrice){
            htmlPrice.innerHTML = `Цена :<span> ${inputString} тг</span>`
        }

        if (htmlPriceSize){
            htmlPriceSize.innerHTML = `Цена : <span>${inputString} тг</span>`
        }

        if (htmlPriceTech){
            htmlPriceTech.innerHTML = `Цена : ${inputString} тг`
        }

    })
    minusButton.addEventListener("click",()=>{
        if(count === 1){
            return  true;
        }
        let inputPrice = parseInt(document.getElementById('price').value)
        count -= 1;
        document.getElementById('amount').innerText = count
        inputPrice = inputPrice*count
        document.getElementById('product_buy').setAttribute('data-quantity',count)
        document.getElementById('product_add_basket').setAttribute('data-quantity',count)
        let inputString = formatNumber(inputPrice)
        let htmlPrice = document.getElementById('price_description')
        let htmlPriceSize = document.getElementById('price_size')
        let htmlPriceTech = document.getElementById('price_technical')
        let text = document.createTextNode(`Цена : ${inputString}`)
        if (htmlPrice){
            htmlPrice.innerHTML = `Цена :<span> ${inputString} тг</span>`
        }

        if (htmlPriceSize){
            htmlPriceSize.innerHTML = `Цена : <span>${inputString} тг</span>`
        }

        if (htmlPriceTech){
            htmlPriceTech.innerHTML = `Цена :${inputString} тг`
        }

    })




    let starsDiv = document.getElementById('stars')
    function starChoose(number) {
        let starsDiv = document.getElementById('stars')
        let numberForArray = number*2
        let i =0;
        starsDiv.childNodes.forEach(star=>{
            if(star instanceof HTMLElement){
                if (i <number){
                    star.setAttribute('src','/img/modal/Star.svg')
                }else{
                    star.setAttribute('src','/img/modal/star.png')
                }
                i+=1;
            }
        })
    }

    function changeSize(json) {

    }


</script>
<style>
    .product__description-specification-wrap{
        display: flex;
        flex-wrap: wrap;
        width: 100%;
    }
    .product__description-specification-box{
        width: 50%;
    }
    .review_point:hover{
        cursor: pointer;
    }
</style>
@endsection


<!-- ---------------Модалка оставить -------------------- -->

