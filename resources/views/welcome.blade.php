@extends('layout.app')
@section('content')
<div class="container-fluid">
    <section class="main">
        <div class="main__inner">
            <div class="main__items">
                @foreach($banners as $banner)
                <div class="main__item">
                    <img src="{{asset('storage/'.$banner->image)}}" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">{{$banner->percent}}</div>
                        <div class="main__item-subtitle">
                            {{$banner->name}}
                        </div>
                        <div class="main__item-btn">
                            <button class="button" onclick="window.location.href='{{$banner->link}}'">Перейти</button>
                        </div>
                    </div>
                </div>
                @endforeach
                @if(count($banners)<2)
                <div class="main__item">
                    <img src="/img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                <div class="main__item">
                    <img src="/img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                <div class="main__item">
                    <img src="/img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                <div class="main__item">
                    <img src="/img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                <div class="main__item">
                    <img src="img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                <div class="main__item">
                    <img src="img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                <div class="main__item">
                    <img src="img/main-page/main2.png" alt="" />
                    <div class="main__item-wrap">
                        <div class="main__item-title">15%</div>
                        <div class="main__item-subtitle">свежие фрукты и овощи</div>
                        <div class="main__item-btn">
                            <button class="button">Перейти</button>
                        </div>
                    </div>
                </div>
                    @endif
            </div>
        </div>
    </section>
    <section class="catalog">
        <div class="catalog__inner">
            @if(count(\App\Models\Brand::get()) > 0)
            <div class="catalog__blocks catalog__blocks-bestProduct">
                <div class="catalog__wrap">
                    <div class="catalog__title catalog__title2">лучшие бренды</div>
                </div>
                <div class="catalog__items brand-slider">
                    {{-- <div class="catalog__slider"> --}}
                    @foreach(\App\Models\Brand::get() as $brand)

                    <div class="catalog__item catalog__item2">
                        <a style="flex:1 1 auto; justify-content: center; align-items: center; display: flex;" href="{{route('shop')}}?brand={{$brand->id}}">
                            <div class="catalog__item2-img">
                                <img src="{{asset('storage/'.$brand->image)}}" alt="" />
                            </div>
                        </a>
                        <a href="{{route('shop')}}?brand={{$brand->id}}">
                            <div class="catalog__item2-title">{{$brand->name}}</div>

                        </a>
                    </div>
                {{-- </div> --}}
                    @endforeach
                </div>
            </div>

            @endif

        @foreach($main_sections as $section)
            <div class="catalog__blocks catalog__blocks-product">
                <div class="catalog__wrap">
                    <div class="catalog__title">{{$section->name}}</div>
                    <a href="{{$section->link}}"> <div class="catalog__showAll">показать все</div></a>
                </div>
                <div class="catalog__items">

                    @foreach($section->products_section as $product)

                        @if($product->products != null)
                    <div class="catalog__item">

                        <div class="catalog__item-wrap">
                            @if(isset($product->products->product_flags[0]))
                            <div class="catalog__item-discound" style="background-color: {{$product->products->product_flags[0]->flag->color}} !important;">
                                {{$product->products->product_flags[0]->flag->name}}
                            </div>
                            @endif
                            <div class="catalog__item-favorite product_add_favourite" data-product-id="{{$product->products->id}}">
                                <img
                                    class="favorite-block"
                                    src="{{ isset(session()->get('favourite')[$product->products->id])?'/img/product-page/red_heart.svg':'/img/catalog/favorite-item.svg'}}"
                                    alt=""
                                />
                            </div>
                        </div>
                        @endif
                        @if($product->products != null)

                        <div class="catalog__item-img">
                            <a href="/catalog/{{$product->products->id}}">
                                <img class="product_image_{{$product->id}}" src="{{count($product->products->variations)>0 ? asset('storage/'.$product->products->variations[0]->image) : asset('storage/'.$product->products->image)}}" style="width: 100%;" alt="product" />

                            </a>
                        </div>
                        <div class="catalog__item-title">
                            <a href="/catalog/{{$product->products->id}}">

                            {{$product->products->name}}
                            </a>
                        </div>
                        <div class="catalog__item-subtitle catalog__item-NoProduct">
                            @if($product->products->stock)
                                В наличии

                                @else
                                Нету в складе
                            @endif
                        </div>
                        <div class="catalog__item-wrap2">
                            @if(count($product->products->variations)>0)
                                <select  id=""
                                         class="form-control select_choose"
                                         data-product-id="{{$product->products->id}}"
                                         style="border:none;font-weight:bold;font-size:11px;padding:0px; margin-top:40px; border-bottom:1px solid #31AC80;width: 35%;border-radius: 0px;">
                                    @foreach($product->products->variations as $variation)
                                        <option value="{{$variation}}">{{$variation->name}}</option>
                                    @endforeach
                                </select>
                                <div class="catalog__item-cost catalog__item-costs"><span class="product_span_price_{{$product->products->id}}">{{number_format($product->products->variations[0]->price, 0, '.', ' ')}}</span> тг.</div>

                            @else
                            <div class="catalog__item-cost"><span>{{number_format($product->products->price, 0, '.', ' ')}}</span> тг.</div>
                            @endif
                            <div class="catalog__item-buy">
                                <img src="img/catalog/buy.svg" alt="basket"
                                     class="product_add_basket product_add_basket__{{$product->products->id}}"
                                     data-product-id="{{$product->products->id}}"
                                     @if(count($product->products->variations)>0)
                                        data-variation-id="{{$product->products->variations[0]->id}}"
                                    @endif />
                            </div>
                        </div>
                    </div>
                        @endif
                    @endforeach

                </div>
            </div>
            @endforeach

        </div>
    </section>
</div>
@endsection
