<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\Attribute\AttributeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\Api\Catalog\CatalogController;
use App\Http\Controllers\Api\Admin\Product\ProductController;
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::prefix('admin')->group(function(){
    Route::get('categories',[AttributeController::class,'getCategories']);
    Route::get('attributes/form/{id}',[AttributeController::class,'get']);
    Route::get('product/{id}',[ProductController::class,'getProduct']);

});

Route::get('/filters',[CatalogController::class,'filters']);
Route::get('/products',[CatalogController::class,'products']);
Route::get('/category/name',[CatalogController::class,'category']);
Route::post('/submit/return',[CatalogController::class,'returnCreate']);
