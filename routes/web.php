<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\IndexController;
use App\Http\Controllers\Front\CatalogController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class,'index'])->name('index');
Route::get('/shop/{categoryId?}',[CatalogController::class,'index'])->name('shop');
Route::get('/catalog/{product}',[CatalogController::class,'show'])->name('product');
Route::get('order',[CatalogController::class,'order'])->name('order');
Route::get('basket',[CatalogController::class,'basket']);
Route::get('favourite',[CatalogController::class,'favourite'])->name('favourite');
Route::post('login',[IndexController::class,'login'])->name('login');
Route::get('account',[IndexController::class,'user'])->name('user');
Route::get('/add/basket/{product_id}',[CatalogController::class,'addBasket']);
Route::get('/add/plus/{product_id}',[CatalogController::class,'addPlus'])->name('addPlus');
Route::get('/add/minus/{product_id}',[CatalogController::class,'addMinus'])->name('addMinus');
Route::get('/search/{search?}',[IndexController::class,'search'])->name('search');
Route::get('/delete/basket/{product_id}',[CatalogController::class,'deleteBasketProduct'])->name('deleteBasket');
Route::get('/add/favourite/{product_id}',[CatalogController::class,'addFavourite']);
Route::get('/delete/favourite/{product_id}',[CatalogController::class,'deleteFavourite'])->name('deleteFavourite');
Route::post('create/order',[IndexController::class,'order']);
Route::get('logout',[IndexController::class,'logout'])->name('logout');
Route::post('create/review',[IndexController::class,'storeReview'])->name('storeReview');
Route::post('/register',[IndexController::class,'register'])->name('register');
Route::post('/update',[IndexController::class,'updateUser'])->name('update');
Route::get('order/{id}',[IndexController::class,'orderMain'])->name('orderMain');
Route::get('orders',[IndexController::class,'orders'])->name('orders');
Route::get('delivery',[IndexController::class,'delivery'])->name('delivery');
Route::get('conditions',[IndexController::class,'conditions'])->name('conditions');
Route::get('about',[IndexController::class,'about'])->name('about');
Route::get('contacts',[IndexController::class,'contacts'])->name('contacts');
Route::get('pay/{id}',[IndexController::class,'pay'])->name('pay');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
