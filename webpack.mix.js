const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);

mix.js('resources/js/main.js', 'public/js')
mix.js('resources/js/product.js', 'public/js')
mix.js('resources/js/catalog.js', 'public/js')
mix.sass('resources/sass/blocks.sass', 'public/css')
mix.sass('resources/sass/category_style.sass', 'public/css')
mix.sass('resources/sass/global.sass', 'public/css')
mix.sass('resources/sass/header.sass', 'public/css')
